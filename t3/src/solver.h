/*
 * solver.h
 *
 *  Created on: Dec 6, 2018
 *      Author: bvdmitri
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include <thrust/device_vector.h>

#include "linalg.h"

__constant__ int M, N;
__constant__ double h1, h2;
__constant__ double invh1_2, invh2_2, inv2h1_2, inv2h2_2;
__constant__ double inv2h1, inv2h2;

// Functor to compute r = A w
// Input arguments:
//   : M      - number of grid points on x-axis
//   : N      - number of grid points on y-axis
//   : h1     - grid step on x-axis
//   : h2     - grid step on y-axis
//   : w      - Device vector to multiply on
//   : r      - Device vector to store result in
template<typename D>
struct ComputeAwFunctor : public thrust::unary_function<int, D> {
	const D * const w;

	ComputeAwFunctor(const thrust::device_vector<D> &w): w(thrust::raw_pointer_cast(w.data())) {}

	__device__ D operator()(const int k) const {
		int gi = k % M;   // global i index
		int gj = k / M;   // global j index

		D wij   = w[k];
		D wip1j = (gi == M - 1) ? 0.0 : w[k + 1];
		D wijp1 = (gj == N - 1) ? 0.0 : w[k + M];

		if ((gi >= 1 && gi < M) && (gj >= 1 && gj < N)) { // Check if current point is inner point of grid
			// Here we have the following formula
			// -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
			double wim1j = w[k - 1];
			double wijm1 = w[k - M];
			return invh1_2 * (wip1j - 2.0 * wij + wim1j) + invh2_2 * (wijp1 - 2.0 * wij + wijm1);

		} else if (gi == 0 && gj != 0) { // Check if current point on Left border
			// Here we have the following formula
			// -2 / h1^2 (w_i+1_j - w_i_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
			// i = 0
			// -2 / h1^2 (w1j - w0j) - 1 / h2^2 (w0j+1 - 2w0j + w0j-1)
			double w0jm1 = w[k - M];
			return inv2h1_2 * (wip1j - wij) + invh2_2 * (wijp1 - 2.0 * wij + w0jm1);

		} else if (gj == 0 && gi != 0) { // Check if current point on Bottom border
			// Here we have the following formula
			// -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 2 / h^2 (w_i_j+1 - w_i_j)
			// j = 0
			// -1 / h1^2 (w_i+1_0 - 2wi0 + w_i-1_0) - 2 / h^2 (wi1 - wi0)
			double wim10 = w[k - 1];
			return invh1_2 * (wip1j - 2.0 * wij + wim10) + inv2h2_2 * (wijp1 - wij);

		} else if (gi == 0 && gj == 0) { // Check if current point on Left-Bottom point
			// Here we have the following formula
			// -2 / h1^2 (w_i+1_j - w_i_j) - 2 / h2^2 (w_i_j+1 - w_i_j)
			// i = 0 and j = 0
			// -2 / h1^2 (w10 - w00) - 2 / h2^2 (w01 - w00)
			return inv2h1_2 * (wip1j - wij) + inv2h2_2 * (wijp1 - wij);
		}

		return (D) 0;
	}
};

// Functor to compute r = A w - B
template<typename D>
struct ComputeAwmBFunctor : public thrust::unary_function<int, D> {
	const D * const w;
	const D * const f;
	const D * const dirichleT;
	const D * const dirichleR;
	const D * const neumannB;
	const D * const neumannL;

	ComputeAwmBFunctor(const thrust::device_vector<D> &w, const thrust::device_vector<D> &f,
			const thrust::device_vector<D> &dirichleT, const thrust::device_vector<D> &dirichleR,
			const thrust::device_vector<D> &neumannB, const thrust::device_vector<D> &neumannL):
				w(thrust::raw_pointer_cast(w.data())),
				f(thrust::raw_pointer_cast(f.data())),
				dirichleT(thrust::raw_pointer_cast(dirichleT.data())),
				dirichleR(thrust::raw_pointer_cast(dirichleR.data())),
				neumannB(thrust::raw_pointer_cast(neumannB.data())),
				neumannL(thrust::raw_pointer_cast(neumannL.data())) {}

	__device__ D operator()(const int k) const {
		int gi = k % M;   // global i index
		int gj = k / M;   // global j index

		D wij   = w[k];
		D wip1j = (gi == M - 1) ? 0.0 : w[k + 1];
		D wijp1 = (gj == N - 1) ? 0.0 : w[k + M];

		D b = (D) f[k];

		if (gi == M - 1) {
			b -= invh1_2 * dirichleR[gj];
		}

		if (gj == N - 1) {
			b -= invh2_2 * dirichleT[gi];
		}


		if ((gi >= 1 && gi < M) && (gj >= 1 && gj < N)) { // Check if current point is inner point of grid

			// Here we have the following formula
			// -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
			double wim1j = w[k - 1];
			double wijm1 = w[k - M];
			return invh1_2 * (wip1j - 2.0 * wij + wim1j) + invh2_2 * (wijp1 - 2.0 * wij + wijm1) - b;

		} else if (gi == 0 && gj != 0) { // Check if current point on Left border

			// Here we have the following formula
			// -2 / h1^2 (w_i+1_j - w_i_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
			// i = 0
			// -2 / h1^2 (w1j - w0j) - 1 / h2^2 (w0j+1 - 2w0j + w0j-1)
			double w0jm1 = w[k - M];
			return inv2h1_2 * (wip1j - wij) + invh2_2 * (wijp1 - 2.0 * wij + w0jm1) + inv2h1 * neumannL[gj] - b;

		} else if (gj == 0 && gi != 0) { // Check if current point on Bottom border

			// Here we have the following formula
			// -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 2 / h^2 (w_i_j+1 - w_i_j)
			// j = 0
			// -1 / h1^2 (w_i+1_0 - 2wi0 + w_i-1_0) - 2 / h^2 (wi1 - wi0)

			double wim10 = w[k - 1];
			return invh1_2 * (wip1j - 2.0 * wij + wim10) + inv2h2_2 * (wijp1 - wij) + inv2h2 * neumannB[gi] - b;

		} else if (gi == 0 && gj == 0) { // Check if current point on Left-Bottom point

			// Here we have the following formula
			// -2 / h1^2 (w_i+1_j - w_i_j) - 2 / h2^2 (w_i_j+1 - w_i_j)
			// i = 0 and j = 0
			// -2 / h1^2 (w10 - w00) - 2 / h2^2 (w01 - w00)

			// Exact neumann in Left-Bottom point
			D neuman_exact = (D) 0;
			return inv2h1_2 * (wip1j - wij) + inv2h2_2 * (wijp1 - wij) + neuman_exact - b;

		}

		return (D) 0;
	}
};

template<typename D>
struct ComputewmtrFunctor : public thrust::unary_function<const thrust::tuple<D, D>, D> {
	const D t;

	ComputewmtrFunctor(D t): t(t) {}

	__device__ D operator()(const thrust::tuple<D, D> &e) const {
		return thrust::get<0>(e) - t * thrust::get<1>(e);
	}
};

template<typename T, typename P>
class Solver {
private:
	const Problem<T, P> &problem;

public:
	Solver(const Problem<T, P> &problem): problem(problem) {}

	T solve(double eps, int max_iterations) const {
		fillInConstants(problem);

		thrust::device_vector<int> grid_indices(problem.domain.M * problem.domain.N);
		thrust::sequence(grid_indices.begin(), grid_indices.end());

		thrust::device_vector<T> w1(problem.domain.M * problem.domain.N);
		thrust::device_vector<T> w2(problem.domain.M * problem.domain.N);

		thrust::device_vector<T> r(problem.domain.M * problem.domain.N);
		thrust::device_vector<T> Ar(problem.domain.M * problem.domain.N);

		thrust::fill(w1.begin(), w1.end(), (T) 0);
		thrust::fill(w2.begin(), w2.end(), (T) 0);
		thrust::fill(r.begin(), r.end(), (T) 0);
		thrust::fill(Ar.begin(), Ar.end(), (T) 0);

		const ProblemDomain<P> &d = problem.domain;

		int iter = 0;
		T norm = (T) 0;
		while (iter < max_iterations) {
			const thrust::device_vector<T> &current = iter % 2 == 0 ? w1 : w2;
			thrust::device_vector<T> &next = iter % 2 == 1 ? w1 : w2;

			computeAwmB(problem, grid_indices, current, r);
			computeAw(problem, grid_indices, r, Ar);

			// T t = linalg::ScalarProduct_E(d.h1, d.h2, Ar, r) / linalg::ScalarProduct_E(d.h1, d.h2, Ar);
			// T t = linalg::DivideTwoScalarProduct_E(r, Ar);

			const thrust::tuple<T, T, T> products = linalg::Compute3ScalarProduct_E(r, Ar);
			T t = thrust::get<2>(products) / thrust::get<1>(products);
			norm = std::sqrt(d.h1 * d.h2 * thrust::get<0>(products));

			computewmtr(current, r, t, next);

			// norm = linalg::Norm_E(d.h1, d.h2, r);

			if (iter % 50000 == 0) {
				std::cout << iter << " " << norm << std::endl;
			}

			if (norm < eps) {
				std::cout << "Iterations count: " << iter << std::endl;
				std::cout << "E norm of error:  " << linalg::NormSubtract_E(d.h1, d.h2, problem.U, next) << std::endl;
				std::cout << "C norm of error:  " << linalg::NormSubtract_E(d.h1, d.h2, problem.U, next) << std::endl;
				break;
			}

			iter += 1;
		}
		return norm;
	}

private:

	void computeAw(const Problem<T, P> &p, const thrust::device_vector<int> &indices,
			const thrust::device_vector<T> &w, thrust::device_vector<T> &out) const {
		thrust::transform(indices.begin(), indices.end(), out.begin(), ComputeAwFunctor<double>(w));
	}

	void computeAwmB(const Problem<T, P> &p, const thrust::device_vector<int> &indices,
			const thrust::device_vector<T> &w, thrust::device_vector<T> &out) const {
		thrust::transform(indices.begin(), indices.end(), out.begin(),
				ComputeAwmBFunctor<double>(w, p.F, p.dirichleT, p.dirichleR, p.neumannB, p.neumannL)
		);
	}

	void computewmtr(const thrust::device_vector<T> &w, const thrust::device_vector<T> &r, T t,
			thrust::device_vector<T> &out) const {
		thrust::transform(
				thrust::make_zip_iterator(thrust::make_tuple(w.begin(), r.begin())),
				thrust::make_zip_iterator(thrust::make_tuple(w.end(), r.end())),
				out.begin(),
				ComputewmtrFunctor<T>(t)
		);
	}

	void fillInConstants(const Problem<T, P> &p) const {
		const ProblemDomain<P> &d = p.domain;
		cudaMemcpyToSymbol(M, &(d.M), sizeof(int), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(N, &(d.N), sizeof(int), 0, cudaMemcpyHostToDevice);

		T p_h1 = d.h1;
		T p_h2 = d.h2;
		T p_invh1_2 = -1.0 / (d.h1 * d.h1);
		T p_invh2_2 = -1.0 / (d.h2 * d.h2);
		T p_inv2h1_2 = -2.0 / (d.h1 * d.h1);
		T p_inv2h2_2 = -2.0 / (d.h2 * d.h2);
		T p_inv2h1 = 2.0 / d.h1;
		T p_inv2h2 = 2.0 / d.h2;

		cudaMemcpyToSymbol(h1, &(p_h1), sizeof(T), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(h2, &(p_h2), sizeof(T), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(invh1_2, &(p_invh1_2), sizeof(T), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(invh2_2, &(p_invh2_2), sizeof(T), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(inv2h1_2, &(p_inv2h1_2), sizeof(T), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(inv2h2_2, &(p_inv2h2_2), sizeof(T), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(inv2h1, &(p_inv2h1), sizeof(T), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(inv2h2, &(p_inv2h2), sizeof(T), 0, cudaMemcpyHostToDevice);
	}
};

#endif /* SOLVER_H_ */
