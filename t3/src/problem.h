/*
 * problem.h
 *
 *  Created on: Dec 6, 2018
 *      Author: bvdmitri
 */

#ifndef PROBLEM_H_
#define PROBLEM_H_

#include <functional>
#include <algorithm>
#include <iostream>
#include <thrust/device_vector.h>

#include "debug.h"

template<typename T>
class ProblemDomain {
public:
	const int M, N;
	const T A1, A2, B1, B2;
	const T h1, h2;

	ProblemDomain(int M, int N, T A1, T A2, T B1, T B2): M(M), N(N), A1(A1), A2(A2), B1(B1), B2(B2), h1((A2 - A1) / M), h2((B2 - B1) / N) {};

	template<typename OT>
	friend std::ostream& operator<<(std::ostream& os, const ProblemDomain<OT>& d);
};

template<typename T>
std::ostream& operator<<(std::ostream& os, const ProblemDomain<T>& d) {
    os << "Omega: (" << d.A1 << "," << d.A2 << ") x (" << d.B1 << "," << d.B2 << ")" << std::endl;
    os << "Grid:  [" << 0 << "," << d.M << ") x [" << 0 << "," << d.N << ")" << std::endl;
    os << "h1 = " << d.h1 << ", h2 = " << d.h2;
    return os;
}

template<typename T, typename D>
struct ApplyGridLambda {
	const int M, N;
	const D A1, B1;
	const D h1, h2;
	const std::function<T (D, D)> &lambda;

	ApplyGridLambda(int M, int N, D A1, D B1, D h1, D h2, const std::function<T (D, D)> &lambda):
		M(M), N(N), A1(A1), B1(B1), h1(h1), h2(h2), lambda(lambda) {}

	T operator()(const int &k) const {
		int gi = k % M;   // global i index
		int gj = k / M;   // global j index
		return lambda(A1 + gi * h1, B1 + gj * h2);
	}
};

template<typename T, typename D>
struct ApplyGridBorderXLambda  {
	const D from, h, Y;
	const std::function<T (D, D)> &lambda;

	ApplyGridBorderXLambda(D from, D h, D Y, const std::function<T (D, D)> &lambda): from(from), h(h), Y(Y), lambda(lambda) {}

	T operator()(const int &k) const {
		return lambda(from + k * h, Y);
	}
};

template<typename T, typename D>
struct ApplyGridBorderYLambda {
	const D from, h, X;
	const std::function<T (D, D)> &lambda;

	ApplyGridBorderYLambda(D from, D h, D X, const std::function<T (D, D)> &lambda): from(from), h(h), X(X), lambda(lambda) {}

	T operator()(const int &k) const {
		return lambda(X, from + k * h);
	}
};

template<typename T, typename D>
class Problem {
public:
	const ProblemDomain<D>         &domain;

	thrust::device_vector<T> U;
	thrust::device_vector<T> F;
	thrust::device_vector<T> dirichleT;
	thrust::device_vector<T> dirichleR;
	thrust::device_vector<T> neumannL;
	thrust::device_vector<T> neumannB;
public:
	Problem(const ProblemDomain<D> &d,
			const std::function<T (D, D)> &u, const std::function<T (D, D)> &f,
			const std::function<T (D, D)> &dirichle, const std::function<T (D, D)> &neumann):
				domain(d),
				U(thrust::device_vector<T>(d.M * d.N)),
				F(thrust::device_vector<T>(d.M * d.N)),
				dirichleT(thrust::device_vector<T>(d.M)),
				dirichleR(thrust::device_vector<T>(d.N)),
				neumannL(thrust::device_vector<T>(d.N)),
				neumannB(thrust::device_vector<T>(d.M)) {

		// Create grid values (i, j)
		std::vector<int> grid(d.M * d.N);
		std::iota(grid.begin(), grid.end(), 0);

		// Fill grid function U and F
		std::vector<double> grid_values(d.M * d.N);

		std::transform(grid.begin(), grid.end(), grid_values.begin(), ApplyGridLambda<T, D>(d.M, d.N, d.A1, d.B1, d.h1, d.h2, u));
		thrust::copy(grid_values.begin(), grid_values.end(), U.begin());

		std::transform(grid.begin(), grid.end(), grid_values.begin(), ApplyGridLambda<T, D>(d.M, d.N, d.A1, d.B1, d.h1, d.h2, f));
		thrust::copy(grid_values.begin(), grid_values.end(), F.begin());

		// Create grid values (i, const)
		std::vector<int> xborder(d.M);
		std::vector<double> xvalues(d.M);
		std::iota(xborder.begin(), xborder.end(), 0);

		// Create grid values (const, j)
		std::vector<int> yborder(domain.N);
		std::vector<double> yvalues(d.M);
		std::iota(yborder.begin(), yborder.end(), 0);

		std::transform(xborder.begin(), xborder.end(), xvalues.begin(), ApplyGridBorderXLambda<T, D>(d.A1, d.h1, d.B2, dirichle));
		thrust::copy(xvalues.begin(), xvalues.end(), dirichleT.begin());

		std::transform(yborder.begin(), yborder.end(), yvalues.begin(), ApplyGridBorderYLambda<T, D>(d.B1, d.h2, d.A2, dirichle));
		thrust::copy(yvalues.begin(), yvalues.end(), dirichleR.begin());

		std::transform(xborder.begin(), xborder.end(), xvalues.begin(), ApplyGridBorderXLambda<T, D>(d.A1, d.h1, d.B1, neumann));
		thrust::copy(xvalues.begin(), xvalues.end(), neumannB.begin());

		std::transform(yborder.begin(), yborder.end(), yvalues.begin(), ApplyGridBorderYLambda<T, D>(d.B1, d.h2, d.A1, neumann));
		thrust::copy(yvalues.begin(), yvalues.end(), neumannL.begin());
	};
};


#endif /* PROBLEM_H_ */
