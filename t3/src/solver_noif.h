/*
 * solver.h
 *
 *  Created on: Dec 6, 2018
 *      Author: bvdmitri
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include <thrust/device_vector.h>

#include "linalg.h"

// COMPUTED ONLY ONCE! so ifs here is not so important
// Functor to compute some additional data for non-if algorithm
struct ComputeIfDataFunctor : public thrust::unary_function<unsigned int, unsigned int> {
	const unsigned int M, N;

	ComputeIfDataFunctor(unsigned int M, unsigned int N): M(M), N(N) {}


	// pnt   [ i+1    i+1    i-1    i-1   j+1    j+1     j-1    j-1    mc1    mc1   mc2   mc2]
	// coef  [ 0      1      0      1      0      1      0      1      1     2      1     2  ]
	__device__ unsigned int operator()(const unsigned int k) const {
		unsigned int gi = k % M;   // global i index
		unsigned int gj = k / M;   // global j index

		unsigned int out = 0;

		if (gi != M - 1) {
			out += 1 << 0; //coeff1 = 1
		}

		if (gj != N - 1) {
			out += 1 << 2; //coeff3 = 1
		}

		if ((gi >= 1 && gi < M) && (gj >= 1 && gj < N)) { // Check if current point is inner point of grid
			out += 1 << 1; //coeff2 = 1
			out += 1 << 3; //coeff4 = 1
		} else if (gi == 0 && gj != 0) { // Check if current point on Left border
			out += 1 << 4; // mc1 = 2.0
			out += 1 << 3; // coeff4 = 1
		} else if (gj == 0 && gi != 0) { // Check if current point on Bottom border
			out += 1 << 5; // mc2 = 2.0
			out += 1 << 1; // coeff2 = 1
		} else if (gi == 0 && gj == 0) { // Check if current point on Left-Bottom point
			out += 1 << 4; //mc1 = 2.0;
			out += 1 << 5; //mc2 = 2.0;
		}

		return (k << 7) + out; // packing data here, cause of that we cannot have grid size more than 2^24 points (fair enough)
	}
};

// Functor to compute r = A w
// Input arguments:
//   : M      - number of grid points on x-axis
//   : N      - number of grid points on y-axis
//   : h1     - grid step on x-axis
//   : h2     - grid step on y-axis
//   : w      - Device vector to multiply on
template<typename D>
struct ComputeAwFunctor : public thrust::unary_function<unsigned int, D> {
	const unsigned int M, N;
	const D invh1_2, invh2_2;
	const D * const w;

	// const int coeff[12] = { 0, 1, 0, 1, 0, 1, 0, 1, 1, 2, 1, 2 };
	// pnt   [ i+1    i+1    i-1    i-1   j+1    j+1     j-1    j-1    mc1    mc1   mc2   mc2]
	// coef  [ 0      1      0      1      0      1      0      1      1     2      1     2  ]
	ComputeAwFunctor(int M, int N, D h1, D h2, const thrust::device_vector<D> &w):
		M(M), N(N),
		invh1_2(-1.0 / (h1 * h1)), invh2_2(-1.0 / (h2 * h2)),
		w(thrust::raw_pointer_cast(w.data())) {}

	//		return invh1_2 * (mc1 * coeff1 * wip1j - 2.0 * wij + coeff2 * wim1j) +
	//							invh2_2 * (mc2 * coeff3 * wijp1 - 2.0 * wij + coeff4 * wijm1);

	__device__ D operator()(const unsigned int data) const {
		unsigned int k = data >> 7;
		unsigned int d = data & ((1 << 6) - 1);

		int coeff1 = d & 1;
		int coeff2 = (d >> 1) & 1;
		int coeff3 = (d >> 2) & 1;
		int coeff4 = (d >> 3) & 1;
		int mc1 = 1 + (d >> 4) & 1;
		int mc2 = 1 + (d >> 5) & 1;

		return (D) invh1_2 * (mc1 * coeff1 * w[k + 1 * coeff1] - 2.0 * w[k] + coeff2 * w[k - 1 * coeff2]) +
				invh2_2 * (mc2 * coeff3 * w[k + M * coeff3] - 2.0 * w[k] + coeff4 * w[k - M * coeff4]);
	}
};

// Functor to compute r = A w - B
template<typename D>
struct ComputeAwmBFunctor : public thrust::unary_function<unsigned int, D> {
	const unsigned int M, N;
	const D h1, h2;
	const D invh1_2, invh2_2;
	const D * const w;
	const D * const f;
	const D * const dirichleT;
	const D * const dirichleR;
	const D * const neumannB;
	const D * const neumannL;

	ComputeAwmBFunctor(unsigned int M, unsigned int N, D h1, D h2, const thrust::device_vector<D> &w,
			const thrust::device_vector<D> &f,
			const thrust::device_vector<D> &dirichleT, const thrust::device_vector<D> &dirichleR,
			const thrust::device_vector<D> &neumannB, const thrust::device_vector<D> &neumannL):
				M(M), N(N), h1(h1), h2(h2),
				invh1_2(-1.0 / (h1 * h1)), invh2_2(-1.0 / (h2 * h2)),
				w(thrust::raw_pointer_cast(w.data())),
				f(thrust::raw_pointer_cast(f.data())),
				dirichleT(thrust::raw_pointer_cast(dirichleT.data())),
				dirichleR(thrust::raw_pointer_cast(dirichleR.data())),
				neumannB(thrust::raw_pointer_cast(neumannB.data())),
				neumannL(thrust::raw_pointer_cast(neumannL.data())) {}

	__host__ __device__ D operator()(const unsigned int data) const {
		unsigned int k = data >> 7;
		unsigned int d = data & ((1 << 6) - 1);

		int coeff1 = d & 1;
		int coeff2 = (d >> 1) & 1;
		int coeff3 = (d >> 2) & 1;
		int coeff4 = (d >> 3) & 1;
		int mc1 = 1 + (d >> 4) & 1;
		int mc2 = 1 + (d >> 5) & 1;

		unsigned int gi = k % M;
		unsigned int gj = k / M;

		return  (D) invh1_2 * (mc1 * coeff1 * w[k + 1 * coeff1] - 2.0 * w[k] + coeff2 * w[k - 1 * coeff2]) +
				invh2_2 * (mc2 * coeff3 * w[k + M * coeff3] - 2.0 * w[k] + coeff4 * w[k - M * coeff4]) -
				(f[k] - (1 - coeff1) * (invh1_2 * dirichleR[gj]) - (1 - coeff3) * (invh2_2 * dirichleT[gi]) -
						(1 - coeff2) * (2.0 / h1) * neumannL[gj] - (1 - coeff4) * (2.0 / h2) * neumannB[gi]);
	}
};

template<typename D>
struct ComputewmtrFunctor : public thrust::unary_function<const thrust::tuple<D, D>, D> {
	const D t;

	ComputewmtrFunctor(D t): t(t) {}

	__device__ D operator()(const thrust::tuple<D, D> &e) const {
		return thrust::get<0>(e) - t * thrust::get<1>(e);
	}
};

template<typename T, typename P>
class Solver {
private:
	const Problem<T, P> &problem;

public:
	Solver(const Problem<T, P> &problem): problem(problem) {}

	T solve(double eps, int max_iterations) const {
		const ProblemDomain<P> &d = problem.domain;

		thrust::device_vector<int> grid_indices(d.M * d.N);
		thrust::sequence(grid_indices.begin(), grid_indices.end());
		thrust::device_vector<unsigned int> grid_data(d.M * d.N);
		thrust::transform(grid_indices.begin(), grid_indices.end(), grid_data.begin(), ComputeIfDataFunctor(d.M, d.N));
		grid_indices.clear();

		thrust::device_vector<T> w1(d.M * d.N);
		thrust::device_vector<T> w2(d.M * d.N);

		thrust::device_vector<T> r(d.M * d.N);
		thrust::device_vector<T> Ar(d.M * d.N);

		thrust::fill(w1.begin(), w1.end(), (T) 0);
		thrust::fill(w2.begin(), w2.end(), (T) 0);
		thrust::fill(r.begin(), r.end(), (T) 0);
		thrust::fill(Ar.begin(), Ar.end(), (T) 0);

		int iter = 0;
		T norm = (T) 0;
		while (iter < max_iterations) {
			const thrust::device_vector<T> &current = iter % 2 == 0 ? w1 : w2;
			thrust::device_vector<T> &next = iter % 2 == 1 ? w1 : w2;

			computeAwmB(problem, grid_data, current, r);
			computeAw(problem, grid_data, r, Ar);

			T t = linalg::ScalarProduct_E(d.h1, d.h2, Ar, r) / linalg::ScalarProduct_E(d.h1, d.h2, Ar);
			computewmtr(current, r, t, next);

			norm = linalg::Norm_E(d.h1, d.h2, r);
			// std::cout << iter << " " << norm << std::endl;
			if (norm < eps) {
				break;
			}

			iter += 1;
		}
		return norm;
	}

private:

	void computeAw(const Problem<T, P> &p, const thrust::device_vector<unsigned int> &data,
			const thrust::device_vector<T> &w, thrust::device_vector<T> &out) const {
		const ProblemDomain<P> &d = p.domain;
		thrust::transform(data.begin(), data.end(), out.begin(), ComputeAwFunctor<double>(d.M, d.N, d.h1, d.h2, w));
	}

	void computeAwmB(const Problem<T, P> &p, const thrust::device_vector<unsigned int> &data,
			const thrust::device_vector<T> &w, thrust::device_vector<T> &out) const {
		const ProblemDomain<P> &d = p.domain;
		thrust::transform(data.begin(), data.end(), out.begin(),
				ComputeAwmBFunctor<double>(d.M, d.N, d.h1, d.h2, w, p.F, p.dirichleT, p.dirichleR, p.neumannB, p.neumannL)
		);
	}

	void computewmtr(const thrust::device_vector<T> &w, const thrust::device_vector<T> &r, T t,
			thrust::device_vector<T> &out) const {
		thrust::transform(
				thrust::make_zip_iterator(thrust::make_tuple(w.begin(), r.begin())),
				thrust::make_zip_iterator(thrust::make_tuple(w.end(), r.end())),
				out.begin(),
				ComputewmtrFunctor<T>(t)
		);
	}
};

#endif /* SOLVER_H_ */
