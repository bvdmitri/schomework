/*
 ============================================================================
 Name        : t3.cu
 Author      : Bagaev Dmitri
 Version     :
 Copyright   : 
 Description : Compute sum of reciprocals using STL on CPU and Thrust on GPU
 ============================================================================
 */

#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>
#include <cmath>
#include <chrono>
#include <thrust/reduce.h>
#include <thrust/device_vector.h>

#include "debug.h"
#include "problem.h"
#include "linalg.h"
#include "solver.h"

int main(int argc, char **argv) {

	debug::PrintDeviceInfo();

	auto U = [](double x, double y) -> double {
		return 1 + std::cos(M_PI * x * y);
	};

	auto F = [](double x, double y) -> double {
		return M_PI * M_PI * (x * x + y * y) * std::cos(M_PI * x * y);
	};

	auto psi = [](double x, double y) -> double {
		return 0.0;
	};

	const ProblemDomain<double> domain(1024, 1024, 0.0, 1.0, 0.0, 1.0);
	const Problem<double, double> problem(domain, U, F, U, psi);

	std::cout << domain << std::endl;

	// debug::PrintGridVector<double>(domain.M, domain.N, problem.U);

	Solver<double, double> solver(problem);

	auto t1 = std::chrono::high_resolution_clock::now();
	double norm = solver.solve(1e-6, 5000);
	auto t2 = std::chrono::high_resolution_clock::now();
	std::cout << "solver.solve() took "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
			<< " milliseconds\n";

	std::cout << norm << std::endl;

	return 0;
}
