/*
 * linalg.cuh
 *
 *  Created on: Dec 6, 2018
 *      Author: bvdmitri
 */

#ifndef LINALG_CUH_
#define LINALG_CUH_

#include <cmath>
#include <thrust/device_vector.h>

namespace linalg {

template<typename T>
struct ScalarSubtractAbsolutFunctor : public thrust::unary_function<thrust::tuple<T, T>, T> {
	__device__ T operator()(const thrust::tuple<T, T> &input) const {
		return (T) std::fabs(thrust::get<0>(input) - thrust::get<1>(input));
	}
};

template<typename T>
struct ScalarSquareFunctor : public thrust::unary_function<T, T> {
	__device__ T operator()(const T &input) const {
		return input * input;
	}
};

template<typename T>
struct ScalarMultiplicationFunctor : public thrust::unary_function<thrust::tuple<T, T>, T> {
	__device__ T operator()(const thrust::tuple<T, T> &input) const {
		return thrust::get<0>(input) * thrust::get<1>(input);
	}
};

template<typename T>
struct TwoScalarProductFunctor : public thrust::unary_function<thrust::tuple<T, T>, thrust::tuple<T, T>> {
	__device__ thrust::tuple<T, T> operator()(const thrust::tuple<T, T> &input) const {
		// [u, v] and [v, v]
		return thrust::make_tuple(thrust::get<0>(input) * thrust::get<1>(input), thrust::get<1>(input) * thrust::get<1>(input));
	}
};

template<typename T>
struct ThreeScalarProductFunctor : public thrust::unary_function<thrust::tuple<T, T>, thrust::tuple<T, T, T>> {
	__device__ thrust::tuple<T, T, T> operator()(const thrust::tuple<T, T> &input) const {
		// [u, u] [v, v] and [u, v]
		const double u = thrust::get<0>(input);
		const double v = thrust::get<1>(input);
		return thrust::make_tuple(u * u, v * v, u * v);
	}
};

template<typename T>
struct SumTupleFunctor : public thrust::binary_function<thrust::tuple<T, T>, thrust::tuple<T, T>, thrust::tuple<T, T>> {
	__device__ thrust::tuple<T, T> operator()(const thrust::tuple<T, T> &l, const thrust::tuple<T, T> &r) const {
		return thrust::make_tuple(thrust::get<0>(l) + thrust::get<0>(r), thrust::get<1>(l) + thrust::get<1>(r));
	}
};

template<typename T>
struct SumThreeTupleFunctor : public thrust::binary_function<thrust::tuple<T, T, T>, thrust::tuple<T, T, T>, thrust::tuple<T, T, T>> {
	__device__ thrust::tuple<T, T, T> operator()(const thrust::tuple<T, T, T> &l, const thrust::tuple<T, T, T> &r) const {
		return thrust::make_tuple(
				thrust::get<0>(l) + thrust::get<0>(r),
				thrust::get<1>(l) + thrust::get<1>(r),
				thrust::get<2>(l) + thrust::get<2>(r)
		);
	}
};

template<typename T>
struct ScalarMultiplicationSubtractFunctor : public thrust::unary_function<thrust::tuple<T, T>, T> {
	__device__ T operator()(const thrust::tuple<T, T> &input) const {
		T t1 = thrust::get<0>(input);
		T t2 = thrust::get<1>(input);
		return (t1 - t2) * (t1 - t2);
	}
};

// Function to compute scalar product in energy space for one vector
// Computed like [ u, u ] = sum(i = 0, M) * sum(j = 0, N) * (u_ij * u_ij * h1 * h2)
// Input arguments:
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u    - vector to compute scalar-product for
template<typename T>
T ScalarProduct_E(T h1, T h2, const thrust::device_vector<T> &u) {
	return h1 * h2 * thrust::transform_reduce(
			u.begin(),
			u.end(),
			ScalarSquareFunctor<T>(),
			(T) 0,
			thrust::plus<T>()
	);
};

// Function to compute scalar product in energy space for two vectors
// Computed like [ u, v ] = sum(i = 0, M) * sum(j = 0, N) * (u_ij * v_ij * h1 * h2)
// Input arguments:
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u,v  - vectors to compute scalar-product for
template<typename T>
T ScalarProduct_E(T h1, T h2, const thrust::device_vector<T> &u, const thrust::device_vector<T> &v) {
	return h1 * h2 * thrust::transform_reduce(
			thrust::make_zip_iterator(thrust::make_tuple(u.begin(), v.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(u.end(), v.end())),
			ScalarMultiplicationFunctor<T>(),
			(T) 0,
			thrust::plus<T>()
	);
};

// Function to compute two scalar products simultanuosly (used for t = [r, Ar] / [Ar, Ar])
// [After test] P.S. Seems to be slower that just two separate scalar products ?
// Input arguments:
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u,v  - vectors to compute scalar-product for
//   : [u, v] / [ v, v ]
template<typename T>
T DivideTwoScalarProduct_E(const thrust::device_vector<T> &u, const thrust::device_vector<T> &v) {
	const thrust::tuple<T, T> &r = thrust::transform_reduce(
			thrust::make_zip_iterator(thrust::make_tuple(u.begin(), v.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(u.end(), v.end())),
			TwoScalarProductFunctor<T>(),
			thrust::make_tuple(T (0), T(0)),
			SumTupleFunctor<T>()
	);
	return thrust::get<0>(r) / thrust::get<1>(r);
}

// Function to compute three scalar products simultanuosly (used for t = [r, Ar] / [Ar, Ar] and norm of r)
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u,v  - vectors to compute scalar-product for
//   : on out tuple of three T elements [u, u] [v, v] [u, v]
template<typename T>
thrust::tuple<T, T, T> Compute3ScalarProduct_E(const thrust::device_vector<T> &u, const thrust::device_vector<T> &v) {
	return thrust::transform_reduce(
			thrust::make_zip_iterator(thrust::make_tuple(u.begin(), v.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(u.end(), v.end())),
			ThreeScalarProductFunctor<T>(),
			thrust::make_tuple((T) 0, (T) 0, (T) 0),
			SumThreeTupleFunctor<T>()
	);
}




// Function to compute scalar product in energy space for subtraction of two vectors
// Computed like [ u, v ] = sum(i = 0, M) * sum(j = 0, N) * ((u_ij - v_ij)^2 * h1 * h2)
// Input arguments:
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u,v  - vectors to compute scalar-product for
template<typename T>
T ScalarProductSubtract_E(T h1, T h2, const thrust::device_vector<T> &u, const thrust::device_vector<T> &v) {
	return h1 * h2 * thrust::transform_reduce(
			thrust::make_zip_iterator(thrust::make_tuple(u.begin(), v.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(u.end(), v.end())),
			ScalarMultiplicationSubtractFunctor<T>(),
			(T) 0,
			thrust::plus<T>()
	);
};

// Function to compute norm in energy space for one vector
// Computed like ||u|| = sqrt([ u, u ])
// Input arguments:
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u    - vector to compute norm for
template<typename T>
T Norm_E(T h1, T h2, const thrust::device_vector<T> &u) {
	return std::sqrt(ScalarProduct_E(h1, h2, u));
}

// Function to compute norm in energy space for subtraction of two vectors
// Computed like ||u|| = sqrt([ u, u ])
// Input arguments:
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u    - vector to compute norm for
template<typename T>
T NormSubtract_E(T h1, T h2, const thrust::device_vector<T> &u, const thrust::device_vector<T> &v) {
	return std::sqrt(ScalarProductSubtract_E(h1, h2, u, v));
}

// Function to compute C norm of w = u - v vector
// Computed like ||w|| = max|w_ij|
// Input arguments:
//   : u   - vector to subtract from
//   : v   - vector to subtract
template<typename T>
T NormSubtract_C(const thrust::device_vector<T> &u, const thrust::device_vector<T> &v) {
	return thrust::transform_reduce(
			thrust::make_zip_iterator(thrust::make_tuple(u.begin(), v.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(u.end(), v.end())),
			ScalarSubtractAbsolutFunctor<T>(),
			(T) 0,
			thrust::maximum<T>()
	);
}

}


#endif /* LINALG_CUH_ */
