/*
 * debug.h
 *
 *  Created on: Dec 6, 2018
 *      Author: bvdmitri
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include <thrust/device_vector.h>

namespace debug {

template<typename T, class V>
void PrintGridVector(int M, int N, V &v) {
	thrust::host_vector<T> h = v;

	int k = 0;
	for (int i = 0; i < M; ++i) {
		std::cout << "[ ";
		for (int j = 0; j < N; ++j) {
			std::cout << h[k++] << " ";
		}
		std::cout << "]" << std::endl;
	}
}

void CudaSafeCheck(int error, const std::string& message, const std::string& file, int line) {
	if (error != cudaSuccess) {
		std::cerr << "CUDA Error: " << message << " (on line: " << file << ":" << line << ")" << std::endl;
		exit(-1);
	}
}

void PrintDeviceInfo() {
	int deviceCount;

	CudaSafeCheck(cudaGetDeviceCount(&deviceCount), "GetDeviceCount", __FILE__, __LINE__);

	std::cout << "Number of CUDA devices" << deviceCount << std::endl;

	for (int dev = 0; dev < deviceCount; dev++) {
		cudaDeviceProp deviceProp;

		CudaSafeCheck(cudaGetDeviceProperties(&deviceProp, dev), "Get Device Properties", __FILE__, __LINE__);

		if (dev == 0) {
			if (deviceProp.major == 9999 && deviceProp.minor == 9999) {
				std::cout << "No CUDA GPU has been detected" << std::endl;
				return;
			} else if (deviceCount == 1) {
				std::cout << "There is 1 device supporting CUDA"  << std::endl;
			} else {
				std::cout << "There are %d devices supporting CUDA: " << deviceCount << std::endl;
			}
		}

		std::cout << "For device #" << dev << std::endl
		 	 	  << "Device name:                " << deviceProp.name << std::endl
		 	 	  << "Major revision number:      " << deviceProp.major << std::endl
		 	 	  << "Minor revision number:      " << deviceProp.minor << std::endl
		 	 	  << "Total Global Memory:        " << deviceProp.totalGlobalMem << std::endl
		 	 	  << "Total shared mem per block: " << deviceProp.sharedMemPerBlock << std::endl
		 	 	  << "Total const mem size:       " << deviceProp.totalConstMem << std::endl
		 	 	  << "Warp size:                  " << deviceProp.warpSize << std::endl
		 	 	  << "Maximum block dimensions:   "
		 	 	  << deviceProp.maxThreadsDim[0] << "x"
		 	 	  << deviceProp.maxThreadsDim[1] << "x"
				  << deviceProp.maxThreadsDim[2] << std::endl
				  << "Maximum grid dimensions:    "
				  << deviceProp.maxGridSize[0] << "x"
				  << deviceProp.maxGridSize[1] << "x"
				  << deviceProp.maxGridSize[2] << std::endl
				  << "Clock Rate:                 " << deviceProp.clockRate << std::endl
				  << "Number of multiprocessors:  " << deviceProp.multiProcessorCount << std::endl;

	}
}

}

#endif /* DEBUG_H_ */
