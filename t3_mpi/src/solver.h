/*
 * solver.h
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#ifndef SOLVER_H_
#define SOLVER_H_

#include <thrust/device_vector.h>
#include <thrust/sequence.h>
#include <cmath>

#include "gridvector.h"
#include "linalg.h"
#include "problem.h"
#include "stats.h"

__constant__ int istart, iend;
__constant__ int jstart, jend;
__constant__ int ilocal, jlocal;
__constant__ int M, N;
__constant__ double h1, h2;
__constant__ double invh1_2, invh2_2, inv2h1_2, inv2h2_2;
__constant__ double inv2h1, inv2h2;

// Functor to compute r = A w
// Input arguments:
//   : M      - number of grid points on x-axis
//   : N      - number of grid points on y-axis
//   : h1     - grid step on x-axis
//   : h2     - grid step on y-axis
//   : w      - Device vector to multiply on
//   : r      - Device vector to store result in
struct ComputeAwFunctor : public thrust::unary_function<int, double> {
	const double * const w;
	const double * const iendcolumn;
	const double * const istartm1column;
	const double * const jendrow;
	const double * const jstartm1row;

	ComputeAwFunctor(const GridVector &v, const GridVectorHaloPoints &halo):
		w(thrust::raw_pointer_cast(v.getv().data())),
		iendcolumn(thrust::raw_pointer_cast(halo.iendcolumn.data())), istartm1column(thrust::raw_pointer_cast(halo.istartm1column.data())),
		jendrow(thrust::raw_pointer_cast(halo.jendrow.data())), jstartm1row(thrust::raw_pointer_cast(halo.jstartm1row.data())) {}

	__device__ double operator()(const int k) const {
		int gi = istart + k % ilocal;   // global i index
		int gj = jstart + k / ilocal;   // global j index

		double wij   = w[k];
		double wip1j = (gi == M - 1) ? 0.0 : (gi == iend - 1) ? iendcolumn[gj - jstart] : w[k + 1];
		double wijp1 = (gj == N - 1) ? 0.0 : (gj == jend - 1) ? jendrow[gi - istart] : w[k + ilocal];


		if ((gi >= 1 && gi < M) && (gj >= 1 && gj < N)) { // Check if current point is inner point of grid
			// Here we have the following formula
			// -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
			double wim1j = (gi == istart) ? istartm1column[gj - jstart] : w[k - 1];
			double wijm1 = (gj == jstart) ? jstartm1row[gi - istart] : w[k - ilocal];
			return invh1_2 * (wip1j - 2.0 * wij + wim1j) + invh2_2 * (wijp1 - 2.0 * wij + wijm1);

		} else if (gi == 0 && gj != 0) { // Check if current point on Left border
			// Here we have the following formula
			// -2 / h1^2 (w_i+1_j - w_i_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
			// i = 0
			// -2 / h1^2 (w1j - w0j) - 1 / h2^2 (w0j+1 - 2w0j + w0j-1)
			double w0jm1 = (gj == jstart) ? jstartm1row[gi - istart] : w[k - ilocal];
			return inv2h1_2 * (wip1j - wij) + invh2_2 * (wijp1 - 2.0 * wij + w0jm1);

		} else if (gj == 0 && gi != 0) { // Check if current point on Bottom border
			// Here we have the following formula
			// -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 2 / h^2 (w_i_j+1 - w_i_j)
			// j = 0
			// -1 / h1^2 (w_i+1_0 - 2wi0 + w_i-1_0) - 2 / h^2 (wi1 - wi0)
			double wim10 = (gi == istart) ? istartm1column[gj - jstart] : w[k - 1];
			return invh1_2 * (wip1j - 2.0 * wij + wim10) + inv2h2_2 * (wijp1 - wij);

		} else if (gi == 0 && gj == 0) { // Check if current point on Left-Bottom point
			// Here we have the following formula
			// -2 / h1^2 (w_i+1_j - w_i_j) - 2 / h2^2 (w_i_j+1 - w_i_j)
			// i = 0 and j = 0
			// -2 / h1^2 (w10 - w00) - 2 / h2^2 (w01 - w00)
			return inv2h1_2 * (wip1j - wij) + inv2h2_2 * (wijp1 - wij);
		}

		return 0.0;
	}
};

// Functor to compute r = A w - B
struct ComputeAwmBFunctor : public thrust::unary_function<int, double> {
	const double * const w;
	const double * const iendcolumn;
	const double * const istartm1column;
	const double * const jendrow;
	const double * const jstartm1row;
	const double * const f;
	const double * const dirichleT;
	const double * const dirichleR;
	const double * const neumannB;
	const double * const neumannL;

	ComputeAwmBFunctor(const GridVector &v, const GridVectorHaloPoints &halo, const thrust::device_vector<double> &f,
			const thrust::device_vector<double> &dirichleT, const thrust::device_vector<double> &dirichleR,
			const thrust::device_vector<double> &neumannB, const thrust::device_vector<double> &neumannL):
				w(thrust::raw_pointer_cast(v.getv().data())),
				iendcolumn(thrust::raw_pointer_cast(halo.iendcolumn.data())), istartm1column(thrust::raw_pointer_cast(halo.istartm1column.data())),
				jendrow(thrust::raw_pointer_cast(halo.jendrow.data())), jstartm1row(thrust::raw_pointer_cast(halo.jstartm1row.data())),
				f(thrust::raw_pointer_cast(f.data())),
				dirichleT(thrust::raw_pointer_cast(dirichleT.data())),
				dirichleR(thrust::raw_pointer_cast(dirichleR.data())),
				neumannB(thrust::raw_pointer_cast(neumannB.data())),
				neumannL(thrust::raw_pointer_cast(neumannL.data())) {}

	__device__ double operator()(const int k) const {
		int gi = istart + k % ilocal;   // global i index
		int gj = jstart + k / ilocal;   // global j index

		double wij   = w[k];
		double wip1j = (gi == M - 1) ? 0.0 : (gi == iend - 1) ? iendcolumn[gj - jstart] : w[k + 1];
		double wijp1 = (gj == N - 1) ? 0.0 : (gj == jend - 1) ? jendrow[gi - istart] : w[k + ilocal];

		double b = f[k];

		if (gi == M - 1) {
			b -= invh1_2 * dirichleR[gj];
		}

		if (gj == N - 1) {
			b -= invh2_2 * dirichleT[gi];
		}


		if ((gi >= 1 && gi < M) && (gj >= 1 && gj < N)) { // Check if current point is inner point of grid

			// Here we have the following formula
			// -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
			double wim1j = (gi == istart) ? istartm1column[gj - jstart] : w[k - 1];
			double wijm1 = (gj == jstart) ? jstartm1row[gi - istart] : w[k - ilocal];
			return invh1_2 * (wip1j - 2.0 * wij + wim1j) + invh2_2 * (wijp1 - 2.0 * wij + wijm1) - b;

		} else if (gi == 0 && gj != 0) { // Check if current point on Left border

			// Here we have the following formula
			// -2 / h1^2 (w_i+1_j - w_i_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
			// i = 0
			// -2 / h1^2 (w1j - w0j) - 1 / h2^2 (w0j+1 - 2w0j + w0j-1)
			double w0jm1 = (gj == jstart) ? jstartm1row[gi - istart] : w[k - ilocal];
			return inv2h1_2 * (wip1j - wij) + invh2_2 * (wijp1 - 2.0 * wij + w0jm1) + inv2h1 * neumannL[gj] - b;

		} else if (gj == 0 && gi != 0) { // Check if current point on Bottom border

			// Here we have the following formula
			// -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 2 / h^2 (w_i_j+1 - w_i_j)
			// j = 0
			// -1 / h1^2 (w_i+1_0 - 2wi0 + w_i-1_0) - 2 / h^2 (wi1 - wi0)

			double wim10 = (gi == istart) ? istartm1column[gj - jstart] : w[k - 1];
			return invh1_2 * (wip1j - 2.0 * wij + wim10) + inv2h2_2 * (wijp1 - wij) + inv2h2 * neumannB[gi] - b;

		} else if (gi == 0 && gj == 0) { // Check if current point on Left-Bottom point

			// Here we have the following formula
			// -2 / h1^2 (w_i+1_j - w_i_j) - 2 / h2^2 (w_i_j+1 - w_i_j)
			// i = 0 and j = 0
			// -2 / h1^2 (w10 - w00) - 2 / h2^2 (w01 - w00)

			// Exact neumann in Left-Bottom point
			double neuman_exact = 0.0;
			return inv2h1_2 * (wip1j - wij) + inv2h2_2 * (wijp1 - wij) + neuman_exact - b;

		}

		return 0.0;
	}
};

struct ComputewmtrFunctor : public thrust::unary_function<const thrust::tuple<double, double>, double> {
	const double t;

	ComputewmtrFunctor(double t): t(t) {}

	__device__ double operator()(const thrust::tuple<double, double> &e) const {
		return thrust::get<0>(e) - t * thrust::get<1>(e);
	}
};


class Solver {
private:
	const MPIEnvironment &env;
	const Problem &problem;
	TimeStats &stats;

public:
	Solver(const MPIEnvironment &env, const Problem &problem, TimeStats &stats): env(env), problem(problem), stats(stats) {}

	double solve(double eps, int max_iterations, int print_it) const {
		fillInConstants();

		thrust::device_vector<int> indices(env.ilocal * env.jlocal);
		thrust::sequence(indices.begin(), indices.end());

		GridVector w1(env, 0.0);
		GridVector w2(env, 0.0);
		GridVector r(env, 0.0);
		GridVector Ar(env, 0.0);

		GridVectorHaloPoints halo(env);
		GridVectorHaloSentBuffer buffer(env);

		const ProblemDomain &d = problem.domain;

		int iter = 0;
		double norm = 0.0;
		while (iter < max_iterations) {
			const GridVector &current = iter % 2 == 0 ? w1 : w2;
			GridVector &next = iter % 2 == 1 ? w1 : w2;

			computeAwmB(indices, current, halo, buffer, r);
			computeAw(indices, r, halo, buffer, Ar);

			stats.scpr.start();
			const thrust::tuple<double, double, double> products = linalg::Compute3ScalarProduct_E(env, r.getv(), Ar.getv());
			stats.scpr.end();

			double t = thrust::get<2>(products) / thrust::get<1>(products);
			norm = std::sqrt(d.h1 * d.h2 * thrust::get<0>(products));
			computewmtr(current, r, t, next);

			if (env.rank == 0 && iter % print_it == 0) {
				std::cout << iter << " " << norm << " " << t << std::endl;
			}

			if (norm < eps) {
				break;
			}

			iter += 1;
		}

		stats.scpr.start();
		std::cout << "E norm of r       " << norm << std::endl;
		std::cout << "E norm of error:  " << linalg::NormSubtract_E(d.h1, d.h2, problem.U.getv(), (iter % 2 == 1 ? w1 : w2).getv()) << std::endl;
		std::cout << "C norm of error:  " << linalg::NormSubtract_E(d.h1, d.h2, problem.U.getv(), (iter % 2 == 1 ? w1 : w2).getv()) << std::endl;
		std::cout << "Iterations count: " << iter << std::endl;
		stats.scpr.end();

		return norm;
	}

private:

	void computeAw(const thrust::device_vector<int> &indices,
			const GridVector &v, GridVectorHaloPoints &halo, GridVectorHaloSentBuffer &buffer,
			GridVector &out) const {
		halo.sync(env, v, buffer, stats);
		stats.Ar.start();
		thrust::transform(indices.begin(), indices.end(), out.getvnoc().begin(), ComputeAwFunctor(v, halo));
		stats.Ar.end();
	}

	void computeAwmB(const thrust::device_vector<int> &indices,
			const GridVector &v, GridVectorHaloPoints &halo, GridVectorHaloSentBuffer &buffer,
			GridVector& out) const {
		halo.sync(env, v, buffer, stats);
		stats.r.start();
		thrust::transform(indices.begin(), indices.end(), out.getvnoc().begin(),
				ComputeAwmBFunctor(v, halo, problem.F.getv(), problem.dirichleT, problem.dirichleR, problem.neumannB, problem.neumannL)
		);
		stats.r.end();
	}

	void computewmtr(const GridVector &w, const GridVector &r, double t, GridVector &out) const {
		stats.wmtr.start();
		thrust::transform(
				thrust::make_zip_iterator(thrust::make_tuple(w.getv().begin(), r.getv().begin())),
				thrust::make_zip_iterator(thrust::make_tuple(w.getv().end(), r.getv().end())),
				out.getvnoc().begin(),
				ComputewmtrFunctor(t)
		);
		stats.wmtr.end();
	}

	void fillInConstants() const {
		const ProblemDomain &d = problem.domain;
		cudaMemcpyToSymbol(M, &(d.M), sizeof(int), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(N, &(d.N), sizeof(int), 0, cudaMemcpyHostToDevice);

		cudaMemcpyToSymbol(istart, &(env.istart), sizeof(int), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(iend, &(env.iend), sizeof(int), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(jstart, &(env.jstart), sizeof(int), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(jend, &(env.jend), sizeof(int), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(ilocal, &(env.ilocal), sizeof(int), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(jlocal, &(env.jlocal), sizeof(int), 0, cudaMemcpyHostToDevice);

		double p_h1 = d.h1;
		double p_h2 = d.h2;
		double p_invh1_2 = -1.0 / (d.h1 * d.h1);
		double p_invh2_2 = -1.0 / (d.h2 * d.h2);
		double p_inv2h1_2 = -2.0 / (d.h1 * d.h1);
		double p_inv2h2_2 = -2.0 / (d.h2 * d.h2);
		double p_inv2h1 = 2.0 / d.h1;
		double p_inv2h2 = 2.0 / d.h2;

		cudaMemcpyToSymbol(h1, &(p_h1), sizeof(double), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(h2, &(p_h2), sizeof(double), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(invh1_2, &(p_invh1_2), sizeof(double), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(invh2_2, &(p_invh2_2), sizeof(double), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(inv2h1_2, &(p_inv2h1_2), sizeof(double), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(inv2h2_2, &(p_inv2h2_2), sizeof(double), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(inv2h1, &(p_inv2h1), sizeof(double), 0, cudaMemcpyHostToDevice);
		cudaMemcpyToSymbol(inv2h2, &(p_inv2h2), sizeof(double), 0, cudaMemcpyHostToDevice);
	}
};

#endif /* SOLVER_H_ */
