/*
 * debug.h
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#ifndef DEBUG_H_
#define DEBUG_H_

namespace debug {

void printDeviceCheck() {
	int ngpus;
	cudaGetDeviceCount(&ngpus);

	std::vector<cudaDeviceProp> gpuprops(ngpus);
	for (int i = 0; i < ngpus; ++i) {
		cudaGetDeviceProperties(&gpuprops[i], i);
	}

	// check results
	std::cout <<
			"--------------------------------------------------------------------------------"
			<< std::endl;
	std::cout << "Devices: " << std::endl;

	bool is_fermi = true;
	bool has_uva = true;
	for (int i = 0; i < ngpus; ++i) {
		std::cout << "    " <<
				gpuprops[i].name << " " <<
				gpuprops[i].major << " " <<
				gpuprops[i].unifiedAddressing <<
				gpuprops[i].pciBusID << " " <<
				gpuprops[i].pciDeviceID << std::endl;
		is_fermi &= (gpuprops[i].major >= 2);  // must be fermi or newer
		has_uva &= (gpuprops[i].unifiedAddressing);
	}

	if (ngpus >= 2) {
		// TODO: only works for ngpus == 2
		int access1from0;
		int access0from1;
		cudaDeviceCanAccessPeer(&access1from0, 1, 0);
		cudaDeviceCanAccessPeer(&access0from1, 0, 1);
		bool same_complex = (access1from0 && access0from1);

		std::cout << "Peer access: " << std::endl <<
				"    access 1 from 0: " << access1from0 << std::endl <<
				"    access 0 from 1: " << access0from1 << std::endl;

		std::cout << "General info: " << std::endl <<
				"    num devices? " << ngpus << std::endl <<
				"    is fermi? " << is_fermi << std::endl <<
				"    has uva? " << has_uva << std::endl <<
				"    same complex? " << same_complex << std::endl;
		std::cout <<
				"--------------------------------------------------------------------------------"
				<< std::endl;
	}
}

}



#endif /* DEBUG_H_ */
