/*
 * problem.h
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#ifndef PROBLEM_H_
#define PROBLEM_H_

#include <thrust/device_vector.h>
#include <iostream>
#include <functional>

#include "mpitools.h"
#include "gridvector.h"

class ProblemDomain {
public:
	const int M, N;
	const double A1, A2, B1, B2;
	const double h1, h2;

	ProblemDomain(int M, int N, double A1, double A2, double B1, double B2);

	friend std::ostream& operator<<(std::ostream& os, const ProblemDomain& d);
};

struct ApplyGridLambda {
	const int M, N;
	const double A1, B1;
	const double h1, h2;
	const std::function<double(double, double)> &lambda;

	ApplyGridLambda(int M, int N, double A1, double B1, double h1, double h2, const std::function<double (double, double)> &lambda):
		M(M), N(N), A1(A1), B1(B1), h1(h1), h2(h2), lambda(lambda) {}

	__host__ double operator()(const int &k) const;
};

struct ApplyGridBorderXLambda  {
	const double from, h, Y;
	const std::function<double (double, double)> &lambda;

	ApplyGridBorderXLambda(double from, double h, double Y, const std::function<double (double, double)> &lambda): from(from), h(h), Y(Y), lambda(lambda) {}

	__host__ double operator()(const int &k) const;
};

struct ApplyGridBorderYLambda {
	const double from, h, X;
	const std::function<double (double, double)> &lambda;

	ApplyGridBorderYLambda(double from, double h, double X, const std::function<double (double, double)> &lambda): from(from), h(h), X(X), lambda(lambda) {}

	__host__ double operator()(const int &k) const;
};

class Problem {
public:
	const ProblemDomain      &domain;
	const GridVector U;
	const GridVector F;

	thrust::device_vector<double> dirichleT;
	thrust::device_vector<double> dirichleR;
	thrust::device_vector<double> neumannL;
	thrust::device_vector<double> neumannB;
public:
	Problem(const MPIEnvironment &env, const ProblemDomain &d,
			const std::function<double (double, double)> &u, const std::function<double (double, double)> &f,
			const std::function<double (double, double)> &dirichle, const std::function<double (double, double)> &neumann);
};

#endif /* PROBLEM_H_ */
