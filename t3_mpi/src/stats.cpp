/*
 * stats.cpp
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#include <chrono>
#include "stats.h"

TimeStatsTimer::TimeStatsTimer(): ms(0) {}

void TimeStatsTimer::start() {
	tmp = std::chrono::high_resolution_clock::now();
}

void TimeStatsTimer::end() {
	ms += std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - tmp).count();
}

unsigned long long int TimeStatsTimer::time() const {
	return ms;
}

TimeStats::TimeStats() {}

std::ostream& operator<<(std::ostream& os, const TimeStats& t) {
	os << "Timings" << std::endl;
	os <<
			"Program\t" <<
			"Init\t" <<
			"Solver\t" <<
			"Send\t" <<
			"Receive\t" <<
			"r\t" <<
			"Ar\t" <<
			"wmtr\t" <<
			"scpr\t" <<
			std::endl;
	os <<
			t.program.time() << "\t" <<
			t.init.time() << "\t" <<
			t.solver.time() << "\t" <<
			t.send.time() << "\t" <<
			t.receive.time() << "\t" <<
			t.r.time() << "\t" <<
			t.Ar.time() << "\t" <<
			t.wmtr.time() << "\t" <<
			t.scpr.time() << "\t" <<
			std::endl;
	return os;
}
