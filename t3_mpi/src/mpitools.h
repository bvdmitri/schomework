/*
 * mpitools.h
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#ifndef MPITOOLS_H_
#define MPITOOLS_H_

#include <mpi.h>

class MPIEnvironment {
public:
	MPI_Comm comm;   // communicator with cartesian structure
	int      size;   // size of communicator
	int      rank;   // rank of current process
	int      xcoord; // xcoord of current process
	int      ycoord; // ycoord of current process
	int      xsize;  // size of communicator by x-axis
	int      ysize;  // size of communicator by y-axis

	int rtop;     // rank of 'top' process
	int rleft;    // rank of 'left' process
	int rbottom;  // rank of 'bottom' process
	int rright;   // rank of 'right' process

	int ilocal, jlocal;
	int iglobal, jglobal;
	int istart, iend;
	int jstart, jend;

	MPIEnvironment(int IM, int IN);

	void print() const;

	~MPIEnvironment();
};

#endif /* MPITOOLS_H_ */
