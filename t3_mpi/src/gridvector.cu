/*
 * gridvector.cpp
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#include <thrust/host_vector.h>
#include <thrust/sequence.h>
#include <mpi.h>
#include <assert.h>
#include <cmath>
#include <algorithm>

#include "gridvector.h"

GridVector::GridVector(const MPIEnvironment &env, double d) {
	this->istart  = env.istart;
	this->iend    = env.iend;
	this->jstart  = env.jstart;
	this->jend    = env.jend;
	this->iglobal = env.iglobal;
	this->jglobal = env.jglobal;
	this->ilocal  = env.ilocal;
	this->jlocal  = env.jlocal;

	int    vcount  = (iend - istart) * (jend - jstart);
	this->values = thrust::device_vector<double>(vcount);

	thrust::fill(this->values.begin(), this->values.end(), d);
}

GridVector::GridVector(const MPIEnvironment &env,
		double A1, double B1, double h1, double h2,
		const std::function<double(double, double)> &f) {

	this->istart  = env.istart;
	this->iend    = env.iend;
	this->jstart  = env.jstart;
	this->jend    = env.jend;
	this->iglobal = env.iglobal;
	this->jglobal = env.jglobal;
	this->ilocal  = env.ilocal;
	this->jlocal  = env.jlocal;

	int    vcount  = (iend - istart) * (jend - jstart);
	this->values = thrust::device_vector<double>(vcount);

	thrust::host_vector<double> tmp(vcount);

	for (int k = 0; k < ilocal * jlocal; ++k) {
		int gi = istart + k % ilocal;   // global i index
		int gj = jstart + k / ilocal;   // global j index

		double x = A1 + gi * h1;
		double y = B1 + gj * h2;

		tmp[k] = f(x, y);
	}
	thrust::copy(tmp.begin(), tmp.end(), this->values.begin());
}

const thrust::device_vector<double>& GridVector::getv() const {
	return values;
}

thrust::device_vector<double>& GridVector::getvnoc() {
	return values;
}

void GridVector::print() const {
	thrust::host_vector<double> h = values;

	int k = 0;
	for (int i = 0; i < jlocal; ++i) {
		std::cout << "[ ";
		for (int j = 0; j < ilocal; ++j) {
			std::cout << h[k++] << " ";
		}
		std::cout << "]" << std::endl;
	}
}

GridVector::~GridVector() {
	// TODO Auto-generated destructor stub
}

struct FillColumnIndices : public thrust::unary_function<unsigned int, unsigned int> {
	unsigned int local;

	FillColumnIndices(unsigned int local): local(local) {}

	__device__ unsigned int operator()(const unsigned int k) const {
		return k * local;
	}
};

struct ExtractColumn : public thrust::unary_function<unsigned int, double> {
	unsigned int shift;
	const double * const w;

	ExtractColumn(unsigned int shift, const thrust::device_vector<double> v): shift(shift), w(thrust::raw_pointer_cast(v.data())) {}

	__device__ double operator()(const unsigned int k) const {
		return w[k + shift];
	}
};

GridVectorHaloSentBuffer::GridVectorHaloSentBuffer(const MPIEnvironment &env):
																																	icolumnindices(thrust::device_vector<unsigned int>(env.jlocal)),
#ifndef USE_MCA
																																	icolumntmp(thrust::device_vector<double>(env.jlocal)),
																																	istartrequest(NULL), iendm1request(NULL), jstartrequest(NULL), jendm1request(NULL),
																																	istartcolumn(thrust::host_vector<double>(env.jlocal)), iendm1column(thrust::host_vector<double>(env.jlocal)),
																																	jstartrow(thrust::host_vector<double>(env.ilocal)), jendm1row(thrust::host_vector<double>(env.ilocal))
#else
istartrequest(NULL), iendm1request(NULL), jstartrequest(NULL), jendm1request(NULL),
istartcolumn(thrust::device_vector<double>(env.jlocal)), iendm1column(thrust::device_vector<double>(env.jlocal)),
jstartrow(thrust::device_vector<double>(env.ilocal)), jendm1row(thrust::device_vector<double>(env.ilocal))
#endif
{
	thrust::device_vector<unsigned int> sequence(env.jlocal);
	thrust::sequence(sequence.begin(), sequence.end());
	thrust::transform(sequence.begin(), sequence.end(), icolumnindices.begin(), FillColumnIndices(env.ilocal));
}

GridVectorHaloSentBuffer::~GridVectorHaloSentBuffer() {
	MPI_Status status;

	if (iendm1request != NULL) {
		MPI_Wait(&iendm1request, &status);
	}

	if (istartrequest != NULL) {
		MPI_Wait(&istartrequest, &status);
	}

	if (jendm1request != NULL) {
		MPI_Wait(&jendm1request, &status);
	}

	if (jstartrequest != NULL) {
		MPI_Wait(&jstartrequest, &status);
	}
}

GridVectorHaloPoints::GridVectorHaloPoints(const MPIEnvironment &env) {

	if (env.rright != -1) {
		this->iendcolumn = thrust::device_vector<double>(env.jlocal);
	}

	if (env.rleft != -1) {
		this->istartm1column = thrust::device_vector<double>(env.jlocal);
	}

	if (env.rtop != -1) {
		this->jendrow = thrust::device_vector<double>(env.ilocal);
	}

	if (env.rbottom != -1) {
		this->jstartm1row = thrust::device_vector<double>(env.ilocal);
	}

	this->ilocal = env.ilocal;
	this->jlocal = env.jlocal;
}

void GridVectorHaloPoints::print() const {
	thrust::host_vector<double> tmp = iendcolumn;
	std::cout << "iendcolumn: ";
	std::for_each(tmp.begin(),tmp.end(), [] (const double d) {std::cout << d << " ";} );
	std::cout << std::endl;
	tmp = istartm1column;
	std::cout << "istartm1column: ";
	std::for_each(tmp.begin(),tmp.end(), [] (const double d) {std::cout << d << " ";} );
	std::cout << std::endl;
	tmp = jendrow;
	std::cout << "jendrow: ";
	std::for_each(tmp.begin(),tmp.end(), [] (const double d) {std::cout << d << " ";} );
	std::cout << std::endl;
	tmp = jstartm1row;
	std::cout << "jstartm1row: ";
	std::for_each(tmp.begin(),tmp.end(), [] (const double d) {std::cout << d << " ";} );
	std::cout << std::endl;

}

#ifdef USE_ASYNC_COMMUNICATION
void GridVectorHaloPoints::sync(const MPIEnvironment &env, const GridVector &v, GridVectorHaloSentBuffer &buffer, TimeStats &stats) {
	assert(v.ilocal == ilocal && v.jlocal == jlocal);

	MPI_Status status;

	// Attempt to send istart column (by j) to 'rleft' process (for it it will be 'iend' column (by j))
	// Note: assuming what previous send has been completed
	stats.send.start();
	if (v.istart != 0) {
		assert(env.xcoord != 0 && env.rleft != -1);

		if (buffer.istartrequest != NULL) {
			MPI_Wait(&(buffer.istartrequest), &status);
		}

		//		for (int j = 0; j < v.jlocal; ++j) {
		//			buffer.istartcolumn[j] = v.values[j * v.ilocal];
		//		}
#ifndef USE_MCA
		thrust::transform(buffer.icolumnindices.begin(), buffer.icolumnindices.end(), buffer.icolumntmp.begin(), ExtractColumn(0, v.getv()));
		thrust::copy(buffer.icolumntmp.begin(), buffer.icolumntmp.end(), buffer.istartcolumn.begin());
#else
		thrust::transform(buffer.icolumnindices.begin(), buffer.icolumnindices.end(), buffer.istartcolumn.begin(), ExtractColumn(0, v.getv()));
#endif

		MPI_Isend(thrust::raw_pointer_cast(buffer.istartcolumn.data()), v.jlocal, MPI_DOUBLE, env.rleft, IENDCOLUMN_SYNC_TAG, env.comm, &(buffer.istartrequest));
	}

	// Attempt to send iend - 1 column to 'rright' process (for it it will be 'istart - 1' column (by j))
	// Note: assuming what previous send has been completed
	if (v.iend != v.iglobal) {
		assert(env.xcoord != env.xsize - 1 && env.rright != -1);

		if (buffer.iendm1request != NULL) {
			MPI_Wait(&(buffer.iendm1request), &status);
		}

		//		for (int j = 0; j < v.jlocal; ++j) {
		//			buffer.iendm1column[j] = v.values[j * v.ilocal + (v.ilocal - 1)];
		//		}

#ifndef USE_MCA
		thrust::transform(buffer.icolumnindices.begin(), buffer.icolumnindices.end(), buffer.icolumntmp.begin(), ExtractColumn(v.ilocal - 1, v.getv()));
		thrust::copy(buffer.icolumntmp.begin(), buffer.icolumntmp.end(), buffer.iendm1column.begin());
#else
		thrust::transform(buffer.icolumnindices.begin(), buffer.icolumnindices.end(), buffer.iendm1column.begin(), ExtractColumn(v.ilocal - 1, v.getv()));
#endif

		MPI_Isend(thrust::raw_pointer_cast(buffer.iendm1column.data()), v.jlocal, MPI_DOUBLE, env.rright, ISTARTM1COLUMN_SYNC_TAG, env.comm, &(buffer.iendm1request));
	}

	// Attempt to send jstart row (by i) to 'rbottom' process (for it it will be 'jend' row (by i))
	// Note: assuming what previous send has been completed
	if (v.jstart != 0) {
		assert(env.ycoord != 0 && env.rbottom != -1);

		if (buffer.jstartrequest != NULL) {
			MPI_Wait(&(buffer.jstartrequest), &status);
		}

		thrust::copy_n(v.values.begin(), v.ilocal, buffer.jstartrow.begin());
		//		for (int i = 0; i < v.ilocal; ++i) {
		//			buffer.jstartrow[i] = v.values[i];
		//		}

		MPI_Isend(thrust::raw_pointer_cast(buffer.jstartrow.data()), v.ilocal, MPI_DOUBLE, env.rbottom, JENDROW_SYNC_TAG, env.comm, &(buffer.jstartrequest));
	}

	// Attempt to send jend - 1 column to 'rtop' process (for it it will be 'jstart - 1' row (by i))
	if (v.jend != v.jglobal) {
		assert(env.ycoord != env.ysize - 1 && env.rtop != -1);

		if (buffer.jendm1request != NULL) {
			MPI_Wait(&(buffer.jendm1request), &status);
		}

		thrust::copy_n(v.values.begin() + v.ilocal * (v.jlocal - 1), v.ilocal, buffer.jendm1row.begin());
		//		for (int i = 0; i < w->ilocal; ++i) {
		//			buffer->jendm1row[i] = w->values[i + w->ilocal * (w->jlocal - 1)];
		//		}

		MPI_Isend(thrust::raw_pointer_cast(buffer.jendm1row.data()), v.ilocal, MPI_DOUBLE, env.rtop, JSTARTM1ROW_SYNC_TAG, env.comm, &(buffer.jendm1request));
	}
	stats.send.end();

	stats.receive.start();
#ifndef USE_MCA
	thrust::host_vector<double> tmp(std::max(v.ilocal, v.jlocal));
#endif
	// Attempt to receive istart column (by j) from 'rright' process (for current rank it will be 'iend' (by j))
	if (v.iend != v.iglobal) {
		assert(env.xcoord != env.xsize - 1 && env.rright != -1);
#ifndef USE_MCA
		MPI_Recv(thrust::raw_pointer_cast(tmp.data()), v.jlocal, MPI_DOUBLE, env.rright, IENDCOLUMN_SYNC_TAG, env.comm, &status);
		thrust::copy_n(tmp.begin(), v.jlocal, iendcolumn.begin());
#else
		MPI_Recv(thrust::raw_pointer_cast(iendcolumn.data()), v.jlocal, MPI_DOUBLE, env.rright, IENDCOLUMN_SYNC_TAG, env.comm, &status);
#endif
	}

	// Attempt to receive iend - 1 column (by j) from 'rleft' process (for current rank it will be 'istart - 1' (by j))
	if (v.istart != 0) {
		assert(env.xcoord != 0 && env.rleft != -1);
#ifndef USE_MCA
		MPI_Recv(thrust::raw_pointer_cast(tmp.data()), v.jlocal, MPI_DOUBLE, env.rleft, ISTARTM1COLUMN_SYNC_TAG, env.comm, &status);
		thrust::copy_n(tmp.begin(), v.jlocal, istartm1column.begin());
#else
		MPI_Recv(thrust::raw_pointer_cast(istartm1column.data()), v.jlocal, MPI_DOUBLE, env.rleft, ISTARTM1COLUMN_SYNC_TAG, env.comm, &status);
#endif
	}

	// Attempt to receive jstart row (by i) from 'rtop' (for current rank it will be 'jend' (by i))
	if (v.jend != v.jglobal) {
		assert(env.ycoord != env.ysize - 1 && env.rtop != -1);
#ifndef USE_MCA
		MPI_Recv(thrust::raw_pointer_cast(tmp.data()), v.ilocal, MPI_DOUBLE, env.rtop, JENDROW_SYNC_TAG, env.comm, &status);
		thrust::copy_n(tmp.begin(), v.ilocal, jendrow.begin());
#else
		MPI_Recv(thrust::raw_pointer_cast(jendrow.data()), v.ilocal, MPI_DOUBLE, env.rtop, JENDROW_SYNC_TAG, env.comm, &status);
#endif
	}

	// Attempt to receive 'jend-1' row (by i) from 'rbottom' (for current rank it will be 'jstart-1' (by i))
	if (v.jstart != 0) {
		assert(env.ycoord != 0 && env.rbottom != -1);
#ifndef USE_MCA
		MPI_Recv(thrust::raw_pointer_cast(tmp.data()), v.ilocal, MPI_DOUBLE, env.rbottom, JSTARTM1ROW_SYNC_TAG, env.comm, &status);
		thrust::copy_n(tmp.begin(), v.ilocal, jstartm1row.begin());
#else
		MPI_Recv(thrust::raw_pointer_cast(jstartm1row.data()), v.ilocal, MPI_DOUBLE, env.rbottom, JSTARTM1ROW_SYNC_TAG, env.comm, &status);
#endif
	}
	stats.receive.end();
}
#else

void __private_sync_halo(int source, int destination, const double *send, double *recv, int size, MPI_Comm comm) {
	assert(source != -1 && destination != -1);
	MPI_Sendrecv(send, size, MPI_DOUBLE, destination, 0, recv, size, MPI_DOUBLE, destination, 0, comm, MPI_STATUS_IGNORE);
}

void GridVectorHaloPoints::sync(const MPIEnvironment &env, const GridVector &v, GridVectorHaloSentBuffer &buffer, TimeStats &stats) {
	stats.send.start();
	stats.receive.start();

#ifndef USE_MCA
	thrust::host_vector<double> tmp(std::max(v.ilocal, v.jlocal));
#endif

	// Attempt to send iend - 1 column to 'rright' process (for it it will be 'istart - 1' column (by j))
	// Note: assuming what previous send has been completed
	if (env.rright != -1) {
		assert(env.xcoord != env.xsize - 1 && env.rright != -1);
#ifndef USE_MCA
		thrust::transform(buffer.icolumnindices.begin(), buffer.icolumnindices.end(), buffer.icolumntmp.begin(), ExtractColumn(v.ilocal - 1, v.getv()));
		thrust::copy(buffer.icolumntmp.begin(), buffer.icolumntmp.end(), buffer.iendm1column.begin());
		__private_sync_halo(env.rank, env.rright, thrust::raw_pointer_cast(buffer.iendm1column.data()), thrust::raw_pointer_cast(tmp.data()), v.jlocal, env.comm);
		thrust::copy_n(tmp.begin(), v.jlocal, iendcolumn.begin());
#else
		thrust::transform(buffer.icolumnindices.begin(), buffer.icolumnindices.end(), buffer.iendm1column.begin(), ExtractColumn(v.ilocal - 1, v.getv()));
		__private_sync_halo(env.rank, env.rright, thrust::raw_pointer_cast(buffer.iendm1column.data()), thrust::raw_pointer_cast(iendcolumn.data()), v.jlocal, env.comm);
#endif
	}

	// Attempt to send jend - 1 column to 'rtop' process (for it it will be 'jstart - 1' row (by i))
	if (env.rtop != -1) {
		assert(env.ycoord != env.ysize - 1 && env.rtop != -1);
#ifndef USE_MCA
		thrust::copy_n(v.values.begin() + v.ilocal * (v.jlocal - 1), v.ilocal, buffer.jendm1row.begin());
		__private_sync_halo(env.rank, env.rtop, thrust::raw_pointer_cast(buffer.jendm1row.data()), thrust::raw_pointer_cast(tmp.data()), v.ilocal, env.comm);
		thrust::copy_n(tmp.begin(), v.ilocal, jendrow.begin());
#else
		__private_sync_halo(env.rank, env.rtop,
				thrust::raw_pointer_cast(v.getv().data()) + v.ilocal * (v.jlocal - 1),
				thrust::raw_pointer_cast(jendrow.data()), v.ilocal, env.comm
		);
#endif
	}

	if (env.rleft != -1) {
		assert(env.xcoord != 0 && env.rleft != -1);
#ifndef USE_MCA
		thrust::transform(buffer.icolumnindices.begin(), buffer.icolumnindices.end(), buffer.icolumntmp.begin(), ExtractColumn(0, v.getv()));
		thrust::copy(buffer.icolumntmp.begin(), buffer.icolumntmp.end(), buffer.istartcolumn.begin());
		__private_sync_halo(env.rank, env.rleft, thrust::raw_pointer_cast(buffer.istartcolumn.data()), thrust::raw_pointer_cast(tmp.data()), v.jlocal, env.comm);
		thrust::copy_n(tmp.begin(), v.jlocal, istartm1column.begin());
#else
		thrust::transform(buffer.icolumnindices.begin(), buffer.icolumnindices.end(), buffer.istartcolumn.begin(), ExtractColumn(0, v.getv()));
		__private_sync_halo(env.rank, env.rleft, thrust::raw_pointer_cast(buffer.istartcolumn.data()), thrust::raw_pointer_cast(istartm1column.data()), v.jlocal, env.comm);
#endif
	}

	// Attempt to send jstart row (by i) to 'rbottom' process (for it it will be 'jend' row (by i))
	if (env.rbottom != -1) {
		assert(env.ycoord != 0 && env.rbottom != -1);
#ifndef USE_MCA
		thrust::copy_n(v.values.begin(), v.ilocal, buffer.jstartrow.begin());
		__private_sync_halo(env.rank, env.rbottom, thrust::raw_pointer_cast(buffer.jstartrow.data()), thrust::raw_pointer_cast(tmp.data()), v.ilocal, env.comm);
		thrust::copy_n(tmp.begin(), v.ilocal, jstartm1row.begin());
#else
		__private_sync_halo(env.rank, env.rbottom, thrust::raw_pointer_cast(v.getv().data()), thrust::raw_pointer_cast(jstartm1row.data()), v.ilocal, env.comm);
#endif
	}

	stats.send.end();
	stats.receive.end();
}
#endif

GridVectorHaloPoints::~GridVectorHaloPoints() {};

