/*
 * linalg.cpp
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#include <mpi.h>
#include <cmath>
#include <thrust/transform_reduce.h>
#include "linalg.h"

namespace linalg {

struct ThreeScalarProductFunctor : public thrust::unary_function<thrust::tuple<double, double>, thrust::tuple<double, double, double>> {
	__device__ thrust::tuple<double, double, double> operator()(const thrust::tuple<double, double> &input) const {
		// [u, u] [v, v] and [u, v]
		const double u = thrust::get<0>(input);
		const double v = thrust::get<1>(input);
		return thrust::make_tuple(u * u, v * v, u * v);
	}
};

struct SumThreeTupleFunctor : public thrust::binary_function<thrust::tuple<double, double, double>, thrust::tuple<double, double, double>, thrust::tuple<double, double, double>> {
	__device__ thrust::tuple<double, double, double> operator()(const thrust::tuple<double, double, double> &l, const thrust::tuple<double, double, double> &r) const {
		return thrust::make_tuple(
				thrust::get<0>(l) + thrust::get<0>(r),
				thrust::get<1>(l) + thrust::get<1>(r),
				thrust::get<2>(l) + thrust::get<2>(r)
		);
	}
};

struct ScalarSubtractAbsolutFunctor : public thrust::unary_function<thrust::tuple<double, double>, double> {
	__device__ double operator()(const thrust::tuple<double, double> &input) const {
		return std::fabs(thrust::get<0>(input) - thrust::get<1>(input));
	}
};

struct ScalarMultiplicationSubtractFunctor : public thrust::unary_function<thrust::tuple<double, double>, double> {
	__device__ double operator()(const thrust::tuple<double, double> &input) const {
		double t1 = thrust::get<0>(input);
		double t2 = thrust::get<1>(input);
		return (t1 - t2) * (t1 - t2);
	}
};

thrust::tuple<double, double, double> Compute3ScalarProduct_E(const MPIEnvironment &env, const thrust::device_vector<double> &u, const thrust::device_vector<double> &v) {
	const thrust::tuple<double, double, double> o = thrust::transform_reduce(
			thrust::make_zip_iterator(thrust::make_tuple(u.begin(), v.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(u.end(), v.end())),
			ThreeScalarProductFunctor(),
			thrust::make_tuple(0.0, 0.0, 0.0),
			SumThreeTupleFunctor()
	);
	double tmp[3];
	double res[3];

	tmp[0] = thrust::get<0>(o);
	tmp[1] = thrust::get<1>(o);
	tmp[2] = thrust::get<2>(o);

	MPI_Allreduce(tmp, res, 3, MPI_DOUBLE, MPI_SUM, env.comm);

	return thrust::make_tuple(res[0], res[1], res[2]);
}

double NormSubtract_C(const thrust::device_vector<double> &u, const thrust::device_vector<double> &v) {
	return thrust::transform_reduce(
			thrust::make_zip_iterator(thrust::make_tuple(u.begin(), v.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(u.end(), v.end())),
			ScalarSubtractAbsolutFunctor(),
			0.0,
			thrust::maximum<double>()
	);
}

double ScalarProductSubtract_E(double h1, double h2, const thrust::device_vector<double> &u, const thrust::device_vector<double> &v) {
	return h1 * h2 * thrust::transform_reduce(
			thrust::make_zip_iterator(thrust::make_tuple(u.begin(), v.begin())),
			thrust::make_zip_iterator(thrust::make_tuple(u.end(), v.end())),
			ScalarMultiplicationSubtractFunctor(),
			0.0,
			thrust::plus<double>()
	);
}

double NormSubtract_E(double h1, double h2, const thrust::device_vector<double> &u, const thrust::device_vector<double> &v) {
	return std::sqrt(ScalarProductSubtract_E(h1, h2, u, v));
}

} /* namespace linalg */
