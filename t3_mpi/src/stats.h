/*
 * stats.h
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#ifndef STATS_H_
#define STATS_H_

#include <chrono>
#include <iostream>

class TimeStatsTimer {
	std::chrono::time_point<std::chrono::system_clock> tmp;
	unsigned long long ms;

public:
	TimeStatsTimer();

	void start();

	void end();

	unsigned long long int time() const;
};

class TimeStats {
public:
	TimeStatsTimer program;
	TimeStatsTimer init;
	TimeStatsTimer solver;
	TimeStatsTimer send;
	TimeStatsTimer receive;
	TimeStatsTimer r;
	TimeStatsTimer Ar;
	TimeStatsTimer wmtr;
	TimeStatsTimer scpr;

	TimeStats();

	friend std::ostream& operator<<(std::ostream& os, const TimeStats& t);
};


#endif /* STATS_H_ */
