/*
 * mpitools.cpp
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#include <iostream>

#include "mpitools.h"

MPIEnvironment::MPIEnvironment(int IM, int IN) {
	int size, rank;

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int p_size[2]   = {0, 0};
	int p_coord[2]  = {0, 0};
	int p_period[2] = {0, 0};

	MPI_Dims_create(size, 2, p_size);

	MPI_Cart_create(MPI_COMM_WORLD, 2, p_size, p_period, 0, &(this->comm));

	MPI_Comm_size(this->comm, &size);
	MPI_Comm_rank(this->comm, &rank);
	MPI_Cart_coords(this->comm, rank, 2, p_coord);

	this->size   = size;
	this->rank   = rank;
	this->xsize  = p_size[0];
	this->ysize  = p_size[1];
	this->xcoord = p_coord[0];
	this->ycoord = p_coord[1];

	if (p_coord[1] != this->ysize - 1) { // Top
		const int coords[2] = {p_coord[0], p_coord[1] + 1};
		MPI_Cart_rank(this->comm, (const int *) coords, &(this->rtop));
	} else {
		this->rtop = -1;
	}

	if (p_coord[0] != 0) { // Left
		const int coords[2] = {p_coord[0] - 1, p_coord[1]};
		MPI_Cart_rank(this->comm, (const int *)coords, &(this->rleft));
	} else {
		this->rleft = -1;
	}

	if (p_coord[1] != 0) { // Bottom
		const int coords[2] = {p_coord[0], p_coord[1] - 1};
		MPI_Cart_rank(this->comm, (const int *)coords, &(this->rbottom));
	} else {
		this->rbottom = -1;
	}

	if (p_coord[0] != this->xsize - 1) { // Right
		const int coords[2] = {p_coord[0] + 1, p_coord[1]};
		MPI_Cart_rank(this->comm, (const int *)coords, &(this->rright));
	} else {
		this->rright = -1;
	}

	if (IM % xsize != 0 || IN % ysize != 0) {
		std::cout << "Failed to create distributed grid vector: IM and IN should be dividable by MPI x and y size:"
				<< "(" << IM << "," << IN << ") / ("  << xsize << "," << ysize << ")" << std::endl;
		exit(-1);
	}

	int xblksz = IM / this->xsize;
	int yblksz = IN / this->ysize;

	int istart = xblksz * this->xcoord;
	int iend   = istart + xblksz;

	int jstart = yblksz * this->ycoord;
	int jend   = jstart + yblksz;

	this->istart  = istart;
	this->iend    = iend;
	this->jstart  = jstart;
	this->jend    = jend;
	this->iglobal = IM;
	this->jglobal = IN;
	this->ilocal  = (iend - istart);
	this->jlocal  = (jend - jstart);
}

void MPIEnvironment::print() const {
	std::cout << "MPI Environment for rank: " << rank << "of " << size << std::endl;
	std::cout << "Coords size = [" << xsize << "," << ysize << "]" << std::endl;
	std::cout << "Coords      = [" << xcoord << "," << ycoord << "]" << std::endl;
	std::cout << "[ilocal, jlocal] = [" << ilocal << "," << jlocal << "]" << std::endl;
	std::cout << "Top: " << rtop << ", Left: " << rleft << ", Bottom: " << rbottom << ", Right: " << rright << std::endl;
	std::cout << "-----------------------------------------" << std::endl;

	//	printf("Compile time check:\n");
	//#if defined(MPIX_CUDA_AWARE_SUPPORT) && MPIX_CUDA_AWARE_SUPPORT
	//	printf("This MPI library has CUDA-aware support.\n", MPIX_CUDA_AWARE_SUPPORT);
	//#elif defined(MPIX_CUDA_AWARE_SUPPORT) && !MPIX_CUDA_AWARE_SUPPORT
	//	printf("This MPI library does not have CUDA-aware support.\n");
	//#else
	//	printf("This MPI library cannot determine if there is CUDA-aware support.\n");
	//#endif /* MPIX_CUDA_AWARE_SUPPORT */
	//
	//	printf("Run time check:\n");
	//#if defined(MPIX_CUDA_AWARE_SUPPORT)
	//	if (1 == MPIX_Query_cuda_support()) {
	//		printf("This MPI library has CUDA-aware support.\n");
	//	} else {
	//		printf("This MPI library does not have CUDA-aware support.\n");
	//	}
	//#else /* !defined(MPIX_CUDA_AWARE_SUPPORT) */
	//	printf("This MPI library cannot determine if there is CUDA-aware support.\n");
	//#endif /* MPIX_CUDA_AWARE_SUPPORT */

}

MPIEnvironment::~MPIEnvironment() {
	// TODO Auto-generated destructor stub
}

