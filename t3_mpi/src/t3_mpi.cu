/*
 ============================================================================
 Name        : t3_mpi.cu
 Author      : Bagaev Dmitri
 Version     :
 Copyright   : 
 Description : Compute sum of reciprocals using STL on CPU and Thrust on GPU
 ============================================================================
 */

#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>
#include <chrono>
#include <mpi.h>

#include <thrust/reduce.h>
#include <thrust/device_vector.h>

#include "debug.h"
#include "problem.h"
#include "mpitools.h"
#include "solver.h"
#include "stats.h"

int main(int argc, char **argv) {
	MPI_Init(&argc, &argv);

	TimeStats timings;
	timings.program.start();

	int IM = 16, IN = 16;
	int max_iter = 10;
	double eps = 1e-6;
	int print_it = 1;

	if (argc >= 2) IM = (int) strtol(argv[1], NULL, 10);
	if (argc >= 3) IN = (int) strtol(argv[2], NULL, 10);
	if (argc >= 4) max_iter = (int) strtol(argv[3], NULL, 10);
	if (argc >= 5) eps = (double) strtod(argv[4], NULL);
	if (argc >= 6) print_it = (int) strtol(argv[5], NULL, 10);

	const MPIEnvironment env(IM, IN);
	if (env.rank == 0) {
		std::cout << "./solve " << IM << " " << IN << " " << max_iter << " " << eps << " " << print_it << std::endl;
		env.print();
		debug::printDeviceCheck();
	}

	auto U = [](double x, double y) -> double {
		return 1 + std::cos(M_PI * x * y);
	};

	auto F = [](double x, double y) -> double {
		return M_PI * M_PI * (x * x + y * y) * std::cos(M_PI * x * y);
	};

	auto psi = [](double x, double y) -> double {
		return 0.0;
	};

	timings.init.start();
	const ProblemDomain domain(IM, IN, 0.0, 2.0, 0.0, 1.0);
	const Problem problem(env, domain, U, F, U, psi);
	timings.init.end();

	if (env.rank == 0) {
		std::cout << domain << std::endl;
	}

	Solver solver(env, problem, timings);
	timings.solver.start();
	double norm = solver.solve(eps, max_iter, print_it);
	timings.solver.end();

	timings.program.end();
	if (env.rank == 0) {
		std::cout << timings << std::endl;
	}
	MPI_Finalize();
	return 0;
}
