/*
 * problem.cpp
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#include "problem.h"

#include <algorithm>
#include <numeric>

ProblemDomain::ProblemDomain(int M, int N, double A1, double A2, double B1, double B2):
M(M), N(N), A1(A1), A2(A2), B1(B1), B2(B2), h1((A2 - A1) / M), h2((B2 - B1) / N) {

};

std::ostream& operator<<(std::ostream& os, const ProblemDomain& d) {
	os << "Omega: (" << d.A1 << "," << d.A2 << ") x (" << d.B1 << "," << d.B2 << ")" << std::endl;
	os << "Grid:  [" << 0 << "," << d.M << ") x [" << 0 << "," << d.N << ")" << std::endl;
	os << "h1 = " << d.h1 << ", h2 = " << d.h2;
	return os;
}

double ApplyGridLambda::operator()(const int &k) const {
	int gi = k % M;   // global i index
	int gj = k / M;   // global j index
	return lambda(A1 + gi * h1, B1 + gj * h2);
}

double ApplyGridBorderXLambda::operator()(const int &k) const {
	return lambda(from + k * h, Y);
}

double ApplyGridBorderYLambda::operator()(const int &k) const {
	return lambda(X, from + k * h);
}

Problem::Problem(const MPIEnvironment &env, const ProblemDomain &d,
		const std::function<double (double, double)> &u, const std::function<double (double, double)> &f,
		const std::function<double (double, double)> &dirichle, const std::function<double (double, double)> &neumann):
								domain(d),
								U(GridVector(env, d.A1, d.B1, d.h1, d.h2, u)),
								F(GridVector(env, d.A1, d.B1, d.h1, d.h2, f)),
								dirichleT(thrust::device_vector<double>(d.M)),
								dirichleR(thrust::device_vector<double>(d.N)),
								neumannL(thrust::device_vector<double>(d.N)),
								neumannB(thrust::device_vector<double>(d.M)) {

	// Create grid values (i, const)
	std::vector<int> xborder(d.M);
	std::vector<double> xvalues(d.M);
	std::iota(xborder.begin(), xborder.end(), 0);

	// Create grid values (const, j)
	std::vector<int> yborder(d.N);
	std::vector<double> yvalues(d.M);
	std::iota(yborder.begin(), yborder.end(), 0);

	std::transform(xborder.begin(), xborder.end(), xvalues.begin(), ApplyGridBorderXLambda(d.A1, d.h1, d.B2, dirichle));
	thrust::copy(xvalues.begin(), xvalues.end(), dirichleT.begin());

	std::transform(yborder.begin(), yborder.end(), yvalues.begin(), ApplyGridBorderYLambda(d.B1, d.h2, d.A2, dirichle));
	thrust::copy(yvalues.begin(), yvalues.end(), dirichleR.begin());

	std::transform(xborder.begin(), xborder.end(), xvalues.begin(), ApplyGridBorderXLambda(d.A1, d.h1, d.B1, neumann));
	thrust::copy(xvalues.begin(), xvalues.end(), neumannB.begin());

	std::transform(yborder.begin(), yborder.end(), yvalues.begin(), ApplyGridBorderYLambda(d.B1, d.h2, d.A1, neumann));
	thrust::copy(yvalues.begin(), yvalues.end(), neumannL.begin());
};
