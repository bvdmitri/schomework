/*
 * linalg.h
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#ifndef LINALG_H_
#define LINALG_H_

#include <thrust/device_vector.h>

#include "mpitools.h"

namespace linalg {

// Function to compute three scalar products simultanuosly (used for t = [r, Ar] / [Ar, Ar] and norm of r)
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u,v  - vectors to compute scalar-product for
//   : on out tuple of three T elements [u, u] [v, v] [u, v]
thrust::tuple<double, double, double> Compute3ScalarProduct_E(const MPIEnvironment &env, const thrust::device_vector<double> &u, const thrust::device_vector<double> &v);

double NormSubtract_C(const thrust::device_vector<double> &u, const thrust::device_vector<double> &v);

double ScalarProductSubtract_E(double h1, double h2, const thrust::device_vector<double> &u, const thrust::device_vector<double> &v);

double NormSubtract_E(double h1, double h2, const thrust::device_vector<double> &u, const thrust::device_vector<double> &v);

} /* namespace linalg */

#endif /* LINALG_H_ */
