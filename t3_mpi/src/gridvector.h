/*
 * gridvector.h
 *
 *  Created on: Dec 10, 2018
 *      Author: bvdmitri
 */

#ifndef GRIDVECTOR_H_
#define GRIDVECTOR_H_

#include <thrust/device_vector.h>
#include <functional>

#include "mpitools.h"
#include "stats.h"

class GridVectorHaloPoints;

class GridVector {
	thrust::device_vector<double> values;    // values in vector
public:
	int istart;        // start i (included)
	int iend;          // end i   (not included)
	int jstart;        // start j (included)
	int jend;          // end j   (not included)

	int iglobal;          // number of i (global)
	int jglobal;          // number of j (global)

	int ilocal;           // number of i (global)
	int jlocal;           // number of j (global)


	GridVector(const MPIEnvironment &env, double d);

	GridVector(const MPIEnvironment &env,
			double A1, double B1, double h1, double h2,
			const std::function<double(double, double)> &f);

	const thrust::device_vector<double>& getv() const;

	thrust::device_vector<double>& getvnoc();

	void print() const;

	~GridVector();

	friend class GridVectorHaloPoints;
};

class GridVectorHaloSentBuffer {
public:
	thrust::device_vector<unsigned int> icolumnindices;
#ifndef USE_MCA
	thrust::device_vector<unsigned int> icolumntmp;
	thrust::host_vector<double> istartcolumn;  // values of temporary buffer for istart column (by j)
	thrust::host_vector<double> iendm1column;  // values of temporary buffer iend - 1 column (by j)
	thrust::host_vector<double> jstartrow;     // values of temporary buffer for jstart row (by i)
	thrust::host_vector<double> jendm1row;     // values of temporary buffer jend - 1 row (by i)
#else
	thrust::device_vector<double> istartcolumn;  // values of temporary buffer for istart column (by j)
	thrust::device_vector<double> iendm1column;  // values of temporary buffer iend - 1 column (by j)
	thrust::device_vector<double> jstartrow;     // values of temporary buffer for jstart row (by i)
	thrust::device_vector<double> jendm1row;     // values of temporary buffer jend - 1 row (by i)
#endif
	MPI_Request istartrequest;
	MPI_Request iendm1request;
	MPI_Request jstartrequest;
	MPI_Request jendm1request;

	GridVectorHaloSentBuffer(const MPIEnvironment &env);

	~GridVectorHaloSentBuffer();
};

enum SyncGridHaloTag {
	IENDCOLUMN_SYNC_TAG     = 1,
	ISTARTM1COLUMN_SYNC_TAG = 2,
	JENDROW_SYNC_TAG        = 3,
	JSTARTM1ROW_SYNC_TAG    = 4,
};

class GridVectorHaloPoints {
private:
	int    ilocal;           // number of i (local)
	int    jlocal;           // number of j (local)
public:
	thrust::device_vector<double> iendcolumn;      // values of iend column
	thrust::device_vector<double> istartm1column;  // values of istart-1 column

	thrust::device_vector<double> jendrow;         // values of jend column
	thrust::device_vector<double> jstartm1row;     // values of jstart-1 column

	GridVectorHaloPoints(const MPIEnvironment &env);

	void sync(const MPIEnvironment &env, const GridVector &v, GridVectorHaloSentBuffer &buffer, TimeStats &stats);

	void print() const;

	~GridVectorHaloPoints();
};


#endif /* GRIDVECTOR_H_ */
