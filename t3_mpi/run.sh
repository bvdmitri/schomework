#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --time=0-01:00
mpirun.openmpi -N 1 ./t3_mpi_release 1024 1024 1000 1e-6 100
