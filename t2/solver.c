// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//
// Created by Dmitri Bagaev on 16/11/2018.
//

#include <assert.h>
#include <stdio.h>
#include "solver.h"

void compute_Aw(int M, int N, double h1, double h2,
                const GridVector *w, const GridVectorHaloPoints *halo, const GridVector *r) {

  assert(w->istart == r->istart && w->iend == r->iend &&
         w->ilocal == r->ilocal && w->jlocal == r->jlocal);

  double invh1_2 = -1.0 / (h1 * h1);
  double invh2_2 = -1.0 / (h2 * h2);

  double inv2h1_2 = 2.0 * invh1_2;
  double inv2h2_2 = 2.0 * invh2_2;

  double *rvalues = r->values;
  const double *const wvalues = w->values;

  int istart = w->istart;
  int jstart = w->jstart;
  int ilocal = w->ilocal;
  int jlocal = w->jlocal;

  #ifdef _OPENMP
  #pragma omp parallel for
  #endif
  for (int k = 0; k < ilocal * jlocal; ++k) {
    int gi = istart + k % ilocal;   // global i index
    int gj = jstart + k / ilocal;   // global j index

    assert(!(gi == M || gj == N)); // grid function cannot be defined on Right or Top border

    double wij   = wvalues[k];
    double wip1j = (gi == M - 1) ? 0.0 : (gi == w->iend - 1) ? halo->iendcolumn[gj - jstart] : wvalues[k + 1];
    double wijp1 = (gj == N - 1) ? 0.0 : (gj == w->jend - 1) ? halo->jendrow[gi - istart] : wvalues[k + ilocal];

    if ((gi >= 1 && gi < M) && (gj >= 1 && gj < N)) { // Check if current point is inner point of grid

      // Here we have the following formula
      // -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)

      double wim1j = (gi == istart) ? halo->istartm1column[gj - jstart] : wvalues[k - 1];
      double wijm1 = (gj == jstart) ? halo->jstartm1row[gi - istart] : wvalues[k - ilocal];

      rvalues[k] = invh1_2 * (wip1j - 2.0 * wij + wim1j) +
                   invh2_2 * (wijp1 - 2.0 * wij + wijm1);

    } else if (gi == 0 && gj != 0) { // Check if current point on Left border
      assert(gj >= 1 && gj < N); // Some additional checks

      // Here we have the following formula
      // -2 / h1^2 (w_i+1_j - w_i_j) - 1 / h2^2 (w_i_j+1 - 2w_i_j + w_i_j-1)
      // i = 0
      // -2 / h1^2 (w1j - w0j) - 1 / h2^2 (w0j+1 - 2w0j + w0j-1)

      double w0jm1 = (gj == jstart) ? halo->jstartm1row[gi - istart] : wvalues[k - ilocal];;

      rvalues[k] = inv2h1_2 * (wip1j - wij) +
                   invh2_2 * (wijp1 - 2.0 * wij + w0jm1);

    } else if (gj == 0 && gi != 0) { // Check if current point on Bottom border
      assert(gi >= 1 && gi < M); // Some additional checks

      // Here we have the following formula
      // -1 / h1^2 (w_i+1_j - 2w_i_j + w_i-1_j) - 2 / h^2 (w_i_j+1 - w_i_j)
      // j = 0
      // -1 / h1^2 (w_i+1_0 - 2wi0 + w_i-1_0) - 2 / h^2 (wi1 - wi0)

      double wim10 = (gi == istart) ? halo->istartm1column[gj - jstart] : wvalues[k - 1];

      rvalues[k] = invh1_2 * (wip1j - 2.0 * wij + wim10) +
                   inv2h2_2 * (wijp1 - wij);

    } else if (gi == 0 && gj == 0) { // Check if current point on Left-Bottom point

      // Here we have the following formula
      // -2 / h1^2 (w_i+1_j - w_i_j) - 2 / h2^2 (w_i_j+1 - w_i_j)
      // i = 0 and j = 0
      // -2 / h1^2 (w10 - w00) - 2 / h2^2 (w01 - w00)

      rvalues[k] = inv2h1_2 * (wip1j - wij) +
                   inv2h2_2 * (wijp1 - wij);

    } else if (gi == M || gj == N) { // Check if current point on Right or Top border

      printf("[ERROR] Unreachable code, grid function cannot be defined on Right or Top border");
      assert(0);

    } else {
      assert(0);
    }
  }

}

void compute_r(int M, int N, double h1, double h2,
               const GridVector *w, const GridVectorHaloPoints *halo, const GridVector *F,
               const DirichleBoundaryConditionRT *dirichle, const NeumannBoundaryConditionLB *neumann,
               const GridVector *r) {
  compute_Aw(M, N, h1, h2, w, halo, r);

  const double *const Fvalues = F->values;
  double       *const rvalues = r->values;

  int istart = w->istart;
  int jstart = w->jstart;
  int ilocal = w->ilocal;
  int jlocal = w->jlocal;

  #ifdef _OPENMP
  #pragma omp parallel for
  #endif
  for (int k = 0; k < ilocal * jlocal; ++k) {
    int gi = istart + k % ilocal;   // global i index
    int gj = jstart + k / ilocal;   // global j index

    double B = 0.0;

    assert(!(gi == M || gj == N)); // grid function cannot be defined on Right or Top border

    if ((gi >= 1 && gi < M) && (gj >= 1 && gj < N)) { // Check if current point is inner point of grid

      // Here we have the following formula
      // B = Fij

      B = Fvalues[k];

    } else if (gi == 0 && gj != 0) { // Check if current point on Left border
      assert(gj >= 1 && gj < N); // Some additional checks

      // Here we have the following formula
      // B = F_ij - (2 / h1) psi_ij

      B = (Fvalues[k] - (2.0 / h1) * get_neumann_boundary_condition_value(neumann, M, N, gi, gj));

    } else if (gj == 0 && gi != 0) { // Check if current point on Bottom border
      assert(gi >= 1 && gi < M); // Some additional checks

      // Here we have the following formula
      // B = F_ij - (2 / h2) psi_ij

      B = (Fvalues[k] - (2.0 / h2) * get_neumann_boundary_condition_value(neumann, M, N, gi, gj));

    } else if (gi == 0 && gj == 0) { // Check if current point on Left-Bottom point

      // Here we have the following formula
      // B = Fij - (2/h1 + 2/h2)psi_ij

      B = (Fvalues[k] - (2.0 / h1 + 2.0 / h2) * get_neumann_boundary_condition_value(neumann, M, N, gi, gj));

    }
//    } else if (gi == M || gj == N) { // Check if current point on Right or Top border
//
//      printf("[ERROR] Unreachable code, grid function cannot be defined on Right or Top border");
//      assert(0);
//
//    }

    if (gi == M - 1) {
      B += 1.0 / (h1 * h1) * get_dirichle_boundary_condition_value(dirichle, M, N, M, gj);
    }

    if (gj == N - 1) {
      B += 1.0 / (h2 * h2) * get_dirichle_boundary_condition_value(dirichle, M, N, gi, N);
    }

    rvalues[k] -= B;
  }
}

void compute_wmtr(const GridVector *v, const GridVector *w, const GridVector *r, double t) {
  assert(v->ilocal == w->ilocal && v->jlocal == w->jlocal && v->ilocal == r->ilocal && v->jlocal == r->jlocal);

  double       *const vvalues = v->values;
  const double *const wvalues = w->values;
  const double *const rvalues = r->values;

  for (int k = 0; k < v->ilocal * v->jlocal; ++k) {
    vvalues[k] = wvalues[k] - t * rvalues[k];
  }

}
