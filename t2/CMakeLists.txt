cmake_minimum_required(VERSION 3.8)
project(solve C)

set(CMAKE_C_STANDARD 99)

find_package(MPI REQUIRED)

include_directories(${MPI_INCLUDE_PATH})

add_executable(solve main.c vector.c vector.h solver.c solver.h linalg.c linalg.h mpi_tools.c mpi_tools.h stats.c stats.h)
target_link_libraries(solve ${MPI_C_LIBRARIES} -lm)

#add_executable(tt tt.c vector.c vector.h solver.c solver.h linalg.c linalg.h mpi_tools.c mpi_tools.h)
#target_link_libraries(tt ${MPI_C_LIBRARIES} -lm)