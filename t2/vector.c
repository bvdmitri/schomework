// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.co
//
// Created by Dmitri Bagaev on 15/11/2018.
//

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <mpi.h>
#include <math.h>
#include "vector.h"
#include "stats.h"

GridVector *create_grid_vector(const MPIEnvironment *env, int IM, int IN, double d) {
  if (IM % env->xsize != 0 || IN % env->ysize != 0) {
    printf(
        "Failed to create distributed grid vector: IM and IN should be dividable by MPI x and y size: (%d, %d) / (%d, %d)\n",
        IM, IN, env->xsize, env->ysize
    );
    exit(-1);
  }

  int xblksz = IM / env->xsize;
  int yblksz = IN / env->ysize;

  int istart = xblksz * env->xcoord;
  int iend   = istart + xblksz;

  int jstart = yblksz * env->ycoord;
  int jend   = jstart + yblksz;

  GridVector *vector = (GridVector *) malloc(sizeof(GridVector));

  vector->istart  = istart;
  vector->iend    = iend;
  vector->jstart  = jstart;
  vector->jend    = jend;
  vector->iglobal = IM;
  vector->jglobal = IN;
  vector->ilocal  = (iend - istart);
  vector->jlocal  = (jend - jstart);

  int    vcount  = (iend - istart) * (jend - jstart);
  double *values = (double *) malloc(vcount * sizeof(double));

  for (int i = 0; i < vcount; ++i) {
    values[i] = d;
  }

  vector->values = values;

  return vector;
}

GridVector *create_grid_vector_from_function(const MPIEnvironment *env, int M, int N,
                                             double A1, double B1, double h1, double h2,
                                             double (*f)(double x, double y)) {
  GridVector *v = create_grid_vector(env, M, N, 0.0);

  for (int k = 0; k < v->ilocal * v->jlocal; ++k) {
    int gi = v->istart + k % v->ilocal;   // global i index
    int gj = v->jstart + k / v->ilocal;   // global j index

    double x = A1 + gi * h1;
    double y = B1 + gj * h2;

    v->values[k] = (*f)(x, y);
  }

  return v;
}

void print_grid_vector(GridVector *v) {

  for (int j = v->jlocal - 1; j >= 0; --j) {
    printf("[");
    for (int i = 0; i < v->ilocal; ++i) {
      printf("%lf ", v->values[j * v->ilocal + i % v->ilocal]);
    }
    printf("]\n");
  }

}

void write_grid_vector(const char *out, int M, int N, const GridVector *v) {
  FILE *file = fopen(out, "w");
  if (file == NULL) {
    printf("[ERROR] Unable to write results to file. Failed to open file %s\n", out);
    return;
  }

  fprintf(file, "%d %d\n", M, N);

  for (int k = 0; k < v->ilocal * v->jlocal; ++k) {
    int gi = v->istart + k % v->ilocal;   // global i index
    int gj = v->jstart + k / v->ilocal;   // global j index

    fprintf(file, "%d %d %.10f\n", gi, gj, v->values[k]);
  }

  fclose(file);
}

void check_vector(const GridVector *v, const char *n) {
  for (int k = 0; k < v->ilocal * v->jlocal; ++k) {
    if (isnan(v->values[k]) || isinf(v->values[k])) {
      int gi = v->istart + k % v->ilocal;   // global i index
      int gj = v->jstart + k / v->ilocal;   // global j index
      printf("Nan catched in vector %s: (%d, %d) = %f", n, gi, gj, v->values[k]);
      exit(-1);
    }
  }
}

void free_grid_vector(GridVector *vector) {
  free(vector->values);
  free(vector);
}

void sync_halo_points(const MPIEnvironment *env, const GridVector *w,
                      const GridVectorHaloPoints *halo, GridVectorHaloSentBuffer *buffer, TimeStats *stats) {

  assert(w->ilocal == halo->ilocal && w->jlocal == halo->jlocal);

  MPI_Status status;

  stats_start(&(stats->send));
  // Attempt to send istart column (by j) to 'rleft' process (for it it will be 'iend' column (by j))
  // Note: assuming what previous send has been completed
  if (w->istart != 0) {
    assert(env->xcoord != 0 && env->rleft != -1);
    if (buffer->istartrequest != NULL) {
      MPI_Wait(&(buffer->istartrequest), &status);
    }
    for (int j = 0; j < w->jlocal; ++j) {
      buffer->istartcolumn[j] = w->values[j * w->ilocal];
    }
    #ifndef NDEBUG
    for (int i = 0; i < w->jlocal; ++i) {
      if (isnan(buffer->istartcolumn[i]) || isinf(buffer->istartcolumn[i])) {
        printf("Non normal value on sent istartcolumn: %f\n", buffer->istartcolumn[i]);
        exit(-1);
      }
    }
    #endif
    MPI_Isend(buffer->istartcolumn, w->jlocal, MPI_DOUBLE, env->rleft, IENDCOLUMN_SYNC_TAG, env->comm,
              &(buffer->istartrequest));
  }

  // Attempt to send iend - 1 column to 'rright' process (for it it will be 'istart - 1' column (by j))
  // Note: assuming what previous send has been completed
  if (w->iend != w->iglobal) {
    assert(env->xcoord != env->xsize - 1 && env->rright != -1);
    if (buffer->iendm1request != NULL) {
      MPI_Wait(&(buffer->iendm1request), &status);
    }
    for (int j = 0; j < w->jlocal; ++j) {
      buffer->iendm1column[j] = w->values[j * w->ilocal + (w->ilocal - 1)];
    }
    #ifndef NDEBUG
    for (int i = 0; i < w->jlocal; ++i) {
      if (isnan(buffer->iendm1column[i]) || isinf(buffer->iendm1column[i])) {
        printf("Non normal value on sent iendm1column: %f\n", buffer->iendm1column[i]);
        exit(-1);
      }
    }
    #endif
    MPI_Isend(buffer->iendm1column, w->jlocal, MPI_DOUBLE, env->rright, ISTARTM1COLUMN_SYNC_TAG, env->comm,
              &(buffer->iendm1request));
  }

  // Attempt to send jstart row (by i) to 'rbottom' process (for it it will be 'jend' row (by i))
  // Note: assuming what previous send has been completed
  if (w->jstart != 0) {
    assert(env->ycoord != 0 && env->rbottom != -1);
    if (buffer->jstartrequest != NULL) {
      MPI_Wait(&(buffer->jstartrequest), &status);
    }
    for (int i = 0; i < w->ilocal; ++i) {
      buffer->jstartrow[i] = w->values[i];
    }
    #ifndef NDEBUG
    for (int i = 0; i < w->ilocal; ++i) {
      if (isnan(buffer->jstartrow[i]) || isinf(buffer->jstartrow[i])) {
        printf("Non normal value on sent jstartrow: %f\n", buffer->jstartrow[i]);
        exit(-1);
      }
    }
    #endif
    MPI_Isend(buffer->jstartrow, w->ilocal, MPI_DOUBLE, env->rbottom, JENDROW_SYNC_TAG, env->comm,
              &(buffer->jstartrequest));
  }

  // Attempt to send jend - 1 column to 'rtop' process (for it it will be 'jstart - 1' row (by i))
  if (w->jend != w->jglobal) {
    assert(env->ycoord != env->ysize - 1 && env->rtop != -1);
    if (buffer->jendm1request != NULL) {
      MPI_Wait(&(buffer->jendm1request), &status);
    }
    for (int i = 0; i < w->ilocal; ++i) {
      buffer->jendm1row[i] = w->values[i + w->ilocal * (w->jlocal - 1)];
    }
    #ifndef NDEBUG
    for (int i = 0; i < w->ilocal; ++i) {
      if (isnan(buffer->jendm1row[i]) || isinf(buffer->jendm1row[i])) {
        printf("Non normal value on sent jendm1row: %f\n", buffer->jendm1row[i]);
        exit(-1);
      }
    }
    #endif
    MPI_Isend(buffer->jendm1row, w->ilocal, MPI_DOUBLE, env->rtop, JSTARTM1ROW_SYNC_TAG, env->comm,
              &(buffer->jendm1request));
  }
  stats_end(&(stats->send));

  stats_start(&(stats->receive));
  // Attempt to receive istart column (by j) from 'rright' process (for current rank it will be 'iend' (by j))
  if (w->iend != w->iglobal) {
    assert(env->xcoord != env->xsize - 1 && env->rright != -1);
    MPI_Recv(halo->iendcolumn, w->jlocal, MPI_DOUBLE, env->rright, IENDCOLUMN_SYNC_TAG, env->comm, &status);
    #ifndef NDEBUG
    for (int i = 0; i < w->jlocal; ++i) {
      if (isnan(halo->iendcolumn[i]) || isinf(halo->iendcolumn[i])) {
        printf("Non normal value in received iendcolumn: %f\n", halo->iendcolumn[i]);
        exit(-1);
      }
    }
    #endif
  }

  // Attempt to receive iend - 1 column (by j) from 'rleft' process (for current rank it will be 'istart - 1' (by j))
  if (w->istart != 0) {
    assert(env->xcoord != 0 && env->rleft != -1);
    MPI_Recv(halo->istartm1column, w->jlocal, MPI_DOUBLE, env->rleft, ISTARTM1COLUMN_SYNC_TAG, env->comm, &status);
    #ifndef NDEBUG
    for (int i = 0; i < w->jlocal; ++i) {
      if (isnan(halo->istartm1column[i]) || isinf(halo->istartm1column[i])) {
        printf("Non normal value in received istartm1column: %f\n", halo->istartm1column[i]);
        exit(-1);
      }
    }
    #endif
  }

  // Attempt to receive jstart row (by i) from 'rtop' (for current rank it will be 'jend' (by i))
  if (w->jend != w->jglobal) {
    assert(env->ycoord != env->ysize - 1 && env->rtop != -1);
    MPI_Recv(halo->jendrow, w->ilocal, MPI_DOUBLE, env->rtop, JENDROW_SYNC_TAG, env->comm, &status);
    #ifndef NDEBUG
    for (int i = 0; i < w->ilocal; ++i) {
      if (isnan(halo->jendrow[i]) || isinf(halo->jendrow[i])) {
        printf("Non normal value in received jendrow: %f\n", halo->jendrow[i]);
        exit(-1);
      }
    }
    #endif
  }

  // Attempt to receive 'jend-1' row (by i) from 'rbottom' (for current rank it will be 'jstart-1' (by i))
  if (w->jstart != 0) {
    assert(env->ycoord != 0 && env->rbottom != -1);
    MPI_Recv(halo->jstartm1row, w->ilocal, MPI_DOUBLE, env->rbottom, JSTARTM1ROW_SYNC_TAG, env->comm, &status);
    #ifndef NDEBUG
    for (int i = 0; i < w->ilocal; ++i) {
      if (isnan(halo->jstartm1row[i]) || isinf(halo->jstartm1row[i])) {
        printf("Non normal value in received jstartm1row: %f\n", halo->jstartm1row[i]);
        exit(-1);
      }
    }
    #endif
  }
  stats_end(&(stats->receive));
}

GridVectorHaloPoints *create_halo_points(const MPIEnvironment *env, int ilocal, int jlocal) {

  GridVectorHaloPoints *halo = (GridVectorHaloPoints *) calloc(1, sizeof(GridVectorHaloPoints));

  if (env->rright != -1) {
    halo->iendcolumn = (double *) malloc(jlocal * sizeof(double));
  }

  if (env->rleft != -1) {
    halo->istartm1column = (double *) malloc(jlocal * sizeof(double));
  }

  if (env->rtop != -1) {
    halo->jendrow = (double *) malloc(ilocal * sizeof(double));
  }

  if (env->rbottom != -1) {
    halo->jstartm1row = (double *) malloc(ilocal * sizeof(double));
  }

  halo->ilocal = ilocal;
  halo->jlocal = jlocal;

  return halo;
}

GridVectorHaloSentBuffer *create_halo_sent_buffer(const MPIEnvironment *env, int ilocal, int jlocal) {

  GridVectorHaloSentBuffer *sent = (GridVectorHaloSentBuffer *) calloc(1, sizeof(GridVectorHaloSentBuffer));

  sent->istartrequest = NULL;
  sent->iendm1request = NULL;
  sent->jstartrequest = NULL;
  sent->jendm1request = NULL;

  if (env->xcoord != 0) {
    sent->istartcolumn = (double *) malloc(jlocal * sizeof(double));
  }

  if (env->xcoord != env->xsize - 1) {
    sent->iendm1column = (double *) malloc(jlocal * sizeof(double));
  }

  if (env->ycoord != 0) {
    sent->jstartrow = (double *) malloc(ilocal * sizeof(double));
  }

  if (env->ycoord != env->ysize - 1) {
    sent->jendm1row = (double *) malloc(ilocal * sizeof(double));
  }

  return sent;
}

void free_halo_points(GridVectorHaloPoints *halo) {
  free(halo->istartm1column);
  free(halo->iendcolumn);
  free(halo->jstartm1row);
  free(halo->jendrow);
  free(halo);
}

void free_halo_sent_buffer(GridVectorHaloSentBuffer *buffer) {
  MPI_Status status;

  if (buffer->iendm1column != NULL) {
    MPI_Wait(&buffer->iendm1request, &status);
    // MPI_Request_free(&buffer->iendm1request);
    free(buffer->iendm1column);
  }

  if (buffer->istartcolumn != NULL) {
    MPI_Wait(&buffer->istartrequest, &status);
    // MPI_Request_free(&buffer->istartrequest);
    free(buffer->istartcolumn);
  }

  if (buffer->jendm1row != NULL) {
    MPI_Wait(&buffer->jendm1request, &status);
    // MPI_Request_free(&buffer->jendm1request);
    free(buffer->jendm1row);
  }

  if (buffer->jstartrow != NULL) {
    MPI_Wait(&buffer->jstartrequest, &status);
    // MPI_Request_free(&buffer->jstartrequest);
    free(buffer->jstartrow);
  }

  free(buffer);
}

DirichleBoundaryConditionRT *create_dirichle_boundary_condition_rt(int M, int N,
                                                                   double A1, double B1, double h1, double h2,
                                                                   double (*f)(double, double)) {

  DirichleBoundaryConditionRT *conditions = (DirichleBoundaryConditionRT *) malloc(sizeof(DirichleBoundaryConditionRT));

  conditions->values = (double *) malloc((M + N + 2) * sizeof(double));

  int k = 0;

  for (int i = 0; i <= M; ++i) {
    conditions->values[k++] = (*f)(A1 + h1 * i, B1 + h2 * N);
  }

  for (int j = 0; j <= N; ++j) {
    conditions->values[k++] = (*f)(A1 + h1 * M, B1 + h2 * (N - j));
  }

  assert(k == (M + N + 2));

  return conditions;
}

void print_dirichle_boundary_condition(DirichleBoundaryConditionRT *d, int M, int N) {
  int k = 0;

  printf("[");
  for (int i = 0; i <= M; ++i) {
    printf("%lf ", d->values[k++]);
  }

  printf("]\n[");

  for (int j = 0; j <= N; ++j) {
    printf("%lf ", d->values[k++]);
  }
  printf("]\n");
}

void free_dirichle_boundary_condition_rt(DirichleBoundaryConditionRT *dirichle) {
  free(dirichle->values);
  free(dirichle);
}

double get_dirichle_boundary_condition_value(const DirichleBoundaryConditionRT *dirichle,
                                             int M, int N, int gi, int gj) {
  assert((gi == M || gj == N) && !(gi == M && gj == N));

  if (gj == N) { // Check if point on top border
    return dirichle->values[gi];
  } else if (gi == M) { // Check if point on right border
    return dirichle->values[gi + (N - gj) + 1];
  }

  assert(0);
  return 0.0;
}

NeumannBoundaryConditionLB *create_neumann_boundary_condition_lb(int M, int N,
                                                                 double A1, double B1, double h1, double h2,
                                                                 double (*f)(double, double, int)) {
  NeumannBoundaryConditionLB *conditions = (NeumannBoundaryConditionLB *) malloc(sizeof(NeumannBoundaryConditionLB));

  conditions->values = (double *) malloc((M + N + 2) * sizeof(double));

  int k = 0;

  for (int j = 0; j <= N; ++j) {
    conditions->values[k++] = (*f)(A1, (B1 + (N - j) * h2), 1);
  }

  for (int i = 0; i <= M; ++i) {
    conditions->values[k++] = (*f)(A1 + i * h1, B1, 2);
  }

  assert(k == (M + N + 2));

  return conditions;
}

void print_neumann_boundary_condition_lb(NeumannBoundaryConditionLB *n, int M, int N) {

  int k = 0;

  printf("[");
  for (int i = 0; i <= M; ++i) {
    printf("%lf ", n->values[k++]);
  }

  printf("]\n[");

  for (int j = 0; j <= N; ++j) {
    printf("%lf ", n->values[k++]);
  }
  printf("]\n");

}

void free_neumann_boundary_condition_lb(NeumannBoundaryConditionLB *neumann) {
  free(neumann->values);
  free(neumann);
}

double get_neumann_boundary_condition_value(const NeumannBoundaryConditionLB *neumann,
                                            int M, int N, int gi, int gj) {
  assert((gi == 0 && gj != N) || (gj == 0 && gi != M));

  if (gi == 0) { // Check if point on left border
    return neumann->values[N - gj];
  } else if (gj == 0) { // Check if point bottom border
    return neumann->values[N + gi];
  }

  assert(0);
  return 0.0;
}