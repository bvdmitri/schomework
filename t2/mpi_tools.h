//
// Created by bvdmitri on 22.11.18.
//

#ifndef SOLVE_MPI_TOOLS_H
#define SOLVE_MPI_TOOLS_H

#include <mpi.h>

// Definition of current mpi environment, stores all necessary data for mpi communication
struct MPIEnvironment {
    MPI_Comm comm;   // communicator with cartesian structure
    int      size;   // size of communicator
    int      rank;   // rank of current process
    int      xcoord; // xcoord of current process
    int      ycoord; // ycoord of current process
    int      xsize;  // size of communicator by x-axis
    int      ysize;  // size of communicator by y-axis

    int rtop;     // rank of 'top' process
    int rleft;    // rank of 'left' process
    int rbottom;  // rank of 'bottom' process
    int rright;   // rank of 'right' process
};

typedef struct MPIEnvironment MPIEnvironment;

// Function to create MPIEnvironment
MPIEnvironment *create_mpi_environment();

// Function to print MPIEnvironment (for debug purpose)
void print_mpi_environment(MPIEnvironment *env);

// Function to free MPIEnvironment
void free_mpi_environment(MPIEnvironment *env);

#endif //SOLVE_MPI_TOOLS_H
