//
// Created by bvdmitri on 11.12.18.
//

#ifndef SOLVE_STATS_H
#define SOLVE_STATS_H

#include <mpi.h>

struct TimeStatsTimer {
    double tmp;
    double sec;
};

typedef struct TimeStatsTimer TimeStatsTimer;

void stats_start(TimeStatsTimer *timer);

void stats_end(TimeStatsTimer *timer);

struct TimeStats {
    TimeStatsTimer program;
    TimeStatsTimer init;
    TimeStatsTimer solver;
    TimeStatsTimer send;
    TimeStatsTimer receive;
    TimeStatsTimer r;
    TimeStatsTimer Ar;
    TimeStatsTimer wmtr;
    TimeStatsTimer scpr;
};

typedef struct TimeStats TimeStats;

TimeStats* create_time_stats();

void print_time_stats(TimeStats *stats);

void free_time_stats(TimeStats *stats);


#endif //SOLVE_STATS_H
