mkdir -p results
mpisubmit.bg -w 01:00:00 -n $1 --mode vn --stdout results/out_$2.mpi.$1 --stderr results/err_$2.mpi.$1 solve -- 0.0 2.0 0.0 1.0 $2 $2 1e-6 900000
