//
// Created by bvdmitri on 22.11.18.
//

#include <stdlib.h>
#include <stdio.h>
#include "mpi_tools.h"

MPIEnvironment *create_mpi_environment() {
  MPIEnvironment *env = (MPIEnvironment *) malloc(sizeof(MPIEnvironment));

  int size, rank;

  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int p_size[2]   = {0, 0};
  int p_coord[2]  = {0, 0};
  int p_period[2] = {0, 0};

  MPI_Dims_create(size, 2, p_size);

  MPI_Cart_create(MPI_COMM_WORLD, 2, p_size, p_period, 0, &(env->comm));

  MPI_Comm_size(env->comm, &size);
  MPI_Comm_rank(env->comm, &rank);
  MPI_Cart_coords(env->comm, rank, 2, p_coord);

  env->size   = size;
  env->rank   = rank;
  env->xsize  = p_size[0];
  env->ysize  = p_size[1];
  env->xcoord = p_coord[0];
  env->ycoord = p_coord[1];

  if (p_coord[1] != env->ysize - 1) { // Top
    const int coords[2] = {p_coord[0], p_coord[1] + 1};
    MPI_Cart_rank(env->comm, (const int *) coords, &(env->rtop));
  } else {
    env->rtop = -1;
  }

  if (p_coord[0] != 0) { // Left
    const int coords[2] = {p_coord[0] - 1, p_coord[1]};
    MPI_Cart_rank(env->comm, (const int *)coords, &(env->rleft));
  } else {
    env->rleft = -1;
  }

  if (p_coord[1] != 0) { // Bottom
    const int coords[2] = {p_coord[0], p_coord[1] - 1};
    MPI_Cart_rank(env->comm, (const int *)coords, &(env->rbottom));
  } else {
    env->rbottom = -1;
  }

  if (p_coord[0] != env->xsize - 1) { // Right
    const int coords[2] = {p_coord[0] + 1, p_coord[1]};
    MPI_Cart_rank(env->comm, (const int *)coords, &(env->rright));
  } else {
    env->rright = -1;
  }

  return env;
}

void print_mpi_environment(MPIEnvironment *env) {
  printf("MPI Environment for rank: %d of %d\n", env->rank, env->size);
  printf("Coords size = [ %d, %d ]\n", env->xsize, env->ysize);
  printf("Coords      = [ %d, %d ]\n", env->xcoord, env->ycoord);
  printf("Top: %d, Left: %d, Bottom: %d, Right: %d\n", env->rtop, env->rleft, env->rbottom, env->rright);
  printf("-----------------------------------------\n");
}

void free_mpi_environment(MPIEnvironment *env) {
  free(env);
}
