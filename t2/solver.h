//
// Created by Dmitri Bagaev on 16/11/2018.
//

#include "vector.h"

#ifndef SOLVE_SOLVER_H
#define SOLVE_SOLVER_H

// Function to compute result = A w
// Input arguments:
//   : M      - number of grid points on x-axis
//   : N      - number of grid points on y-axis
//   : h1     - grid step on x-axis
//   : h2     - grid step on y-axis
//   : w      - Grid vector to multiply on
//   : halo   - Grid vector halo points of w
//   : r      - Grid vector to store result in
void compute_Aw(int M, int N, double h1, double h2,
                const GridVector *w, const GridVectorHaloPoints *halo, const GridVector *r);

// Function to compute r = A w - B
// Input arguments:
//   : M      - number of grid points on x-axis
//   : N      - number of grid points on y-axis
//   : h1     - grid step on x-axis
//   : h2     - grid step on y-axis
//   : w      - Grid vector to multiply on
//   : view   - Grid vector view of w
//   : F      - Grid function
//   : phi    - Dirichle boundary conditions
//   : psi    - Neumann boundary conditions
//   : r      - Grid vector to store result in
void compute_r(int M, int N, double h1, double h2,
               const GridVector *w, const GridVectorHaloPoints *halo, const GridVector *F,
               const DirichleBoundaryConditionRT *dirichle, const NeumannBoundaryConditionLB *neumann,
               const GridVector *r);

// Function to compute v = w - tr
// Input arguments:
//   : v      - GridVector structure to store result in
//   : w      - GridVector structure w
//   : r      - GridVector structure r
//   : t      - t parameter
void compute_wmtr(const GridVector *v, const GridVector *w, const GridVector *r, double t);

#endif //SOLVE_SOLVER_H
