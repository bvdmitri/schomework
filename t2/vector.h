//
// Created by Dmitri Bagaev on 15/11/2018.
//

#ifndef SOLVE_DISTRIBUTED_VECTOR_H
#define SOLVE_DISTRIBUTED_VECTOR_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <assert.h>
#include "mpi_tools.h"
#include "stats.h"

// Definition of distributed grid vector, stores values of discrete function in grid points
struct GridVector {
    double *values;    // values in vector
    int    istart;        // start i (included)
    int    iend;          // end i   (not included)
    int    jstart;        // start j (included)
    int    jend;          // end j   (not included)

    int iglobal;       // number of i (global)
    int jglobal;       // number of j (global)

    int ilocal;        // number of i (global)
    int jlocal;        // number of j (global)
};

typedef struct GridVector GridVector;

// Function to create DistributedGridVector from initializer value
// Input arguments:
//   : env  - MPI Environment
//   : M    - Number of points in grid by x-axis
//   : N    - Number of points in grid by y-axis
//   : d    - Initializer value
GridVector *create_grid_vector(const MPIEnvironment *env, int M, int N, double d);

// Function to create DistributedGridVector from initializer function
// Input arguments:
//   : env  - MPI Environment
//   : M    - Number of points in grid by x-axis
//   : N    - Number of points in grid by y-axis
//   : A1 - grid x-axis starts from
//   : B1 - grid y-axis starts from
//   : h1 - grid step size on x-axis
//   : h2 - grid step size on y-axis
//   : f    - Initializer function
GridVector *create_grid_vector_from_function(const MPIEnvironment *env, int M, int N,
                                             double A1, double B1, double h1, double h2,
                                             double (*f)(double x, double y));

// Function to GridVectorHaloSentBuffer structure
// Input arguments:
//   : v   - vector to print
void print_grid_vector(GridVector *v);

void write_grid_vector(const char *out, int M, int N, const GridVector *v);

void check_vector(const GridVector *v, const char *n);

// Function to delete DistributedGridVector
// Input arguments:
//   : vector - GridVector structure
void free_grid_vector(GridVector *vector);

// Definition of distributed grid vector in grips
struct GridVectorHaloPoints {
    double *iendcolumn;      // values of iend column
    double *istartm1column;  // values of istart-1 column
    int    ilocal;              // number of i (local)

    double *jendrow;         // values of jend column
    double *jstartm1row;     // values of jstart-1 column
    int    jlocal;              // number of j (local)

};

typedef struct GridVectorHaloPoints GridVectorHaloPoints;

// Definition of distributed sent grips of grid vector with MPI status
struct GridVectorHaloSentBuffer {
    double      *istartcolumn;  // values of temporary buffer for istart column (by j)
    MPI_Request istartrequest;

    double      *iendm1column;  // values of temporary buffer iend - 1 column (by j)
    MPI_Request iendm1request;

    double      *jstartrow;     // values of temporary buffer for jstart row (by i)
    MPI_Request jstartrequest;

    double      *jendm1row;     // values of temporary buffer jend - 1 row (by i)
    MPI_Request jendm1request;
};

typedef struct GridVectorHaloSentBuffer GridVectorHaloSentBuffer;

enum SyncGridHaloTag {
    IENDCOLUMN_SYNC_TAG     = 1,
    ISTARTM1COLUMN_SYNC_TAG = 2,
    JENDROW_SYNC_TAG        = 3,
    JSTARTM1ROW_SYNC_TAG    = 4,
};

// Function to sync grid halo points
// Input arguments:
//   : env  - MPI Environment
//   : w      - vector to sync grips from between mpi processes
//   : halo   - GridVectorHaloPoints structure to sync with
//   : buffer - GridVectorHaloSentBuffer structure to store temporary data in
void sync_halo_points(const MPIEnvironment *env, const GridVector *w,
                      const GridVectorHaloPoints *halo, GridVectorHaloSentBuffer *buffer, TimeStats *stats);

// Function to GridVectorHaloPoints structure
// Input arguments:
//   : env  - MPI Environment
//   : ilocal - local i indices count in block
//   : jlocal - local j indices count in block
GridVectorHaloPoints *create_halo_points(const MPIEnvironment *env, int ilocal, int jlocal);

// Function to GridVectorHaloSentBuffer structure
// Input arguments:
//   : env  - MPI Environment
//   : ilocal - local i indices count in block
//   : jlocal - local j indices count in block
GridVectorHaloSentBuffer *create_halo_sent_buffer(const MPIEnvironment *env, int ilocal, int jlocal);

// Function to delete GridVectorHaloPoints
// Input arguments:
//   : halo - GridVectorHaloPoints structure
void free_halo_points(GridVectorHaloPoints *halo);

// Function to delete GridVectorHaloSentBuffer
// Input arguments:
//   : buffer - GridVectorHaloSentBuffer structure
void free_halo_sent_buffer(GridVectorHaloSentBuffer *buffer);


// Struct for storing dirichle boundary conditions for Right and Top borders
// *------^
//        |
//    G   |
//        |
//        *
// values is stored from left-top point -> right-top point -> right-bottom point
// * points are INCLUDED
// ^ point is stored TWICE
struct DirichleBoundaryConditionRT {
    double *values;
};

typedef struct DirichleBoundaryConditionRT DirichleBoundaryConditionRT;

// Function to extract dirichle boundary condition from packed values in DirichleBoundaryConditionRT struct
// Input arguments:
//   : conditions - DirichleBoundaryConditionRT struct
//   : M          - number of grid points on x-axis
//   : N          - number of grid points on y-axis
//   : gi         - global i-index
//   : gj         - global j-index
double get_dirichle_boundary_condition_value(const DirichleBoundaryConditionRT *dirichle,
                                             int M, int N, int gi, int gj);

// Function to pack dirichle boundary conditions on Right and Top border from external function
// Input arguments:
//   : M  - number of grid points on x-axis
//   : N  - number of grid points on y-axis
//   : A1 - grid x-axis starts from
//   : B1 - grid y-axis starts from
//   : h1 - grid step size on x-axis
//   : h2 - grid step size on y-axis
//   : f  - external function
DirichleBoundaryConditionRT *create_dirichle_boundary_condition_rt(int M, int N,
                                                                   double A1, double B1, double h1, double h2,
                                                                   double (*f)(double, double));

// Function to print dirichle boundary conditions on Right and Top border
// Input arguments:
//   : d  - conditions to print
void print_dirichle_boundary_condition(DirichleBoundaryConditionRT *d, int M, int N);

// Function to free dirichle boundary conditions struct
// Input arguments:
//   : conditions - DirichleBoundaryConditionRT struct
void free_dirichle_boundary_condition_rt(DirichleBoundaryConditionRT *dirichle);

// Struct for storing neumann boundary conditions for Left and Bottom borders
// *
// |
// |   G
// |
// ^-------*
// values is stored from left-top point -> left-bottom point -> right-bottom point
// * points are INCLUDED (but not available to export)
// ^ point is stored TWICE
struct NeumannBoundaryConditionLB {
    double *values;
};

typedef struct NeumannBoundaryConditionLB NeumannBoundaryConditionLB;

// Function to extract dirichle boundary condition from packed values in DirichleBoundaryConditionRT struct
// Input arguments:
//   : conditions - DirichleBoundaryConditionRT struct
//   : M          - number of grid points on x-axis
//   : N          - number of grid points on y-axis
//   : gi         - global i-index
//   : gj         - global j-index
double get_neumann_boundary_condition_value(const NeumannBoundaryConditionLB *neumann,
                                            int M, int N, int gi, int gj);

// Function to pack neumann boundary conditions on Left and Bottom border from external function
// Input arguments:
//   : M  - number of grid points on x-axis
//   : N  - number of grid points on y-axis
//   : A1 - grid x-axis starts from
//   : B1 - grid y-axis starts from
//   : h1 - grid step size on x-axis
//   : h2 - grid step size on y-axis
//   : f  - external function
NeumannBoundaryConditionLB *create_neumann_boundary_condition_lb(int M, int N,
                                                                 double A1, double B1, double h1, double h2,
                                                                 double (*f)(double, double, int));

// Function to print neumann boundary conditions on Left and Bottom border
// Input arguments:
//   : n  - conditions to print
void print_neumann_boundary_condition_lb(NeumannBoundaryConditionLB *n, int M, int N);

// Function to free neumann boundary conditions struct
// Input arguments:
//   : conditions - NeumannBoundaryConditionLB struct
void free_neumann_boundary_condition_lb(NeumannBoundaryConditionLB *neumann);


#endif //SOLVE_DISTRIBUTED_VECTOR_H
