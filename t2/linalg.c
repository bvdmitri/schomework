// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//
// Created by Dmitri Bagaev on 16/11/2018.
//

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include "linalg.h"

double scalar_product_E(MPI_Comm comm, int M, int N, double h1, double h2, const GridVector *u, const GridVector *v) {
  assert(u->istart == v->istart && u->iend == v->iend &&
         u->jstart == v->jstart && u->jend == v->jend &&
         u->ilocal == v->ilocal && u->jlocal == v->jlocal);

  double local_sum = 0.0;

  const double *const uvalues = u->values;
  const double *const vvalues = v->values;

  #ifdef _OPENMP
  #pragma omp parallel for reduction(+ : local_sum)
  #endif
  for (int k = 0; k < v->ilocal * v->jlocal; ++k) {
//    int gi = v->istart + k % v->ilocal;   // global i index
//    int gj = v->jstart + k / v->ilocal;   // global j index
//
//    double pi  = gi == 0 || gi == M ? 1.0 : 1.0;
//    double pj  = gj == 0 || gj == N ? 1.0 : 1.0;
//    double pij = pi * pj;
//
//    local_sum += pij * u->values[k] * v->values[k];
    local_sum += uvalues[k] * vvalues[k];
  }

  #ifndef NDEBUG
  if (isnan(local_sum) || isinf(local_sum)) {
    printf("Nan catched on local_sum in scalar_product_E: %f\n", local_sum);
    check_vector(u, "u");
    check_vector(u, "v");
    exit(-1);
  }
  #endif
  double global_sum = 0.0;
  MPI_Allreduce(&local_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, comm);
  #ifndef NDEBUG
  if (isnan(global_sum) || isinf(global_sum)) {
    printf("Nan catched on global_sum in scalar_product_E: %f\n", global_sum);
    exit(-1);
  }
  #endif
  return h1 * h2 * global_sum;
}

double
scalar_product_E_sub(MPI_Comm comm, int M, int N, double h1, double h2, const GridVector *u, const GridVector *v) {
  assert(u->istart == v->istart && u->iend == v->iend &&
         u->jstart == v->jstart && u->jend == v->jend &&
         u->ilocal == v->ilocal && u->jlocal == v->jlocal);

  double local_sum = 0.0;

  const double *const uvalues = u->values;
  const double *const vvalues = v->values;

  #ifdef _OPENMP
  #pragma omp parallel for reduction(+ : local_sum)
  #endif
  for (int k = 0; k < v->ilocal * v->jlocal; ++k) {
//    int gi = v->istart + k % v->ilocal;   // global i index
//    int gj = v->jstart + k / v->ilocal;   // global j index
//
//    double pi  = gi == 0 || gi == M ? 1.0 : 1.0;
//    double pj  = gj == 0 || gj == N ? 1.0 : 1.0;
//    double pij = pi * pj;

//    local_sum += pij * (u->values[k] - v->values[k]) * (u->values[k] - v->values[k]);
    double sub = uvalues[k] - vvalues[k];
    local_sum += sub * sub;
  }

  #ifndef NDEBUG
  if (isnan(local_sum) || isinf(local_sum)) {
    printf("Nan catched on local_sum in scalar_product_E_sub: %f\n", local_sum);
    check_vector(u, "u");
    check_vector(u, "v");
    exit(-1);
  }
  #endif
  double global_sum = 0.0;
  MPI_Allreduce(&local_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, comm);
  #ifndef NDEBUG
  if (isnan(global_sum) || isinf(global_sum)) {
    printf("Nan catched on global_sum in scalar_product_E_sub: %f\n", global_sum);
    exit(-1);
  }
  #endif
  return h1 * h2 * global_sum;
}

double norm_E(MPI_Comm comm, int M, int N, double h1, double h2, const GridVector *u) {
  double scalar = scalar_product_E(comm, M, N, h1, h2, u, u);
  return sqrt(scalar);
}

double norm_E_sub(MPI_Comm comm, int M, int N, double h1, double h2, const GridVector *u1, const GridVector *u2) {
  double scalar = scalar_product_E_sub(comm, M, N, h1, h2, u1, u2);
  return sqrt(scalar);
}

double norm_C_sub(MPI_Comm comm, int M, int N, const GridVector *u1, const GridVector *u2) {
  assert(u1->istart == u2->istart && u1->iend == u2->iend &&
         u1->jstart == u2->jstart && u1->jend == u2->jend &&
         u1->ilocal == u2->ilocal && u1->jlocal == u2->jlocal);

  const double *const u1values = u1->values;
  const double *const u2values = u2->values;

  double   max = -1.0;
  for (int k   = 0; k < u1->ilocal * u1->jlocal; ++k) {
    max = fmax(max, fabs(u1values[k] - u2values[k]));
  }

  double global_max = 0.0;
  MPI_Allreduce(&max, &global_max, 1, MPI_DOUBLE, MPI_MAX, comm);
  return global_max;
}


double norm_L2_sub(MPI_Comm comm, int M, int N, const GridVector *u1, const GridVector *u2) {
  assert(u1->istart == u2->istart && u1->iend == u2->iend &&
         u1->jstart == u2->jstart && u1->jend == u2->jend &&
         u1->ilocal == u2->ilocal && u1->jlocal == u2->jlocal);

  double local_sum = 0.0;

  const double *const u1values = u1->values;
  const double *const u2values = u2->values;

  for (int k = 0; k < u1->ilocal * u1->jlocal; ++k) {
    local_sum += pow(u1values[k] - u2values[k], 2.0);
  }

  double global_sum = 0.0;
  MPI_Allreduce(&local_sum, &global_sum, 1, MPI_DOUBLE, MPI_SUM, comm);
  return sqrt(global_sum);
}
