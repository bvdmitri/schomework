//
// Created by bvdmitri on 11.12.18.
//

#include <stdlib.h>
#include <stdio.h>
#include "stats.h"

void stats_start(TimeStatsTimer *timer) {
  timer->tmp = MPI_Wtime();
}

void stats_end(TimeStatsTimer *timer) {
  timer->sec += (MPI_Wtime() - timer->tmp);
}

TimeStats* create_time_stats() {
  TimeStats *stats = (TimeStats *) calloc(1, sizeof(TimeStats));

  stats->program.sec = 0.0;
  stats->init.sec = 0.0;
  stats->solver.sec = 0.0;
  stats->send.sec = 0.0;
  stats->receive.sec = 0.0;
  stats->r.sec = 0.0;
  stats->Ar.sec = 0.0;
  stats->wmtr.sec = 0.0;
  stats->scpr.sec = 0.0;

  return stats;
}

void print_time_stats(TimeStats *stats) {
  printf("%10s\t%10s\t%10s\t%10s\t%10s\t%10s\t%10s\t%10s\t%10s\n",
         "Program", "Init", "Solver", "Send", "Receive", "r", "Ar", "wmtr", "scpr");
  printf("%10lf\t%10lf\t%10lf\t%10lf\t%10lf\t%10lf\t%10lf\t%10lf\t%10lf\n",
         stats->program.sec,
         stats->init.sec,
         stats->solver.sec,
         stats->send.sec,
         stats->receive.sec,
         stats->r.sec,
         stats->Ar.sec,
         stats->wmtr.sec,
         stats->scpr.sec);
}

void free_time_stats(TimeStats *stats) {
  free(stats);
}