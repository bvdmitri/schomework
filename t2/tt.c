//
// Created by bvdmitri on 24.11.18.
//

// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//
// Created by Dmitri Bagaev on 16/11/2018.
//

#include <mpi.h>
#include <printf.h>
#include <assert.h>
#include "vector.h"
#include <math.h>

void sequential_sync_start(const MPIEnvironment *env) {
  if (env->rank != 0) {
    int temp;
    MPI_Recv(&temp, 1, MPI_INT, env->rank - 1, 0, env->comm, NULL);
  }
}

void sequential_sync_end(const MPIEnvironment *env) {
  if (env->rank + 1 < env->size) {
    int temp = 1;
    MPI_Send(&temp, 1, MPI_INT, env->rank + 1, 0, env->comm);
  }
}

void test_sync_grip_views(const MPIEnvironment *env, int M, int N, int verbose) {

  if (env->rank == 0) {
    printf("[Start] Testing sync_grip_views with M = %2d, N = %2d ", M, N);
    if (verbose) printf("\n");
  }

  GridVector             *w    = create_grid_vector(env, M, N, 0.0);
  GridVectorHaloPoints     *view = create_halo_points(env, w->ilocal, w->jlocal);
  GridVectorHaloSentBuffer *sent = create_halo_sent_buffer(env, w->ilocal, w->jlocal);

  sequential_sync_start(env);
  if (verbose) printf("Block on rank %d: ", env->rank);

  if (verbose) printf("[");
  for (int j = 0; j < w->jlocal; ++j) {
    if (verbose) printf("[");
    for (int i = 0; i < w->ilocal; ++i) {
      w->values[j * w->ilocal + i] = env->rank * 100 + (j * w->ilocal + i);
      if (verbose) printf("%lf ", w->values[j * w->ilocal + i]);
    }
    if (verbose) printf("]\n");
  }
  if (verbose) printf("]\n");

  sequential_sync_end(env);

  MPI_Barrier(MPI_COMM_WORLD);

  sync_halo_points(env, w, view, sent);

  if (view->jlocal != w->jlocal || view->ilocal != w->ilocal) {
    fprintf(stderr, "Invalid jlocal or ilocal in received view\n");
    exit(-1);
  }

  sequential_sync_start(env);
  if (verbose) printf("Received view on rank %d: \n", env->rank);

  if (verbose) printf("\t istart - 1 column: ");
  if (view->istartm1column != NULL) {
    if (verbose) printf("[");
    for (int j = 0; j < view->jlocal; ++j) {
      if (verbose) printf("%lf ", view->istartm1column[j]);
      assert(view->istartm1column[j] == (env->rleft) * 100 + view->ilocal * (j + 1) - 1);
    }
    if (verbose) printf("]");
  } else {
    if (verbose) printf("[ empty ]");
  }
  if (verbose) printf("\n");

  if (verbose) printf("\t iend column: ");
  if (view->iendcolumn != NULL) {
    if (verbose) printf("[");
    for (int j = 0; j < view->jlocal; ++j) {
      if (verbose) printf("%lf ", view->iendcolumn[j]);
      assert(view->iendcolumn[j] == (env->rright) * 100 + view->ilocal * j);
    }
    if (verbose) printf("]");
  } else {
    if (verbose) printf("[ empty ]");
  }
  if (verbose) printf("\n");

  if (verbose) printf("\t jstart - 1 row: ");
  if (view->jstartm1row != NULL) {
    if (verbose) printf("[");
    for (int i = 0; i < view->ilocal; ++i) {
      if (verbose) printf("%lf ", view->jstartm1row[i]);
      assert(view->jstartm1row[i] == (env->rbottom) * 100 + i + view->ilocal * (view->jlocal - 1));
    }
    if (verbose) printf("]");
  } else {
    if (verbose) printf("[ empty ]");
  }
  if (verbose) printf("\n");

  if (verbose) printf("\t jend row: ");
  if (view->jendrow != NULL) {
    if (verbose) printf("[");
    for (int i = 0; i < view->ilocal; ++i) {
      if (verbose) printf("%lf ", view->jendrow[i]);
      assert(view->jendrow[i] == (env->rtop) * 100 + i);
    }
    if (verbose) printf("]");
  } else {
    if (verbose) printf("[ empty ]");
  }
  if (verbose) printf("\n");

  fflush(stdout);
  sequential_sync_end(env);


  MPI_Barrier(MPI_COMM_WORLD);

  free_halo_sent_buffer(sent);
  free_halo_points(view);
  free_grid_vector(w);

  if (env->rank == 0) {
    printf("[Success] \n");
  }

  MPI_Barrier(MPI_COMM_WORLD);
}

//void test_create_grid_vector(int rank, int size, int M, int N, int verbose) {
//  if (rank == 0) {
//    printf("[Start] Testing create_grid_vector with M = %2d, N = %2d ", M, N);
//    if (verbose) printf("\n");
//  }
//
//  GridVector *v = create_grid_vector(size, rank, M, N, 0.0);
//
//  sequential_sync_start(rank, size);
//
//  if (verbose)
//    printf("Created vector on rank %d = { istart = %d, iend = %d, jstart = %d, jend = %d, iglobal = %d, jglobal = %d, ilocal = %d, jlocal = %d }\n",
//           rank, v->istart, v->iend, v->jstart, v->jend, v->iglobal, v->jglobal, v->ilocal, v->jlocal);
//
//  sequential_sync_end(rank, size);
//
//  delete_grid_vector(v);
//
//  MPI_Barrier(MPI_COMM_WORLD);
//
//  if (rank == 0) {
//    printf("[Success] \n");
//  }
//
//  MPI_Barrier(MPI_COMM_WORLD);
//}


int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);

  MPIEnvironment *env = create_mpi_environment();

  sequential_sync_start(env);
  print_mpi_environment(env);
  sequential_sync_end(env);
  fflush(stdout);

  MPI_Barrier(env->comm);

//  test_create_grid_vector(rank, size, 4, 4, 1);

  test_sync_grip_views(env, 4, 4, 1);
  test_sync_grip_views(env, 8, 4, 1);
  test_sync_grip_views(env, 8, 8, 0);
  test_sync_grip_views(env, 4, 8, 0);
  test_sync_grip_views(env, 12, 8, 0);
  test_sync_grip_views(env, 8, 12, 0);
  test_sync_grip_views(env, 16, 16, 0);
  test_sync_grip_views(env, 20, 20, 0);
  test_sync_grip_views(env, 40, 40, 0);
  test_sync_grip_views(env, 80, 80, 0);

  free_mpi_environment(env);

  MPI_Finalize();
}



