// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

#include "vector.h"
#include "stats.h"
#include "solver.h"
#include "linalg.h"
#include <limits.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif

#if defined(_OPENMP)

#include <omp.h>

#endif

double U(double x, double y) {
  return 1 + cos(M_PI * x * y);
}

double psi(double x, double y, int flag) {

//  if (flag == 1) { // Left border
//    return -sin(M_PI * x * y) * M_PI * x;
//  } else if (flag == 2) {// Bottom border
//    return -sin(M_PI * x * y) * M_PI * y;
//  }
  return 0.0;

//  printf("[ERROR] Unreachable code in psi\n");
//  exit(-1);
}

double F(double x, double y) {
  return M_PI * M_PI * (x * x + y * y) * cos(M_PI * x * y);
}

//double U(double x, double y) {
//  return 1.0;
//}
//
//double psi(double x, double y, int flag) {
//
//  if (flag == 1) { // Left border
//    return 0.0;
//  } else if (flag == 2) {// Bottom border
//    return 0.0;
//  }
//
//  printf("[ERROR] Unreachable code in psi\n");
//  exit(-1);
//}
//
//double F(double x, double y) {
//  return 0.0;
//}

void print_vector_C(GridVector *v, GridVector *u) {

//  printf("\n");
//  for (int k = 0; k < v->ilocal * v->jlocal; ++k) {
//    int gi = v->istart + k % v->ilocal;   // global i index
//    int gj = v->jstart + k / v->ilocal;   // global j index
//
//    if ((gi - v->istart) == 0) {
//      printf("[");
//    }
//    printf("(%d, %d) ", gi, gj);
//    if ((gi - v->istart) == v->ilocal - 1) {
//      printf("]\n");
//    }
//  }

  printf("\n");

  for (int j = v->jlocal - 1; j >= 0; --j) {
    printf("[");
    for (int i = 0; i < v->ilocal; ++i) {
      printf("%f ", fabs(v->values[j * v->ilocal + i % v->ilocal] - u->values[j * u->ilocal + i % u->ilocal]));
    }
    printf("]\n");
  }
}

int main(int argc, char **argv) {

  MPI_Init(&argc, &argv);

  TimeStats *stats = create_time_stats();
  stats_start(&(stats->program));
  stats_start(&(stats->init));

  MPIEnvironment *env = create_mpi_environment();

  int rank = env->rank;
  int size = env->size;

  // Parse arguments
  if (argc < 8) {
    printf("Usage: ./solve A1 A2 B1 B2 M N eps <max_it>\n");
    return 0;
  }

  double     A1    = strtod(argv[1], NULL);
  double     A2    = strtod(argv[2], NULL);
  double     B1    = strtod(argv[3], NULL);
  double     B2    = strtod(argv[4], NULL);
  int        M     = (int) strtol(argv[5], NULL, 10);
  int        N     = (int) strtol(argv[6], NULL, 10);
  double     eps   = strtod(argv[7], NULL);
  int        maxit = argc > 8 ? (int) strtol(argv[8], NULL, 10) : INT_MAX;
  int        write = argc > 9 ? (int) strtol(argv[9], NULL, 10) : -1;
  const char *out  = argc > 10 ? argv[10] : "grid_out";

  double h1 = (A2 - A1) / M;
  double h2 = (B2 - B1) / N;

  if (rank == 0) {
    printf("MPI: %d size\n", size);
    #if defined(_OPENMP)
    printf("OpenMP: %d max threads\n", omp_get_max_threads());
    #else
    printf("OpenMP disabled\n");
    #endif
    printf("Omega = [ %f, %f ] x [ %f, %f ]\n", A1, A2, B1, B2);
    printf("Grid  = [ %d, %d ] x [ %d, %d ]\n", 0, M, 0, N);
    printf("h1 = %f\nh2 = %f\n", h1, h2);
    printf("----------------------------------------\n");
  }

  // Create packed values of boundary conditions on all processes
  DirichleBoundaryConditionRT *dirichle = create_dirichle_boundary_condition_rt(M, N, A1, B1, h1, h2, U);
  NeumannBoundaryConditionLB  *neumann  = create_neumann_boundary_condition_lb(M, N, A1, B1, h1, h2, psi);

  // Create u and F grid function
  GridVector *u = create_grid_vector_from_function(env, M, N, A1, B1, h1, h2, U);
  GridVector *f = create_grid_vector_from_function(env, M, N, A1, B1, h1, h2, F);

  // Create wk and wk+1 grid vectors
  GridVector *wk   = create_grid_vector(env, M, N, 0.0);    // w_k
  GridVector *wkp1 = create_grid_vector(env, M, N, 0.0);    // w_k+1

  // Buffers for synchronization between processes
  GridVectorHaloPoints     *grip = create_halo_points(env, wk->ilocal, wk->jlocal);
  GridVectorHaloSentBuffer *sent = create_halo_sent_buffer(env, wk->ilocal, wk->jlocal);

  // Vector for rk and Ark
  GridVector *rk  = create_grid_vector(env, M, N, 0.0);
  GridVector *Ark = create_grid_vector(env, M, N, 0.0);
  stats_end(&(stats->init));

  #ifndef NDEBUG
  check_vector(u, "u");
  check_vector(f, "f");
  #endif

  double norm = 1.0;     // norm of residual
  int    iter = 0;       // iterations number

  stats_start(&(stats->solver));
  while (1) {
    if (iter == maxit) {
      if (rank == 0) {
        printf("Exited due to maximum iteration restriction\n");
      }
      break;
    }

    stats_start(&(stats->scpr));
    norm = norm_E(env->comm, M, N, h1, h2, rk);
    stats_end(&(stats->scpr));
    if (iter != 0 && norm < eps) {
      break;
    }

    if (rank == 0 && iter % 10 == 0) {
      printf("%10d %.10f\n", iter, norm);
    }

    iter += 1;

    sync_halo_points(env, wk, grip, sent, stats);
    stats_start(&(stats->r));
    compute_r(M, N, h1, h2, wk, grip, f, dirichle, neumann, rk);
    stats_end(&(stats->r));

    #ifndef NDEBUG
    check_vector(rk, "rk");
    #endif

    sync_halo_points(env, rk, grip, sent, stats);
    stats_start(&(stats->Ar));
    compute_Aw(M, N, h1, h2, rk, grip, Ark);
    stats_end(&(stats->Ar));

    #ifndef NDEBUG
    check_vector(Ark, "Ark");
    #endif

    stats_start(&(stats->scpr));
    double t = scalar_product_E(env->comm, M, N, h1, h2, Ark, rk) / scalar_product_E(env->comm, M, N, h1, h2, Ark, Ark);
    stats_end(&(stats->scpr));

    stats_start(&(stats->wmtr));
    compute_wmtr(wkp1, wk, rk, t);
    stats_end(&(stats->wmtr));

    #ifndef NDEBUG
    check_vector(wkp1, "wkp1");
    #endif

    // Swap wk and wkp1
    GridVector *tmp = wk;
    wk   = wkp1;
    wkp1 = tmp;

//    if (iter == write) {
//      write_grid_vector(out, M, N, rk);
//    }
  }
  stats_end(&(stats->solver));

  stats_start(&(stats->scpr));
  double rnorm  = norm_E_sub(env->comm, M, N, h1, h2, wk, u);
  double cnorm  = norm_C_sub(env->comm, M, N, wk, u);
  double l2norm = norm_L2_sub(env->comm, M, N, wk, u);
  stats_end(&(stats->scpr));

  stats_end(&(stats->program));
  if (rank == 0) {
    printf("----------------------------------------\n");
    printf("Residual norm: %.8f\n", norm);
    printf("E norm       : %.8f\n", rnorm);
    printf("L2 norm      : %.8f\n", l2norm);
    printf("C norm       : %.8f\n", cnorm);
    printf("Iterations   : %d \n", iter);
    print_time_stats(stats);
  }


  // Free boundary conditions memory
  free_neumann_boundary_condition_lb(neumann);
  free_dirichle_boundary_condition_rt(dirichle);

  // Free grid vectors
  free_grid_vector(f);
  free_grid_vector(u);
  free_grid_vector(wk);
  free_grid_vector(wkp1);

  // Free synchronization buffers
  free_halo_sent_buffer(sent);
  free_halo_points(grip);

  free_mpi_environment(env);

  free_time_stats(stats);

  MPI_Finalize();
  return 0;
}