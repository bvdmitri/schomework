import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import matplotlib.ticker as ticker


def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    return r'${} \times 10^{{{}}}$'.format(a, b)


def results_plot(ff):
    f = open(ff, "r")
    line = f.readline()
    m = int(line.split()[0])
    n = int(line.split()[1])
    x = np.zeros((m, n))
    for line in f:
        i = int(line.split()[0])
        j = int(line.split()[1])
        x[i, j] = float(line.split()[-1])

    cmap = "GnBu"
    if "err" in ff:
        cmap = 'RdYlGn'

    plt.matshow(x, interpolation="none", cmap=cmap)
    plt.gca().invert_yaxis()
    plt.gca().xaxis.tick_bottom()
    if "err" in ff:
        plt.colorbar(format=ticker.FuncFormatter(fmt))
    else:
        plt.colorbar(format=ticker.FuncFormatter(fmt))
    plt.savefig(ff + '.pdf', format='pdf', bbox_inches='tight')


directory = "report/pics"
onlyfiles = [f for f in listdir(directory) if isfile(join(directory, f)) and not "pdf" in f]

print(onlyfiles)
for f in onlyfiles:
    results_plot(directory + "/" + f)
