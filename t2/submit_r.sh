mkdir -p results
mpisubmit.bg -w 01:00:00 -n $1 --mode smp --stdout results/out_$2.hybrid.$1 --stderr results/err_$2.hybrid.$1 solve_r -- 0.0 2.0 0.0 1.0 $2 $2 1e-6 1700000