//
// Created by Dmitri Bagaev on 16/11/2018.
//

#include "vector.h"

#ifndef SOLVE_LINALG_H
#define SOLVE_LINALG_H

// Function to compute scalar product in energy space
// Computed like [ u, v ] = sum(i = 0, M) * h1 * sum(j = 0, N) * (p_ij * u_ij * v_ij)
//         where p_ij = p_i * p_j
//               p_i = if i \in [ 1, M - 1 = 1 else 1/2
//               p_j = if j \in [ 1, N - 1 = 1 else 1/2
// Input arguments:
//   : comm - MPI communicator
//   : M    - number of grid points on x-axis
//   : N    - number of grid points on y-axis
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u,v  - vectors to compute scalar-product for
double scalar_product_E(MPI_Comm comm, int M, int N, double h1, double h2, const GridVector *u, const GridVector *v);

// Function to compute scalar product in energy space
// Computed like [ u - v, v - v ] = sum(i = 0, M) * h1 * sum(j = 0, N) * (p_ij * (u_ij - v_ij) * (u_ij - v_ij))
//         where p_ij = p_i * p_j
//               p_i = if i \in [ 1, M - 1 = 1 else 1/2
//               p_j = if j \in [ 1, N - 1 = 1 else 1/2
// Input arguments:
//   : comm - MPI communicator
//   : M    - number of grid points on x-axis
//   : N    - number of grid points on y-axis
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u,v  - vectors to compute scalar-product for
double scalar_product_E_sub(MPI_Comm comm, int M, int N, double h1, double h2, const GridVector *u, const GridVector *v);

// Function to compute norm in energy space
// Computed like ||u|| = sqrt([ u, u ])
// Input arguments:
//   : comm - MPI communicator
//   : M    - number of grid points on x-axis
//   : N    - number of grid points on y-axis
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u    - vector to compute norm for
double norm_E(MPI_Comm comm, int M, int N, double h1, double h2, const GridVector *u);

// Function to compute norm in energy space
// Computed like ||u1 - u2|| = sqrt([ u1 - u2, u1 - u2 ])
// Input arguments:
//   : comm - MPI communicator
//   : M    - number of grid points on x-axis
//   : N    - number of grid points on y-axis
//   : h1   - grid step on x-axis
//   : h2   - grid step on y-axis
//   : u1   - vector to substract from
//   : u2   - vector to substract
double norm_E_sub(MPI_Comm comm, int M, int N, double h1, double h2, const GridVector *u1, const GridVector *u2);

// Function to compute C norm of v = u1 - u2 vector
// Computed like ||v|| = max|v|
// Input arguments:
//   : comm - MPI communicator
//   : M    - number of grid points on x-axis
//   : N    - number of grid points on y-axis
//   : u1   - vector to substract from
//   : u2   - vector to substract
double norm_C_sub(MPI_Comm comm, int M, int N, const GridVector *u1, const GridVector *u2);

// Function to compute L2 norm of v = u1 - u2 vector
// Input arguments:
//   : comm - MPI communicator
//   : M    - number of grid points on x-axis
//   : N    - number of grid points on y-axis
//   : u1   - vector to substract from
//   : u2   - vector to substract
double norm_L2_sub(MPI_Comm comm, int M, int N, const GridVector *u1, const GridVector *u2);

#endif //SOLVE_LINALG_H
