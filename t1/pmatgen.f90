SUBROUTINE PSMATGEN(ICTXT, AFORM, DIAG, M, N, MB, NB, A, LDA, &
        IAROW, IACOL, ISEED, IROFF, IRNUM, ICOFF, &
        ICNUM, MYROW, MYCOL, NPROW, NPCOL)
    !
    !  -- ScaLAPACK routine (version 1.7) --
    !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
    !     and University of California, Berkeley.
    !     May 1, 1997
    !
    !     .. Scalar Arguments ..
    CHARACTER*1        AFORM, DIAG
    INTEGER            IACOL, IAROW, ICNUM, ICOFF, ICTXT, IRNUM, &
            IROFF, ISEED, LDA, M, MB, MYCOL, MYROW, N, &
            NB, NPCOL, NPROW
    !     ..
    !     .. Array Arguments ..
    REAL               A(LDA, *)
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  PSMATGEN : Parallel Real Single precision MATrix GENerator.
    !  Generate (or regenerate) a distributed matrix A (or sub-matrix of A).
    !
    !  Arguments
    !  =========
    !
    !  ICTXT   (global input) INTEGER
    !          The BLACS context handle, indicating the global context of
    !          the operation. The context itself is global.
    !
    !  AFORM   (global input) CHARACTER*1
    !          if AFORM = 'S' : A is returned is a symmetric matrix.
    !          if AFORM = 'H' : A is returned is a Hermitian matrix.
    !          if AFORM = 'T' : A is overwritten with the transpose of
    !                           what would normally be generated.
    !          if AFORM = 'C' : A is overwritten with the conjugate trans-
    !                           pose of what would normally be generated.
    !          otherwise a random matrix is generated.
    !
    !  DIAG    (global input) CHARACTER*1
    !          if DIAG = 'D' : A is diagonally dominant.
    !
    !  M       (global input) INTEGER
    !          The number of rows in the generated distributed matrix.
    !
    !  N       (global input) INTEGER
    !          The number of columns in the generated distributed
    !          matrix.
    !
    !  MB      (global input) INTEGER
    !          The row blocking factor of the distributed matrix A.
    !
    !  NB      (global input) INTEGER
    !          The column blocking factor of the distributed matrix A.
    !
    !  A       (local output) REAL, pointer into the local memory
    !          to an array of dimension ( LDA, * ) containing the local
    !          pieces of the distributed matrix.
    !
    !  LDA     (local input) INTEGER
    !          The leading dimension of the array containing the local
    !          pieces of the distributed matrix A.
    !
    !  IAROW   (global input) INTEGER
    !          The row processor coordinate which holds the first block
    !          of the distributed matrix A.
    !
    !  IACOL   (global input) INTEGER
    !          The column processor coordinate which holds the first
    !          block of the distributed matrix A.
    !
    !  ISEED   (global input) INTEGER
    !          The seed number to generate the distributed matrix A.
    !
    !  IROFF   (local input) INTEGER
    !          The number of local rows of A that have already been
    !          generated.  It should be a multiple of MB.
    !
    !  IRNUM   (local input) INTEGER
    !          The number of local rows to be generated.
    !
    !  ICOFF   (local input) INTEGER
    !          The number of local columns of A that have already been
    !          generated.  It should be a multiple of NB.
    !
    !  ICNUM   (local input) INTEGER
    !          The number of local columns to be generated.
    !
    !  MYROW   (local input) INTEGER
    !          The row process coordinate of the calling process.
    !
    !  MYCOL   (local input) INTEGER
    !          The column process coordinate of the calling process.
    !
    !  NPROW   (global input) INTEGER
    !          The number of process rows in the grid.
    !
    !  NPCOL   (global input) INTEGER
    !          The number of process columns in the grid.
    !
    !  Notes
    !  =====
    !
    !  The code is originally developed by David Walker, ORNL,
    !  and modified by Jaeyoung Choi, ORNL.
    !
    !  Reference: G. Fox et al.
    !  Section 12.3 of "Solving problems on concurrent processors Vol. I"
    !
    !  =====================================================================
    !
    !     .. Parameters ..
    INTEGER            MULT0, MULT1, IADD0, IADD1
    PARAMETER        (MULT0 = 20077, MULT1 = 16838, IADD0 = 12345, &
            IADD1 = 0)
    REAL               ONE, TWO
    PARAMETER          (ONE = 1.0E+0, TWO = 2.0E+0)
    !     ..
    !     .. Local Scalars ..
    LOGICAL            SYMM, HERM, TRAN
    INTEGER            I, IC, IK, INFO, IOFFC, IOFFR, IR, J, JK, &
            JUMP1, JUMP2, JUMP3, JUMP4, JUMP5, JUMP6, &
            JUMP7, MAXMN, MEND, MOFF, MP, MRCOL, MRROW, &
            NEND, NOFF, NPMB, NQ, NQNB
    !     ..
    !     .. Local Arrays ..
    INTEGER            IADD(2), IA1(2), IA2(2), IA3(2), IA4(2), &
            IA5(2), IB1(2), IB2(2), IB3(2), IC1(2), IC2(2), &
            IC3(2), IC4(2), IC5(2), IRAN1(2), IRAN2(2), &
            IRAN3(2), IRAN4(2), ITMP1(2), ITMP2(2), &
            ITMP3(2), JSEED(2), MULT(2)
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           JUMPIT, PXERBLA, SETRAN, XJUMPM
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC ABS, MAX, MOD
    !     ..
    !     .. External Functions ..
    LOGICAL            LSAME
    INTEGER            ICEIL, NUMROC
    REAL               PSRAND
    EXTERNAL           ICEIL, NUMROC, LSAME, PSRAND
    !     ..
    !     .. Executable Statements ..
    !
    !     Test the input arguments
    !
    MP = NUMROC(M, MB, MYROW, IAROW, NPROW)
    NQ = NUMROC(N, NB, MYCOL, IACOL, NPCOL)
    SYMM = LSAME(AFORM, 'S')
    HERM = LSAME(AFORM, 'H')
    TRAN = LSAME(AFORM, 'T')
    !
    INFO = 0
    IF(.NOT.LSAME(DIAG, 'D') .AND.&
            .NOT.LSAME(DIAG, 'N')) THEN
        INFO = 3
    ELSE IF(SYMM.OR.HERM) THEN
        IF(M.NE.N) THEN
            INFO = 5
        ELSE IF(MB.NE.NB) THEN
            INFO = 7
        END IF
    ELSE IF(M.LT.0) THEN
        INFO = 4
    ELSE IF(N.LT.0) THEN
        INFO = 5
    ELSE IF(MB.LT.1) THEN
        INFO = 6
    ELSE IF(NB.LT.1) THEN
        INFO = 7
    ELSE IF(LDA.LT.0) THEN
        INFO = 9
    ELSE IF((IAROW.LT.0).OR.(IAROW.GE.NPROW)) THEN
        INFO = 10
    ELSE IF((IACOL.LT.0).OR.(IACOL.GE.NPCOL)) THEN
        INFO = 11
    ELSE IF(MOD(IROFF, MB).GT.0) THEN
        INFO = 13
    ELSE IF(IRNUM.GT.(MP - IROFF)) THEN
        INFO = 14
    ELSE IF(MOD(ICOFF, NB).GT.0) THEN
        INFO = 15
    ELSE IF(ICNUM.GT.(NQ - ICOFF)) THEN
        INFO = 16
    ELSE IF((MYROW.LT.0).OR.(MYROW.GE.NPROW)) THEN
        INFO = 17
    ELSE IF((MYCOL.LT.0).OR.(MYCOL.GE.NPCOL)) THEN
        INFO = 18
    END IF
    IF(INFO.NE.0) THEN
        CALL PXERBLA(ICTXT, 'PSMATGEN', INFO)
        RETURN
    END IF
    !
    MRROW = MOD(NPROW + MYROW - IAROW, NPROW)
    MRCOL = MOD(NPCOL + MYCOL - IACOL, NPCOL)
    NPMB = NPROW * MB
    NQNB = NPCOL * NB
    MOFF = IROFF / MB
    NOFF = ICOFF / NB
    MEND = ICEIL(IRNUM, MB) + MOFF
    NEND = ICEIL(ICNUM, NB) + NOFF
    !
    MULT(1) = MULT0
    MULT(2) = MULT1
    IADD(1) = IADD0
    IADD(2) = IADD1
    JSEED(1) = ISEED
    JSEED(2) = 0
    !
    !     Symmetric or Hermitian matrix will be generated.
    !
    IF(SYMM.OR.HERM) THEN
        !
        !        First, generate the lower triangular part (with diagonal block)
        !
        JUMP1 = 1
        JUMP2 = NPMB
        JUMP3 = M
        JUMP4 = NQNB
        JUMP5 = NB
        JUMP6 = MRCOL
        JUMP7 = MB * MRROW
        !
        CALL XJUMPM(JUMP1, MULT, IADD, JSEED, IRAN1, IA1, IC1)
        CALL XJUMPM(JUMP2, MULT, IADD, IRAN1, ITMP1, IA2, IC2)
        CALL XJUMPM(JUMP3, MULT, IADD, IRAN1, ITMP1, IA3, IC3)
        CALL XJUMPM(JUMP4, IA3, IC3, IRAN1, ITMP1, IA4, IC4)
        CALL XJUMPM(JUMP5, IA3, IC3, IRAN1, ITMP1, IA5, IC5)
        CALL XJUMPM(JUMP6, IA5, IC5, IRAN1, ITMP3, ITMP1, ITMP2)
        CALL XJUMPM(JUMP7, MULT, IADD, ITMP3, IRAN1, ITMP1, ITMP2)
        CALL XJUMPM(NOFF, IA4, IC4, IRAN1, ITMP1, ITMP2, ITMP3)
        CALL XJUMPM(MOFF, IA2, IC2, ITMP1, IRAN1, ITMP2, ITMP3)
        CALL SETRAN(IRAN1, IA1, IC1)
        !
        DO 10 I = 1, 2
            IB1(I) = IRAN1(I)
            IB2(I) = IRAN1(I)
            IB3(I) = IRAN1(I)
        10    CONTINUE
        !
        JK = 1
        DO 80 IC = NOFF + 1, NEND
            IOFFC = ((IC - 1) * NPCOL + MRCOL) * NB
            DO 70 I = 1, NB
                IF(JK .GT. ICNUM) GO TO 90
                !
                IK = 1
                DO 50 IR = MOFF + 1, MEND
                    IOFFR = ((IR - 1) * NPROW + MRROW) * MB
                    !
                    IF(IOFFR .GT. IOFFC) THEN
                        DO 20 J = 1, MB
                            IF(IK .GT. IRNUM) GO TO 60
                            A(IK, JK) = ONE - TWO * PSRAND(0)
                            IK = IK + 1
                        20                CONTINUE
                        !
                    ELSE IF(IOFFC .EQ. IOFFR) THEN
                        IK = IK + I - 1
                        IF(IK .GT. IRNUM) GO TO 60
                        DO 30 J = 1, I - 1
                            A(IK, JK) = ONE - TWO * PSRAND(0)
                        30                CONTINUE
                        A(IK, JK) = ONE - TWO * PSRAND(0)
                        DO 40 J = 1, MB - I
                            IF(IK + J .GT. IRNUM) GO TO 60
                            A(IK + J, JK) = ONE - TWO * PSRAND(0)
                            A(IK, JK + J) = A(IK + J, JK)
                        40                CONTINUE
                        IK = IK + MB - I + 1
                    ELSE
                        IK = IK + MB
                    END IF
                    !
                    CALL JUMPIT(IA2, IC2, IB1, IRAN2)
                    IB1(1) = IRAN2(1)
                    IB1(2) = IRAN2(2)
                50          CONTINUE
                !
                60          CONTINUE
                JK = JK + 1
                CALL JUMPIT(IA3, IC3, IB2, IRAN3)
                IB1(1) = IRAN3(1)
                IB1(2) = IRAN3(2)
                IB2(1) = IRAN3(1)
                IB2(2) = IRAN3(2)
            70       CONTINUE
            !
            CALL JUMPIT(IA4, IC4, IB3, IRAN4)
            IB1(1) = IRAN4(1)
            IB1(2) = IRAN4(2)
            IB2(1) = IRAN4(1)
            IB2(2) = IRAN4(2)
            IB3(1) = IRAN4(1)
            IB3(2) = IRAN4(2)
        80    CONTINUE
        !
        !        Next, generate the upper triangular part.
        !
        90    CONTINUE
        MULT(1) = MULT0
        MULT(2) = MULT1
        IADD(1) = IADD0
        IADD(2) = IADD1
        JSEED(1) = ISEED
        JSEED(2) = 0
        !
        JUMP1 = 1
        JUMP2 = NQNB
        JUMP3 = N
        JUMP4 = NPMB
        JUMP5 = MB
        JUMP6 = MRROW
        JUMP7 = NB * MRCOL
        !
        CALL XJUMPM(JUMP1, MULT, IADD, JSEED, IRAN1, IA1, IC1)
        CALL XJUMPM(JUMP2, MULT, IADD, IRAN1, ITMP1, IA2, IC2)
        CALL XJUMPM(JUMP3, MULT, IADD, IRAN1, ITMP1, IA3, IC3)
        CALL XJUMPM(JUMP4, IA3, IC3, IRAN1, ITMP1, IA4, IC4)
        CALL XJUMPM(JUMP5, IA3, IC3, IRAN1, ITMP1, IA5, IC5)
        CALL XJUMPM(JUMP6, IA5, IC5, IRAN1, ITMP3, ITMP1, ITMP2)
        CALL XJUMPM(JUMP7, MULT, IADD, ITMP3, IRAN1, ITMP1, ITMP2)
        CALL XJUMPM(MOFF, IA4, IC4, IRAN1, ITMP1, ITMP2, ITMP3)
        CALL XJUMPM(NOFF, IA2, IC2, ITMP1, IRAN1, ITMP2, ITMP3)
        CALL SETRAN(IRAN1, IA1, IC1)
        !
        DO 100 I = 1, 2
            IB1(I) = IRAN1(I)
            IB2(I) = IRAN1(I)
            IB3(I) = IRAN1(I)
        100    CONTINUE
        !
        IK = 1
        DO 150 IR = MOFF + 1, MEND
            IOFFR = ((IR - 1) * NPROW + MRROW) * MB
            DO 140 J = 1, MB
                IF(IK .GT. IRNUM) GO TO 160
                JK = 1
                DO 120 IC = NOFF + 1, NEND
                    IOFFC = ((IC - 1) * NPCOL + MRCOL) * NB
                    IF(IOFFC .GT. IOFFR) THEN
                        DO 110 I = 1, NB
                            IF(JK .GT. ICNUM) GO TO 130
                            A(IK, JK) = ONE - TWO * PSRAND(0)
                            JK = JK + 1
                        110                CONTINUE
                    ELSE
                        JK = JK + NB
                    END IF
                    CALL JUMPIT(IA2, IC2, IB1, IRAN2)
                    IB1(1) = IRAN2(1)
                    IB1(2) = IRAN2(2)
                120          CONTINUE
                !
                130          CONTINUE
                IK = IK + 1
                CALL JUMPIT(IA3, IC3, IB2, IRAN3)
                IB1(1) = IRAN3(1)
                IB1(2) = IRAN3(2)
                IB2(1) = IRAN3(1)
                IB2(2) = IRAN3(2)
            140       CONTINUE
            !
            CALL JUMPIT(IA4, IC4, IB3, IRAN4)
            IB1(1) = IRAN4(1)
            IB1(2) = IRAN4(2)
            IB2(1) = IRAN4(1)
            IB2(2) = IRAN4(2)
            IB3(1) = IRAN4(1)
            IB3(2) = IRAN4(2)
        150    CONTINUE
        160    CONTINUE
        !
        !     (Conjugate) Transposed matrix A will be generated.
        !
    ELSE IF(TRAN .OR. LSAME(AFORM, 'C')) THEN
        !
        JUMP1 = 1
        JUMP2 = NQNB
        JUMP3 = N
        JUMP4 = NPMB
        JUMP5 = MB
        JUMP6 = MRROW
        JUMP7 = NB * MRCOL
        !
        CALL XJUMPM(JUMP1, MULT, IADD, JSEED, IRAN1, IA1, IC1)
        CALL XJUMPM(JUMP2, MULT, IADD, IRAN1, ITMP1, IA2, IC2)
        CALL XJUMPM(JUMP3, MULT, IADD, IRAN1, ITMP1, IA3, IC3)
        CALL XJUMPM(JUMP4, IA3, IC3, IRAN1, ITMP1, IA4, IC4)
        CALL XJUMPM(JUMP5, IA3, IC3, IRAN1, ITMP1, IA5, IC5)
        CALL XJUMPM(JUMP6, IA5, IC5, IRAN1, ITMP3, ITMP1, ITMP2)
        CALL XJUMPM(JUMP7, MULT, IADD, ITMP3, IRAN1, ITMP1, ITMP2)
        CALL XJUMPM(MOFF, IA4, IC4, IRAN1, ITMP1, ITMP2, ITMP3)
        CALL XJUMPM(NOFF, IA2, IC2, ITMP1, IRAN1, ITMP2, ITMP3)
        CALL SETRAN(IRAN1, IA1, IC1)
        !
        DO 170 I = 1, 2
            IB1(I) = IRAN1(I)
            IB2(I) = IRAN1(I)
            IB3(I) = IRAN1(I)
        170    CONTINUE
        !
        IK = 1
        DO 220 IR = MOFF + 1, MEND
            IOFFR = ((IR - 1) * NPROW + MRROW) * MB
            DO 210 J = 1, MB
                IF(IK .GT. IRNUM) GO TO 230
                JK = 1
                DO 190 IC = NOFF + 1, NEND
                    IOFFC = ((IC - 1) * NPCOL + MRCOL) * NB
                    DO 180 I = 1, NB
                        IF(JK .GT. ICNUM) GO TO 200
                        A(IK, JK) = ONE - TWO * PSRAND(0)
                        JK = JK + 1
                    180             CONTINUE
                    CALL JUMPIT(IA2, IC2, IB1, IRAN2)
                    IB1(1) = IRAN2(1)
                    IB1(2) = IRAN2(2)
                190          CONTINUE
                !
                200          CONTINUE
                IK = IK + 1
                CALL JUMPIT(IA3, IC3, IB2, IRAN3)
                IB1(1) = IRAN3(1)
                IB1(2) = IRAN3(2)
                IB2(1) = IRAN3(1)
                IB2(2) = IRAN3(2)
            210       CONTINUE
            !
            CALL JUMPIT(IA4, IC4, IB3, IRAN4)
            IB1(1) = IRAN4(1)
            IB1(2) = IRAN4(2)
            IB2(1) = IRAN4(1)
            IB2(2) = IRAN4(2)
            IB3(1) = IRAN4(1)
            IB3(2) = IRAN4(2)
        220    CONTINUE
        230    CONTINUE
        !
        !     A random matrix is generated.
        !
    ELSE
        !
        JUMP1 = 1
        JUMP2 = NPMB
        JUMP3 = M
        JUMP4 = NQNB
        JUMP5 = NB
        JUMP6 = MRCOL
        JUMP7 = MB * MRROW
        !
        CALL XJUMPM(JUMP1, MULT, IADD, JSEED, IRAN1, IA1, IC1)
        CALL XJUMPM(JUMP2, MULT, IADD, IRAN1, ITMP1, IA2, IC2)
        CALL XJUMPM(JUMP3, MULT, IADD, IRAN1, ITMP1, IA3, IC3)
        CALL XJUMPM(JUMP4, IA3, IC3, IRAN1, ITMP1, IA4, IC4)
        CALL XJUMPM(JUMP5, IA3, IC3, IRAN1, ITMP1, IA5, IC5)
        CALL XJUMPM(JUMP6, IA5, IC5, IRAN1, ITMP3, ITMP1, ITMP2)
        CALL XJUMPM(JUMP7, MULT, IADD, ITMP3, IRAN1, ITMP1, ITMP2)
        CALL XJUMPM(NOFF, IA4, IC4, IRAN1, ITMP1, ITMP2, ITMP3)
        CALL XJUMPM(MOFF, IA2, IC2, ITMP1, IRAN1, ITMP2, ITMP3)
        CALL SETRAN(IRAN1, IA1, IC1)
        !
        DO 240 I = 1, 2
            IB1(I) = IRAN1(I)
            IB2(I) = IRAN1(I)
            IB3(I) = IRAN1(I)
        240    CONTINUE
        !
        JK = 1
        DO 290 IC = NOFF + 1, NEND
            IOFFC = ((IC - 1) * NPCOL + MRCOL) * NB
            DO 280 I = 1, NB
                IF(JK .GT. ICNUM) GO TO 300
                IK = 1
                DO 260 IR = MOFF + 1, MEND
                    IOFFR = ((IR - 1) * NPROW + MRROW) * MB
                    DO 250 J = 1, MB
                        IF(IK .GT. IRNUM) GO TO 270
                        A(IK, JK) = ONE - TWO * PSRAND(0)
                        IK = IK + 1
                    250             CONTINUE
                    CALL JUMPIT(IA2, IC2, IB1, IRAN2)
                    IB1(1) = IRAN2(1)
                    IB1(2) = IRAN2(2)
                260          CONTINUE
                !
                270          CONTINUE
                JK = JK + 1
                CALL JUMPIT(IA3, IC3, IB2, IRAN3)
                IB1(1) = IRAN3(1)
                IB1(2) = IRAN3(2)
                IB2(1) = IRAN3(1)
                IB2(2) = IRAN3(2)
            280       CONTINUE
            !
            CALL JUMPIT(IA4, IC4, IB3, IRAN4)
            IB1(1) = IRAN4(1)
            IB1(2) = IRAN4(2)
            IB2(1) = IRAN4(1)
            IB2(2) = IRAN4(2)
            IB3(1) = IRAN4(1)
            IB3(2) = IRAN4(2)
        290    CONTINUE
        300    CONTINUE
    END IF
    !
    !     Diagonally dominant matrix will be generated.
    !
    IF(LSAME(DIAG, 'D')) THEN
        IF(MB.NE.NB) THEN
            WRITE(*, *) 'Diagonally dominant matrices with rowNB not' // &
                    ' equal colNB is not supported!'
            RETURN
        END IF
        !
        MAXMN = MAX(M, N)
        JK = 1
        DO 340 IC = NOFF + 1, NEND
            IOFFC = ((IC - 1) * NPCOL + MRCOL) * NB
            IK = 1
            DO 320 IR = MOFF + 1, MEND
                IOFFR = ((IR - 1) * NPROW + MRROW) * MB
                IF(IOFFC.EQ.IOFFR) THEN
                    DO 310 J = 0, MB - 1
                        IF(IK .GT. IRNUM) GO TO 330
                        A(IK, JK + J) = ABS(A(IK, JK + J)) + MAXMN
                        IK = IK + 1
                    310             CONTINUE
                ELSE
                    IK = IK + MB
                END IF
            320       CONTINUE
            330       CONTINUE
            JK = JK + NB
        340    CONTINUE
    END IF
    !
    RETURN
    !
    !     End of PSMATGEN
    !
END

!  ================================================================
!  This file contains the following LAPACK routines, for use by the
!  BLACS tester:  LSAME, SLAMCH, DLAMCH, DLARND, ZLARND, DLARAN,
!  and ZLARAN. If you have ScaLAPACK or LAPACK, all of these files
!  are present in your library, and you may discard this file and
!  point to the appropriate archive instead.
!  ================================================================

DOUBLE PRECISION FUNCTION DLAMCH(CMACH)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    CHARACTER          CMACH
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  DLAMCH determines double precision machine parameters.
    !
    !  Arguments
    !  =========
    !
    !  CMACH   (input) CHARACTER*1
    !          Specifies the value to be returned by DLAMCH:
    !          = 'E' or 'e',   DLAMCH := eps
    !          = 'S' or 's ,   DLAMCH := sfmin
    !          = 'B' or 'b',   DLAMCH := base
    !          = 'P' or 'p',   DLAMCH := eps*base
    !          = 'N' or 'n',   DLAMCH := t
    !          = 'R' or 'r',   DLAMCH := rnd
    !          = 'M' or 'm',   DLAMCH := emin
    !          = 'U' or 'u',   DLAMCH := rmin
    !          = 'L' or 'l',   DLAMCH := emax
    !          = 'O' or 'o',   DLAMCH := rmax
    !
    !          where
    !
    !          eps   = relative machine precision
    !          sfmin = safe minimum, such that 1/sfmin does not overflow
    !          base  = base of the machine
    !          prec  = eps*base
    !          t     = number of (base) digits in the mantissa
    !          rnd   = 1.0 when rounding occurs in addition, 0.0 otherwise
    !          emin  = minimum exponent before (gradual) underflow
    !          rmin  = underflow threshold - base**(emin-1)
    !          emax  = largest exponent before overflow
    !          rmax  = overflow threshold  - (base**emax)*(1-eps)
    !
    ! =====================================================================
    !
    !     .. Parameters ..
    DOUBLE PRECISION   ONE, ZERO
    PARAMETER          (ONE = 1.0D+0, ZERO = 0.0D+0)
    !     ..
    !     .. Local Scalars ..
    LOGICAL            FIRST, LRND
    INTEGER            BETA, IMAX, IMIN, IT
    DOUBLE PRECISION   BASE, EMAX, EMIN, EPS, PREC, RMACH, RMAX, RMIN, &
            RND, SFMIN, SMALL, T
    !     ..
    !     .. External Functions ..
    LOGICAL            LSAME
    EXTERNAL           LSAME
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           DLAMC2
    !     ..
    !     .. Save statement ..
    SAVE               FIRST, EPS, SFMIN, BASE, T, RND, EMIN, RMIN, &
            EMAX, RMAX, PREC
    !     ..
    !     .. Data statements ..
    DATA               FIRST / .TRUE. /
    !     ..
    !     .. Executable Statements ..
    !
    IF(FIRST) THEN
        FIRST = .FALSE.
        CALL DLAMC2(BETA, IT, LRND, EPS, IMIN, RMIN, IMAX, RMAX)
        BASE = BETA
        T = IT
        IF(LRND) THEN
            RND = ONE
            EPS = (BASE**(1 - IT)) / 2
        ELSE
            RND = ZERO
            EPS = BASE**(1 - IT)
        END IF
        PREC = EPS * BASE
        EMIN = IMIN
        EMAX = IMAX
        SFMIN = RMIN
        SMALL = ONE / RMAX
        IF(SMALL.GE.SFMIN) THEN
            !
            !           Use SMALL plus a bit, to avoid the possibility of rounding
            !           causing overflow when computing  1/sfmin.
            !
            SFMIN = SMALL * (ONE + EPS)
        END IF
    END IF
    !
    IF(LSAME(CMACH, 'E')) THEN
        RMACH = EPS
    ELSE IF(LSAME(CMACH, 'S')) THEN
        RMACH = SFMIN
    ELSE IF(LSAME(CMACH, 'B')) THEN
        RMACH = BASE
    ELSE IF(LSAME(CMACH, 'P')) THEN
        RMACH = PREC
    ELSE IF(LSAME(CMACH, 'N')) THEN
        RMACH = T
    ELSE IF(LSAME(CMACH, 'R')) THEN
        RMACH = RND
    ELSE IF(LSAME(CMACH, 'M')) THEN
        RMACH = EMIN
    ELSE IF(LSAME(CMACH, 'U')) THEN
        RMACH = RMIN
    ELSE IF(LSAME(CMACH, 'L')) THEN
        RMACH = EMAX
    ELSE IF(LSAME(CMACH, 'O')) THEN
        RMACH = RMAX
    END IF
    !
    DLAMCH = RMACH
    RETURN
    !
    !     End of DLAMCH
    !
END
!
!***********************************************************************
!
SUBROUTINE DLAMC1(BETA, T, RND, IEEE1)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    LOGICAL            IEEE1, RND
    INTEGER            BETA, T
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  DLAMC1 determines the machine parameters given by BETA, T, RND, and
    !  IEEE1.
    !
    !  Arguments
    !  =========
    !
    !  BETA    (output) INTEGER
    !          The base of the machine.
    !
    !  T       (output) INTEGER
    !          The number of ( BETA ) digits in the mantissa.
    !
    !  RND     (output) LOGICAL
    !          Specifies whether proper rounding  ( RND = .TRUE. )  or
    !          chopping  ( RND = .FALSE. )  occurs in addition. This may not
    !          be a reliable guide to the way in which the machine performs
    !          its arithmetic.
    !
    !  IEEE1   (output) LOGICAL
    !          Specifies whether rounding appears to be done in the IEEE
    !          'round to nearest' style.
    !
    !  Further Details
    !  ===============
    !
    !  The routine is based on the routine  ENVRON  by Malcolm and
    !  incorporates suggestions by Gentleman and Marovich. See
    !
    !     Malcolm M. A. (1972) Algorithms to reveal properties of
    !        floating-point arithmetic. Comms. of the ACM, 15, 949-951.
    !
    !     Gentleman W. M. and Marovich S. B. (1974) More on algorithms
    !        that reveal properties of floating point arithmetic units.
    !        Comms. of the ACM, 17, 276-277.
    !
    ! =====================================================================
    !
    !     .. Local Scalars ..
    LOGICAL            FIRST, LIEEE1, LRND
    INTEGER            LBETA, LT
    DOUBLE PRECISION   A, B, C, F, ONE, QTR, SAVEC, T1, T2
    !     ..
    !     .. External Functions ..
    DOUBLE PRECISION   DLAMC3
    EXTERNAL           DLAMC3
    !     ..
    !     .. Save statement ..
    SAVE               FIRST, LIEEE1, LBETA, LRND, LT
    !     ..
    !     .. Data statements ..
    DATA               FIRST / .TRUE. /
    !     ..
    !     .. Executable Statements ..
    !
    IF(FIRST) THEN
        FIRST = .FALSE.
        ONE = 1
        !
        !        LBETA,  LIEEE1,  LT and  LRND  are the  local values  of  BETA,
        !        IEEE1, T and RND.
        !
        !        Throughout this routine  we use the function  DLAMC3  to ensure
        !        that relevant values are  stored and not held in registers,  or
        !        are not affected by optimizers.
        !
        !        Compute  a = 2.0**m  with the  smallest positive integer m such
        !        that
        !
        !           fl( a + 1.0 ) = a.
        !
        A = 1
        C = 1
        !
        !+       WHILE( C.EQ.ONE )LOOP
        10    CONTINUE
        IF(C.EQ.ONE) THEN
            A = 2 * A
            C = DLAMC3(A, ONE)
            C = DLAMC3(C, -A)
            GO TO 10
        END IF
        !+       END WHILE
        !
        !        Now compute  b = 2.0**m  with the smallest positive integer m
        !        such that
        !
        !           fl( a + b ) .gt. a.
        !
        B = 1
        C = DLAMC3(A, B)
        !
        !+       WHILE( C.EQ.A )LOOP
        20    CONTINUE
        IF(C.EQ.A) THEN
            B = 2 * B
            C = DLAMC3(A, B)
            GO TO 20
        END IF
        !+       END WHILE
        !
        !        Now compute the base.  a and c  are neighbouring floating point
        !        numbers  in the  interval  ( beta**t, beta**( t + 1 ) )  and so
        !        their difference is beta. Adding 0.25 to c is to ensure that it
        !        is truncated to beta and not ( beta - 1 ).
        !
        QTR = ONE / 4
        SAVEC = C
        C = DLAMC3(C, -A)
        LBETA = C + QTR
        !
        !        Now determine whether rounding or chopping occurs,  by adding a
        !        bit  less  than  beta/2  and a  bit  more  than  beta/2  to  a.
        !
        B = LBETA
        F = DLAMC3(B / 2, -B / 100)
        C = DLAMC3(F, A)
        IF(C.EQ.A) THEN
            LRND = .TRUE.
        ELSE
            LRND = .FALSE.
        END IF
        F = DLAMC3(B / 2, B / 100)
        C = DLAMC3(F, A)
        IF((LRND) .AND. (C.EQ.A))&
                LRND = .FALSE.
        !
        !        Try and decide whether rounding is done in the  IEEE  'round to
        !        nearest' style. B/2 is half a unit in the last place of the two
        !        numbers A and SAVEC. Furthermore, A is even, i.e. has last  bit
        !        zero, and SAVEC is odd. Thus adding B/2 to A should not  change
        !        A, but adding B/2 to SAVEC should change SAVEC.
        !
        T1 = DLAMC3(B / 2, A)
        T2 = DLAMC3(B / 2, SAVEC)
        LIEEE1 = (T1.EQ.A) .AND. (T2.GT.SAVEC) .AND. LRND
        !
        !        Now find  the  mantissa, t.  It should  be the  integer part of
        !        log to the base beta of a,  however it is safer to determine  t
        !        by powering.  So we find t as the smallest positive integer for
        !        which
        !
        !           fl( beta**t + 1.0 ) = 1.0.
        !
        LT = 0
        A = 1
        C = 1
        !
        !+       WHILE( C.EQ.ONE )LOOP
        30    CONTINUE
        IF(C.EQ.ONE) THEN
            LT = LT + 1
            A = A * LBETA
            C = DLAMC3(A, ONE)
            C = DLAMC3(C, -A)
            GO TO 30
        END IF
        !+       END WHILE
        !
    END IF
    !
    BETA = LBETA
    T = LT
    RND = LRND
    IEEE1 = LIEEE1
    RETURN
    !
    !     End of DLAMC1
    !
END
!
!***********************************************************************
!
SUBROUTINE DLAMC2(BETA, T, RND, EPS, EMIN, RMIN, EMAX, RMAX)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    LOGICAL            RND
    INTEGER            BETA, EMAX, EMIN, T
    DOUBLE PRECISION   EPS, RMAX, RMIN
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  DLAMC2 determines the machine parameters specified in its argument
    !  list.
    !
    !  Arguments
    !  =========
    !
    !  BETA    (output) INTEGER
    !          The base of the machine.
    !
    !  T       (output) INTEGER
    !          The number of ( BETA ) digits in the mantissa.
    !
    !  RND     (output) LOGICAL
    !          Specifies whether proper rounding  ( RND = .TRUE. )  or
    !          chopping  ( RND = .FALSE. )  occurs in addition. This may not
    !          be a reliable guide to the way in which the machine performs
    !          its arithmetic.
    !
    !  EPS     (output) DOUBLE PRECISION
    !          The smallest positive number such that
    !
    !             fl( 1.0 - EPS ) .LT. 1.0,
    !
    !          where fl denotes the computed value.
    !
    !  EMIN    (output) INTEGER
    !          The minimum exponent before (gradual) underflow occurs.
    !
    !  RMIN    (output) DOUBLE PRECISION
    !          The smallest normalized number for the machine, given by
    !          BASE**( EMIN - 1 ), where  BASE  is the floating point value
    !          of BETA.
    !
    !  EMAX    (output) INTEGER
    !          The maximum exponent before overflow occurs.
    !
    !  RMAX    (output) DOUBLE PRECISION
    !          The largest positive number for the machine, given by
    !          BASE**EMAX * ( 1 - EPS ), where  BASE  is the floating point
    !          value of BETA.
    !
    !  Further Details
    !  ===============
    !
    !  The computation of  EPS  is based on a routine PARANOIA by
    !  W. Kahan of the University of California at Berkeley.
    !
    ! =====================================================================
    !
    !     .. Local Scalars ..
    LOGICAL            FIRST, IEEE, IWARN, LIEEE1, LRND
    INTEGER            GNMIN, GPMIN, I, LBETA, LEMAX, LEMIN, LT, &
            NGNMIN, NGPMIN
    DOUBLE PRECISION   A, B, C, HALF, LEPS, LRMAX, LRMIN, ONE, RBASE, &
            SIXTH, SMALL, THIRD, TWO, ZERO
    !     ..
    !     .. External Functions ..
    DOUBLE PRECISION   DLAMC3
    EXTERNAL           DLAMC3
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           DLAMC1, DLAMC4, DLAMC5
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC ABS, MAX, MIN
    !     ..
    !     .. Save statement ..
    SAVE               FIRST, IWARN, LBETA, LEMAX, LEMIN, LEPS, LRMAX, &
            LRMIN, LT
    !     ..
    !     .. Data statements ..
    DATA               FIRST / .TRUE. /, IWARN / .FALSE. /
    !     ..
    !     .. Executable Statements ..
    !
    IF(FIRST) THEN
        FIRST = .FALSE.
        ZERO = 0
        ONE = 1
        TWO = 2
        !
        !        LBETA, LT, LRND, LEPS, LEMIN and LRMIN  are the local values of
        !        BETA, T, RND, EPS, EMIN and RMIN.
        !
        !        Throughout this routine  we use the function  DLAMC3  to ensure
        !        that relevant values are stored  and not held in registers,  or
        !        are not affected by optimizers.
        !
        !        DLAMC1 returns the parameters  LBETA, LT, LRND and LIEEE1.
        !
        CALL DLAMC1(LBETA, LT, LRND, LIEEE1)
        !
        !        Start to find EPS.
        !
        B = LBETA
        A = B**(-LT)
        LEPS = A
        !
        !        Try some tricks to see whether or not this is the correct  EPS.
        !
        B = TWO / 3
        HALF = ONE / 2
        SIXTH = DLAMC3(B, -HALF)
        THIRD = DLAMC3(SIXTH, SIXTH)
        B = DLAMC3(THIRD, -HALF)
        B = DLAMC3(B, SIXTH)
        B = ABS(B)
        IF(B.LT.LEPS)&
                B = LEPS
        !
        LEPS = 1
        !
        !+       WHILE( ( LEPS.GT.B ).AND.( B.GT.ZERO ) )LOOP
        10    CONTINUE
        IF((LEPS.GT.B) .AND. (B.GT.ZERO)) THEN
            LEPS = B
            C = DLAMC3(HALF * LEPS, (TWO**5) * (LEPS**2))
            C = DLAMC3(HALF, -C)
            B = DLAMC3(HALF, C)
            C = DLAMC3(HALF, -B)
            B = DLAMC3(HALF, C)
            GO TO 10
        END IF
        !+       END WHILE
        !
        IF(A.LT.LEPS)&
                LEPS = A
        !
        !        Computation of EPS complete.
        !
        !        Now find  EMIN.  Let A = + or - 1, and + or - (1 + BASE**(-3)).
        !        Keep dividing  A by BETA until (gradual) underflow occurs. This
        !        is detected when we cannot recover the previous A.
        !
        RBASE = ONE / LBETA
        SMALL = ONE
        DO 20 I = 1, 3
            SMALL = DLAMC3(SMALL * RBASE, ZERO)
        20    CONTINUE
        A = DLAMC3(ONE, SMALL)
        CALL DLAMC4(NGPMIN, ONE, LBETA)
        CALL DLAMC4(NGNMIN, -ONE, LBETA)
        CALL DLAMC4(GPMIN, A, LBETA)
        CALL DLAMC4(GNMIN, -A, LBETA)
        IEEE = .FALSE.
        !
        IF((NGPMIN.EQ.NGNMIN) .AND. (GPMIN.EQ.GNMIN)) THEN
            IF(NGPMIN.EQ.GPMIN) THEN
                LEMIN = NGPMIN
                !            ( Non twos-complement machines, no gradual underflow;
                !              e.g.,  VAX )
            ELSE IF((GPMIN - NGPMIN).EQ.3) THEN
                LEMIN = NGPMIN - 1 + LT
                IEEE = .TRUE.
                !            ( Non twos-complement machines, with gradual underflow;
                !              e.g., IEEE standard followers )
            ELSE
                LEMIN = MIN(NGPMIN, GPMIN)
                !            ( A guess; no known machine )
                IWARN = .TRUE.
            END IF
            !
        ELSE IF((NGPMIN.EQ.GPMIN) .AND. (NGNMIN.EQ.GNMIN)) THEN
            IF(ABS(NGPMIN - NGNMIN).EQ.1) THEN
                LEMIN = MAX(NGPMIN, NGNMIN)
                !            ( Twos-complement machines, no gradual underflow;
                !              e.g., CYBER 205 )
            ELSE
                LEMIN = MIN(NGPMIN, NGNMIN)
                !            ( A guess; no known machine )
                IWARN = .TRUE.
            END IF
            !
        ELSE IF((ABS(NGPMIN - NGNMIN).EQ.1) .AND.&
                (GPMIN.EQ.GNMIN)) THEN
            IF((GPMIN - MIN(NGPMIN, NGNMIN)).EQ.3) THEN
                LEMIN = MAX(NGPMIN, NGNMIN) - 1 + LT
                !            ( Twos-complement machines with gradual underflow;
                !              no known machine )
            ELSE
                LEMIN = MIN(NGPMIN, NGNMIN)
                !            ( A guess; no known machine )
                IWARN = .TRUE.
            END IF
            !
        ELSE
            LEMIN = MIN(NGPMIN, NGNMIN, GPMIN, GNMIN)
            !         ( A guess; no known machine )
            IWARN = .TRUE.
        END IF
        !**
        ! Comment out this if block if EMIN is ok
        IF(IWARN) THEN
            FIRST = .TRUE.
            WRITE(6, FMT = 9999)LEMIN
        END IF
        !**
        !
        !        Assume IEEE arithmetic if we found denormalised  numbers above,
        !        or if arithmetic seems to round in the  IEEE style,  determined
        !        in routine DLAMC1. A true IEEE machine should have both  things
        !        true; however, faulty machines may have one or the other.
        !
        IEEE = IEEE .OR. LIEEE1
        !
        !        Compute  RMIN by successive division by  BETA. We could compute
        !        RMIN as BASE**( EMIN - 1 ),  but some machines underflow during
        !        this computation.
        !
        LRMIN = 1
        DO 30 I = 1, 1 - LEMIN
            LRMIN = DLAMC3(LRMIN * RBASE, ZERO)
        30    CONTINUE
        !
        !        Finally, call DLAMC5 to compute EMAX and RMAX.
        !
        CALL DLAMC5(LBETA, LT, LEMIN, IEEE, LEMAX, LRMAX)
    END IF
    !
    BETA = LBETA
    T = LT
    RND = LRND
    EPS = LEPS
    EMIN = LEMIN
    RMIN = LRMIN
    EMAX = LEMAX
    RMAX = LRMAX
    !
    RETURN
    !
    9999 FORMAT(/ / ' WARNING. The value EMIN may be incorrect:-', &
            '  EMIN = ', I8, /&
            ' If, after inspection, the value EMIN looks', &
            ' acceptable please comment out ', &
            / ' the IF block as marked within the code of routine', &
            ' DLAMC2,', / ' otherwise supply EMIN explicitly.', /)
    !
    !     End of DLAMC2
    !
END
!
!***********************************************************************
!
DOUBLE PRECISION FUNCTION DLAMC3(A, B)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    DOUBLE PRECISION   A, B
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  DLAMC3  is intended to force  A  and  B  to be stored prior to doing
    !  the addition of  A  and  B ,  for use in situations where optimizers
    !  might hold one of these in a register.
    !
    !  Arguments
    !  =========
    !
    !  A, B    (input) DOUBLE PRECISION
    !          The values A and B.
    !
    ! =====================================================================
    !
    !     .. Executable Statements ..
    !
    DLAMC3 = A + B
    !
    RETURN
    !
    !     End of DLAMC3
    !
END
!
!***********************************************************************
!
SUBROUTINE DLAMC4(EMIN, START, BASE)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    INTEGER            BASE, EMIN
    DOUBLE PRECISION   START
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  DLAMC4 is a service routine for DLAMC2.
    !
    !  Arguments
    !  =========
    !
    !  EMIN    (output) EMIN
    !          The minimum exponent before (gradual) underflow, computed by
    !          setting A = START and dividing by BASE until the previous A
    !          can not be recovered.
    !
    !  START   (input) DOUBLE PRECISION
    !          The starting point for determining EMIN.
    !
    !  BASE    (input) INTEGER
    !          The base of the machine.
    !
    ! =====================================================================
    !
    !     .. Local Scalars ..
    INTEGER            I
    DOUBLE PRECISION   A, B1, B2, C1, C2, D1, D2, ONE, RBASE, ZERO
    !     ..
    !     .. External Functions ..
    DOUBLE PRECISION   DLAMC3
    EXTERNAL           DLAMC3
    !     ..
    !     .. Executable Statements ..
    !
    A = START
    ONE = 1
    RBASE = ONE / BASE
    ZERO = 0
    EMIN = 1
    B1 = DLAMC3(A * RBASE, ZERO)
    C1 = A
    C2 = A
    D1 = A
    D2 = A
    !+    WHILE( ( C1.EQ.A ).AND.( C2.EQ.A ).AND.
    !    $       ( D1.EQ.A ).AND.( D2.EQ.A )      )LOOP
    10 CONTINUE
    IF((C1.EQ.A) .AND. (C2.EQ.A) .AND. (D1.EQ.A) .AND.&
            (D2.EQ.A)) THEN
        EMIN = EMIN - 1
        A = B1
        B1 = DLAMC3(A / BASE, ZERO)
        C1 = DLAMC3(B1 * BASE, ZERO)
        D1 = ZERO
        DO 20 I = 1, BASE
            D1 = D1 + B1
        20    CONTINUE
        B2 = DLAMC3(A * RBASE, ZERO)
        C2 = DLAMC3(B2 / RBASE, ZERO)
        D2 = ZERO
        DO 30 I = 1, BASE
            D2 = D2 + B2
        30    CONTINUE
        GO TO 10
    END IF
    !+    END WHILE
    !
    RETURN
    !
    !     End of DLAMC4
    !
END
!
!***********************************************************************
!
SUBROUTINE DLAMC5(BETA, P, EMIN, IEEE, EMAX, RMAX)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    LOGICAL            IEEE
    INTEGER            BETA, EMAX, EMIN, P
    DOUBLE PRECISION   RMAX
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  DLAMC5 attempts to compute RMAX, the largest machine floating-point
    !  number, without overflow.  It assumes that EMAX + abs(EMIN) sum
    !  approximately to a power of 2.  It will fail on machines where this
    !  assumption does not hold, for example, the Cyber 205 (EMIN = -28625,
    !  EMAX = 28718).  It will also fail if the value supplied for EMIN is
    !  too large (i.e. too close to zero), probably with overflow.
    !
    !  Arguments
    !  =========
    !
    !  BETA    (input) INTEGER
    !          The base of floating-point arithmetic.
    !
    !  P       (input) INTEGER
    !          The number of base BETA digits in the mantissa of a
    !          floating-point value.
    !
    !  EMIN    (input) INTEGER
    !          The minimum exponent before (gradual) underflow.
    !
    !  IEEE    (input) LOGICAL
    !          A logical flag specifying whether or not the arithmetic
    !          system is thought to comply with the IEEE standard.
    !
    !  EMAX    (output) INTEGER
    !          The largest exponent before overflow
    !
    !  RMAX    (output) DOUBLE PRECISION
    !          The largest machine floating-point number.
    !
    ! =====================================================================
    !
    !     .. Parameters ..
    DOUBLE PRECISION   ZERO, ONE
    PARAMETER          (ZERO = 0.0D0, ONE = 1.0D0)
    !     ..
    !     .. Local Scalars ..
    INTEGER            EXBITS, EXPSUM, I, LEXP, NBITS, TRY, UEXP
    DOUBLE PRECISION   OLDY, RECBAS, Y, Z
    !     ..
    !     .. External Functions ..
    DOUBLE PRECISION   DLAMC3
    EXTERNAL           DLAMC3
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC MOD
    !     ..
    !     .. Executable Statements ..
    !
    !     First compute LEXP and UEXP, two powers of 2 that bound
    !     abs(EMIN). We then assume that EMAX + abs(EMIN) will sum
    !     approximately to the bound that is closest to abs(EMIN).
    !     (EMAX is the exponent of the required number RMAX).
    !
    LEXP = 1
    EXBITS = 1
    10 CONTINUE
    TRY = LEXP * 2
    IF(TRY.LE.(-EMIN)) THEN
        LEXP = TRY
        EXBITS = EXBITS + 1
        GO TO 10
    END IF
    IF(LEXP.EQ.-EMIN) THEN
        UEXP = LEXP
    ELSE
        UEXP = TRY
        EXBITS = EXBITS + 1
    END IF
    !
    !     Now -LEXP is less than or equal to EMIN, and -UEXP is greater
    !     than or equal to EMIN. EXBITS is the number of bits needed to
    !     store the exponent.
    !
    IF((UEXP + EMIN).GT.(-LEXP - EMIN)) THEN
        EXPSUM = 2 * LEXP
    ELSE
        EXPSUM = 2 * UEXP
    END IF
    !
    !     EXPSUM is the exponent range, approximately equal to
    !     EMAX - EMIN + 1 .
    !
    EMAX = EXPSUM + EMIN - 1
    NBITS = 1 + EXBITS + P
    !
    !     NBITS is the total number of bits needed to store a
    !     floating-point number.
    !
    IF((MOD(NBITS, 2).EQ.1) .AND. (BETA.EQ.2)) THEN
        !
        !        Either there are an odd number of bits used to store a
        !        floating-point number, which is unlikely, or some bits are
        !        not used in the representation of numbers, which is possible,
        !        (e.g. Cray machines) or the mantissa has an implicit bit,
        !        (e.g. IEEE machines, Dec Vax machines), which is perhaps the
        !        most likely. We have to assume the last alternative.
        !        If this is true, then we need to reduce EMAX by one because
        !        there must be some way of representing zero in an implicit-bit
        !        system. On machines like Cray, we are reducing EMAX by one
        !        unnecessarily.
        !
        EMAX = EMAX - 1
    END IF
    !
    IF(IEEE) THEN
        !
        !        Assume we are on an IEEE machine which reserves one exponent
        !        for infinity and NaN.
        !
        EMAX = EMAX - 1
    END IF
    !
    !     Now create RMAX, the largest machine number, which should
    !     be equal to (1.0 - BETA**(-P)) * BETA**EMAX .
    !
    !     First compute 1.0 - BETA**(-P), being careful that the
    !     result is less than 1.0 .
    !
    RECBAS = ONE / BETA
    Z = BETA - ONE
    Y = ZERO
    DO 20 I = 1, P
        Z = Z * RECBAS
        IF(Y.LT.ONE)&
                OLDY = Y
        Y = DLAMC3(Y, Z)
    20 CONTINUE
    IF(Y.GE.ONE)&
            Y = OLDY
    !
    !     Now multiply by BETA**EMAX to get RMAX.
    !
    DO 30 I = 1, EMAX
        Y = DLAMC3(Y * BETA, ZERO)
    30 CONTINUE
    !
    RMAX = Y
    RETURN
    !
    !     End of DLAMC5
    !
END
REAL             FUNCTION SLAMCH(CMACH)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    CHARACTER          CMACH
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  SLAMCH determines single precision machine parameters.
    !
    !  Arguments
    !  =========
    !
    !  CMACH   (input) CHARACTER*1
    !          Specifies the value to be returned by SLAMCH:
    !          = 'E' or 'e',   SLAMCH := eps
    !          = 'S' or 's ,   SLAMCH := sfmin
    !          = 'B' or 'b',   SLAMCH := base
    !          = 'P' or 'p',   SLAMCH := eps*base
    !          = 'N' or 'n',   SLAMCH := t
    !          = 'R' or 'r',   SLAMCH := rnd
    !          = 'M' or 'm',   SLAMCH := emin
    !          = 'U' or 'u',   SLAMCH := rmin
    !          = 'L' or 'l',   SLAMCH := emax
    !          = 'O' or 'o',   SLAMCH := rmax
    !
    !          where
    !
    !          eps   = relative machine precision
    !          sfmin = safe minimum, such that 1/sfmin does not overflow
    !          base  = base of the machine
    !          prec  = eps*base
    !          t     = number of (base) digits in the mantissa
    !          rnd   = 1.0 when rounding occurs in addition, 0.0 otherwise
    !          emin  = minimum exponent before (gradual) underflow
    !          rmin  = underflow threshold - base**(emin-1)
    !          emax  = largest exponent before overflow
    !          rmax  = overflow threshold  - (base**emax)*(1-eps)
    !
    ! =====================================================================
    !
    !     .. Parameters ..
    REAL               ONE, ZERO
    PARAMETER          (ONE = 1.0E+0, ZERO = 0.0E+0)
    !     ..
    !     .. Local Scalars ..
    LOGICAL            FIRST, LRND
    INTEGER            BETA, IMAX, IMIN, IT
    REAL               BASE, EMAX, EMIN, EPS, PREC, RMACH, RMAX, RMIN, &
            RND, SFMIN, SMALL, T
    !     ..
    !     .. External Functions ..
    LOGICAL            LSAME
    EXTERNAL           LSAME
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           SLAMC2
    !     ..
    !     .. Save statement ..
    SAVE               FIRST, EPS, SFMIN, BASE, T, RND, EMIN, RMIN, &
            EMAX, RMAX, PREC
    !     ..
    !     .. Data statements ..
    DATA               FIRST / .TRUE. /
    !     ..
    !     .. Executable Statements ..
    !
    IF(FIRST) THEN
        FIRST = .FALSE.
        CALL SLAMC2(BETA, IT, LRND, EPS, IMIN, RMIN, IMAX, RMAX)
        BASE = BETA
        T = IT
        IF(LRND) THEN
            RND = ONE
            EPS = (BASE**(1 - IT)) / 2
        ELSE
            RND = ZERO
            EPS = BASE**(1 - IT)
        END IF
        PREC = EPS * BASE
        EMIN = IMIN
        EMAX = IMAX
        SFMIN = RMIN
        SMALL = ONE / RMAX
        IF(SMALL.GE.SFMIN) THEN
            !
            !           Use SMALL plus a bit, to avoid the possibility of rounding
            !           causing overflow when computing  1/sfmin.
            !
            SFMIN = SMALL * (ONE + EPS)
        END IF
    END IF
    !
    IF(LSAME(CMACH, 'E')) THEN
        RMACH = EPS
    ELSE IF(LSAME(CMACH, 'S')) THEN
        RMACH = SFMIN
    ELSE IF(LSAME(CMACH, 'B')) THEN
        RMACH = BASE
    ELSE IF(LSAME(CMACH, 'P')) THEN
        RMACH = PREC
    ELSE IF(LSAME(CMACH, 'N')) THEN
        RMACH = T
    ELSE IF(LSAME(CMACH, 'R')) THEN
        RMACH = RND
    ELSE IF(LSAME(CMACH, 'M')) THEN
        RMACH = EMIN
    ELSE IF(LSAME(CMACH, 'U')) THEN
        RMACH = RMIN
    ELSE IF(LSAME(CMACH, 'L')) THEN
        RMACH = EMAX
    ELSE IF(LSAME(CMACH, 'O')) THEN
        RMACH = RMAX
    END IF
    !
    SLAMCH = RMACH
    RETURN
    !
    !     End of SLAMCH
    !
END
!
!***********************************************************************
!
SUBROUTINE SLAMC1(BETA, T, RND, IEEE1)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    LOGICAL            IEEE1, RND
    INTEGER            BETA, T
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  SLAMC1 determines the machine parameters given by BETA, T, RND, and
    !  IEEE1.
    !
    !  Arguments
    !  =========
    !
    !  BETA    (output) INTEGER
    !          The base of the machine.
    !
    !  T       (output) INTEGER
    !          The number of ( BETA ) digits in the mantissa.
    !
    !  RND     (output) LOGICAL
    !          Specifies whether proper rounding  ( RND = .TRUE. )  or
    !          chopping  ( RND = .FALSE. )  occurs in addition. This may not
    !          be a reliable guide to the way in which the machine performs
    !          its arithmetic.
    !
    !  IEEE1   (output) LOGICAL
    !          Specifies whether rounding appears to be done in the IEEE
    !          'round to nearest' style.
    !
    !  Further Details
    !  ===============
    !
    !  The routine is based on the routine  ENVRON  by Malcolm and
    !  incorporates suggestions by Gentleman and Marovich. See
    !
    !     Malcolm M. A. (1972) Algorithms to reveal properties of
    !        floating-point arithmetic. Comms. of the ACM, 15, 949-951.
    !
    !     Gentleman W. M. and Marovich S. B. (1974) More on algorithms
    !        that reveal properties of floating point arithmetic units.
    !        Comms. of the ACM, 17, 276-277.
    !
    ! =====================================================================
    !
    !     .. Local Scalars ..
    LOGICAL            FIRST, LIEEE1, LRND
    INTEGER            LBETA, LT
    REAL               A, B, C, F, ONE, QTR, SAVEC, T1, T2
    !     ..
    !     .. External Functions ..
    REAL               SLAMC3
    EXTERNAL           SLAMC3
    !     ..
    !     .. Save statement ..
    SAVE               FIRST, LIEEE1, LBETA, LRND, LT
    !     ..
    !     .. Data statements ..
    DATA               FIRST / .TRUE. /
    !     ..
    !     .. Executable Statements ..
    !
    IF(FIRST) THEN
        FIRST = .FALSE.
        ONE = 1
        !
        !        LBETA,  LIEEE1,  LT and  LRND  are the  local values  of  BETA,
        !        IEEE1, T and RND.
        !
        !        Throughout this routine  we use the function  SLAMC3  to ensure
        !        that relevant values are  stored and not held in registers,  or
        !        are not affected by optimizers.
        !
        !        Compute  a = 2.0**m  with the  smallest positive integer m such
        !        that
        !
        !           fl( a + 1.0 ) = a.
        !
        A = 1
        C = 1
        !
        !+       WHILE( C.EQ.ONE )LOOP
        10    CONTINUE
        IF(C.EQ.ONE) THEN
            A = 2 * A
            C = SLAMC3(A, ONE)
            C = SLAMC3(C, -A)
            GO TO 10
        END IF
        !+       END WHILE
        !
        !        Now compute  b = 2.0**m  with the smallest positive integer m
        !        such that
        !
        !           fl( a + b ) .gt. a.
        !
        B = 1
        C = SLAMC3(A, B)
        !
        !+       WHILE( C.EQ.A )LOOP
        20    CONTINUE
        IF(C.EQ.A) THEN
            B = 2 * B
            C = SLAMC3(A, B)
            GO TO 20
        END IF
        !+       END WHILE
        !
        !        Now compute the base.  a and c  are neighbouring floating point
        !        numbers  in the  interval  ( beta**t, beta**( t + 1 ) )  and so
        !        their difference is beta. Adding 0.25 to c is to ensure that it
        !        is truncated to beta and not ( beta - 1 ).
        !
        QTR = ONE / 4
        SAVEC = C
        C = SLAMC3(C, -A)
        LBETA = C + QTR
        !
        !        Now determine whether rounding or chopping occurs,  by adding a
        !        bit  less  than  beta/2  and a  bit  more  than  beta/2  to  a.
        !
        B = LBETA
        F = SLAMC3(B / 2, -B / 100)
        C = SLAMC3(F, A)
        IF(C.EQ.A) THEN
            LRND = .TRUE.
        ELSE
            LRND = .FALSE.
        END IF
        F = SLAMC3(B / 2, B / 100)
        C = SLAMC3(F, A)
        IF((LRND) .AND. (C.EQ.A))&
                LRND = .FALSE.
        !
        !        Try and decide whether rounding is done in the  IEEE  'round to
        !        nearest' style. B/2 is half a unit in the last place of the two
        !        numbers A and SAVEC. Furthermore, A is even, i.e. has last  bit
        !        zero, and SAVEC is odd. Thus adding B/2 to A should not  change
        !        A, but adding B/2 to SAVEC should change SAVEC.
        !
        T1 = SLAMC3(B / 2, A)
        T2 = SLAMC3(B / 2, SAVEC)
        LIEEE1 = (T1.EQ.A) .AND. (T2.GT.SAVEC) .AND. LRND
        !
        !        Now find  the  mantissa, t.  It should  be the  integer part of
        !        log to the base beta of a,  however it is safer to determine  t
        !        by powering.  So we find t as the smallest positive integer for
        !        which
        !
        !           fl( beta**t + 1.0 ) = 1.0.
        !
        LT = 0
        A = 1
        C = 1
        !
        !+       WHILE( C.EQ.ONE )LOOP
        30    CONTINUE
        IF(C.EQ.ONE) THEN
            LT = LT + 1
            A = A * LBETA
            C = SLAMC3(A, ONE)
            C = SLAMC3(C, -A)
            GO TO 30
        END IF
        !+       END WHILE
        !
    END IF
    !
    BETA = LBETA
    T = LT
    RND = LRND
    IEEE1 = LIEEE1
    RETURN
    !
    !     End of SLAMC1
    !
END
!
!***********************************************************************
!
SUBROUTINE SLAMC2(BETA, T, RND, EPS, EMIN, RMIN, EMAX, RMAX)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    LOGICAL            RND
    INTEGER            BETA, EMAX, EMIN, T
    REAL               EPS, RMAX, RMIN
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  SLAMC2 determines the machine parameters specified in its argument
    !  list.
    !
    !  Arguments
    !  =========
    !
    !  BETA    (output) INTEGER
    !          The base of the machine.
    !
    !  T       (output) INTEGER
    !          The number of ( BETA ) digits in the mantissa.
    !
    !  RND     (output) LOGICAL
    !          Specifies whether proper rounding  ( RND = .TRUE. )  or
    !          chopping  ( RND = .FALSE. )  occurs in addition. This may not
    !          be a reliable guide to the way in which the machine performs
    !          its arithmetic.
    !
    !  EPS     (output) REAL
    !          The smallest positive number such that
    !
    !             fl( 1.0 - EPS ) .LT. 1.0,
    !
    !          where fl denotes the computed value.
    !
    !  EMIN    (output) INTEGER
    !          The minimum exponent before (gradual) underflow occurs.
    !
    !  RMIN    (output) REAL
    !          The smallest normalized number for the machine, given by
    !          BASE**( EMIN - 1 ), where  BASE  is the floating point value
    !          of BETA.
    !
    !  EMAX    (output) INTEGER
    !          The maximum exponent before overflow occurs.
    !
    !  RMAX    (output) REAL
    !          The largest positive number for the machine, given by
    !          BASE**EMAX * ( 1 - EPS ), where  BASE  is the floating point
    !          value of BETA.
    !
    !  Further Details
    !  ===============
    !
    !  The computation of  EPS  is based on a routine PARANOIA by
    !  W. Kahan of the University of California at Berkeley.
    !
    ! =====================================================================
    !
    !     .. Local Scalars ..
    LOGICAL            FIRST, IEEE, IWARN, LIEEE1, LRND
    INTEGER            GNMIN, GPMIN, I, LBETA, LEMAX, LEMIN, LT, &
            NGNMIN, NGPMIN
    REAL               A, B, C, HALF, LEPS, LRMAX, LRMIN, ONE, RBASE, &
            SIXTH, SMALL, THIRD, TWO, ZERO
    !     ..
    !     .. External Functions ..
    REAL               SLAMC3
    EXTERNAL           SLAMC3
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           SLAMC1, SLAMC4, SLAMC5
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC ABS, MAX, MIN
    !     ..
    !     .. Save statement ..
    SAVE               FIRST, IWARN, LBETA, LEMAX, LEMIN, LEPS, LRMAX, &
            LRMIN, LT
    !     ..
    !     .. Data statements ..
    DATA               FIRST / .TRUE. /, IWARN / .FALSE. /
    !     ..
    !     .. Executable Statements ..
    !
    IF(FIRST) THEN
        FIRST = .FALSE.
        ZERO = 0
        ONE = 1
        TWO = 2
        !
        !        LBETA, LT, LRND, LEPS, LEMIN and LRMIN  are the local values of
        !        BETA, T, RND, EPS, EMIN and RMIN.
        !
        !        Throughout this routine  we use the function  SLAMC3  to ensure
        !        that relevant values are stored  and not held in registers,  or
        !        are not affected by optimizers.
        !
        !        SLAMC1 returns the parameters  LBETA, LT, LRND and LIEEE1.
        !
        CALL SLAMC1(LBETA, LT, LRND, LIEEE1)
        !
        !        Start to find EPS.
        !
        B = LBETA
        A = B**(-LT)
        LEPS = A
        !
        !        Try some tricks to see whether or not this is the correct  EPS.
        !
        B = TWO / 3
        HALF = ONE / 2
        SIXTH = SLAMC3(B, -HALF)
        THIRD = SLAMC3(SIXTH, SIXTH)
        B = SLAMC3(THIRD, -HALF)
        B = SLAMC3(B, SIXTH)
        B = ABS(B)
        IF(B.LT.LEPS)&
                B = LEPS
        !
        LEPS = 1
        !
        !+       WHILE( ( LEPS.GT.B ).AND.( B.GT.ZERO ) )LOOP
        10    CONTINUE
        IF((LEPS.GT.B) .AND. (B.GT.ZERO)) THEN
            LEPS = B
            C = SLAMC3(HALF * LEPS, (TWO**5) * (LEPS**2))
            C = SLAMC3(HALF, -C)
            B = SLAMC3(HALF, C)
            C = SLAMC3(HALF, -B)
            B = SLAMC3(HALF, C)
            GO TO 10
        END IF
        !+       END WHILE
        !
        IF(A.LT.LEPS)&
                LEPS = A
        !
        !        Computation of EPS complete.
        !
        !        Now find  EMIN.  Let A = + or - 1, and + or - (1 + BASE**(-3)).
        !        Keep dividing  A by BETA until (gradual) underflow occurs. This
        !        is detected when we cannot recover the previous A.
        !
        RBASE = ONE / LBETA
        SMALL = ONE
        DO 20 I = 1, 3
            SMALL = SLAMC3(SMALL * RBASE, ZERO)
        20    CONTINUE
        A = SLAMC3(ONE, SMALL)
        CALL SLAMC4(NGPMIN, ONE, LBETA)
        CALL SLAMC4(NGNMIN, -ONE, LBETA)
        CALL SLAMC4(GPMIN, A, LBETA)
        CALL SLAMC4(GNMIN, -A, LBETA)
        IEEE = .FALSE.
        !
        IF((NGPMIN.EQ.NGNMIN) .AND. (GPMIN.EQ.GNMIN)) THEN
            IF(NGPMIN.EQ.GPMIN) THEN
                LEMIN = NGPMIN
                !            ( Non twos-complement machines, no gradual underflow;
                !              e.g.,  VAX )
            ELSE IF((GPMIN - NGPMIN).EQ.3) THEN
                LEMIN = NGPMIN - 1 + LT
                IEEE = .TRUE.
                !            ( Non twos-complement machines, with gradual underflow;
                !              e.g., IEEE standard followers )
            ELSE
                LEMIN = MIN(NGPMIN, GPMIN)
                !            ( A guess; no known machine )
                IWARN = .TRUE.
            END IF
            !
        ELSE IF((NGPMIN.EQ.GPMIN) .AND. (NGNMIN.EQ.GNMIN)) THEN
            IF(ABS(NGPMIN - NGNMIN).EQ.1) THEN
                LEMIN = MAX(NGPMIN, NGNMIN)
                !            ( Twos-complement machines, no gradual underflow;
                !              e.g., CYBER 205 )
            ELSE
                LEMIN = MIN(NGPMIN, NGNMIN)
                !            ( A guess; no known machine )
                IWARN = .TRUE.
            END IF
            !
        ELSE IF((ABS(NGPMIN - NGNMIN).EQ.1) .AND.&
                (GPMIN.EQ.GNMIN)) THEN
            IF((GPMIN - MIN(NGPMIN, NGNMIN)).EQ.3) THEN
                LEMIN = MAX(NGPMIN, NGNMIN) - 1 + LT
                !            ( Twos-complement machines with gradual underflow;
                !              no known machine )
            ELSE
                LEMIN = MIN(NGPMIN, NGNMIN)
                !            ( A guess; no known machine )
                IWARN = .TRUE.
            END IF
            !
        ELSE
            LEMIN = MIN(NGPMIN, NGNMIN, GPMIN, GNMIN)
            !         ( A guess; no known machine )
            IWARN = .TRUE.
        END IF
        !**
        ! Comment out this if block if EMIN is ok
        IF(IWARN) THEN
            FIRST = .TRUE.
            WRITE(6, FMT = 9999)LEMIN
        END IF
        !**
        !
        !        Assume IEEE arithmetic if we found denormalised  numbers above,
        !        or if arithmetic seems to round in the  IEEE style,  determined
        !        in routine SLAMC1. A true IEEE machine should have both  things
        !        true; however, faulty machines may have one or the other.
        !
        IEEE = IEEE .OR. LIEEE1
        !
        !        Compute  RMIN by successive division by  BETA. We could compute
        !        RMIN as BASE**( EMIN - 1 ),  but some machines underflow during
        !        this computation.
        !
        LRMIN = 1
        DO 30 I = 1, 1 - LEMIN
            LRMIN = SLAMC3(LRMIN * RBASE, ZERO)
        30    CONTINUE
        !
        !        Finally, call SLAMC5 to compute EMAX and RMAX.
        !
        CALL SLAMC5(LBETA, LT, LEMIN, IEEE, LEMAX, LRMAX)
    END IF
    !
    BETA = LBETA
    T = LT
    RND = LRND
    EPS = LEPS
    EMIN = LEMIN
    RMIN = LRMIN
    EMAX = LEMAX
    RMAX = LRMAX
    !
    RETURN
    !
    9999 FORMAT(/ / ' WARNING. The value EMIN may be incorrect:-', &
            '  EMIN = ', I8, /&
            ' If, after inspection, the value EMIN looks', &
            ' acceptable please comment out ', &
            / ' the IF block as marked within the code of routine', &
            ' SLAMC2,', / ' otherwise supply EMIN explicitly.', /)
    !
    !     End of SLAMC2
    !
END
!
!***********************************************************************
!
REAL             FUNCTION SLAMC3(A, B)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    REAL               A, B
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  SLAMC3  is intended to force  A  and  B  to be stored prior to doing
    !  the addition of  A  and  B ,  for use in situations where optimizers
    !  might hold one of these in a register.
    !
    !  Arguments
    !  =========
    !
    !  A, B    (input) REAL
    !          The values A and B.
    !
    ! =====================================================================
    !
    !     .. Executable Statements ..
    !
    SLAMC3 = A + B
    !
    RETURN
    !
    !     End of SLAMC3
    !
END
!
!***********************************************************************
!
SUBROUTINE SLAMC4(EMIN, START, BASE)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    INTEGER            BASE, EMIN
    REAL               START
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  SLAMC4 is a service routine for SLAMC2.
    !
    !  Arguments
    !  =========
    !
    !  EMIN    (output) EMIN
    !          The minimum exponent before (gradual) underflow, computed by
    !          setting A = START and dividing by BASE until the previous A
    !          can not be recovered.
    !
    !  START   (input) REAL
    !          The starting point for determining EMIN.
    !
    !  BASE    (input) INTEGER
    !          The base of the machine.
    !
    ! =====================================================================
    !
    !     .. Local Scalars ..
    INTEGER            I
    REAL               A, B1, B2, C1, C2, D1, D2, ONE, RBASE, ZERO
    !     ..
    !     .. External Functions ..
    REAL               SLAMC3
    EXTERNAL           SLAMC3
    !     ..
    !     .. Executable Statements ..
    !
    A = START
    ONE = 1
    RBASE = ONE / BASE
    ZERO = 0
    EMIN = 1
    B1 = SLAMC3(A * RBASE, ZERO)
    C1 = A
    C2 = A
    D1 = A
    D2 = A
    !+    WHILE( ( C1.EQ.A ).AND.( C2.EQ.A ).AND.
    !    $       ( D1.EQ.A ).AND.( D2.EQ.A )      )LOOP
    10 CONTINUE
    IF((C1.EQ.A) .AND. (C2.EQ.A) .AND. (D1.EQ.A) .AND.&
            (D2.EQ.A)) THEN
        EMIN = EMIN - 1
        A = B1
        B1 = SLAMC3(A / BASE, ZERO)
        C1 = SLAMC3(B1 * BASE, ZERO)
        D1 = ZERO
        DO 20 I = 1, BASE
            D1 = D1 + B1
        20    CONTINUE
        B2 = SLAMC3(A * RBASE, ZERO)
        C2 = SLAMC3(B2 / RBASE, ZERO)
        D2 = ZERO
        DO 30 I = 1, BASE
            D2 = D2 + B2
        30    CONTINUE
        GO TO 10
    END IF
    !+    END WHILE
    !
    RETURN
    !
    !     End of SLAMC4
    !
END
!
!***********************************************************************
!
SUBROUTINE SLAMC5(BETA, P, EMIN, IEEE, EMAX, RMAX)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     October 31, 1992
    !
    !     .. Scalar Arguments ..
    LOGICAL            IEEE
    INTEGER            BETA, EMAX, EMIN, P
    REAL               RMAX
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  SLAMC5 attempts to compute RMAX, the largest machine floating-point
    !  number, without overflow.  It assumes that EMAX + abs(EMIN) sum
    !  approximately to a power of 2.  It will fail on machines where this
    !  assumption does not hold, for example, the Cyber 205 (EMIN = -28625,
    !  EMAX = 28718).  It will also fail if the value supplied for EMIN is
    !  too large (i.e. too close to zero), probably with overflow.
    !
    !  Arguments
    !  =========
    !
    !  BETA    (input) INTEGER
    !          The base of floating-point arithmetic.
    !
    !  P       (input) INTEGER
    !          The number of base BETA digits in the mantissa of a
    !          floating-point value.
    !
    !  EMIN    (input) INTEGER
    !          The minimum exponent before (gradual) underflow.
    !
    !  IEEE    (input) LOGICAL
    !          A logical flag specifying whether or not the arithmetic
    !          system is thought to comply with the IEEE standard.
    !
    !  EMAX    (output) INTEGER
    !          The largest exponent before overflow
    !
    !  RMAX    (output) REAL
    !          The largest machine floating-point number.
    !
    ! =====================================================================
    !
    !     .. Parameters ..
    REAL               ZERO, ONE
    PARAMETER          (ZERO = 0.0E0, ONE = 1.0E0)
    !     ..
    !     .. Local Scalars ..
    INTEGER            EXBITS, EXPSUM, I, LEXP, NBITS, TRY, UEXP
    REAL               OLDY, RECBAS, Y, Z
    !     ..
    !     .. External Functions ..
    REAL               SLAMC3
    EXTERNAL           SLAMC3
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC MOD
    !     ..
    !     .. Executable Statements ..
    !
    !     First compute LEXP and UEXP, two powers of 2 that bound
    !     abs(EMIN). We then assume that EMAX + abs(EMIN) will sum
    !     approximately to the bound that is closest to abs(EMIN).
    !     (EMAX is the exponent of the required number RMAX).
    !
    LEXP = 1
    EXBITS = 1
    10 CONTINUE
    TRY = LEXP * 2
    IF(TRY.LE.(-EMIN)) THEN
        LEXP = TRY
        EXBITS = EXBITS + 1
        GO TO 10
    END IF
    IF(LEXP.EQ.-EMIN) THEN
        UEXP = LEXP
    ELSE
        UEXP = TRY
        EXBITS = EXBITS + 1
    END IF
    !
    !     Now -LEXP is less than or equal to EMIN, and -UEXP is greater
    !     than or equal to EMIN. EXBITS is the number of bits needed to
    !     store the exponent.
    !
    IF((UEXP + EMIN).GT.(-LEXP - EMIN)) THEN
        EXPSUM = 2 * LEXP
    ELSE
        EXPSUM = 2 * UEXP
    END IF
    !
    !     EXPSUM is the exponent range, approximately equal to
    !     EMAX - EMIN + 1 .
    !
    EMAX = EXPSUM + EMIN - 1
    NBITS = 1 + EXBITS + P
    !
    !     NBITS is the total number of bits needed to store a
    !     floating-point number.
    !
    IF((MOD(NBITS, 2).EQ.1) .AND. (BETA.EQ.2)) THEN
        !
        !        Either there are an odd number of bits used to store a
        !        floating-point number, which is unlikely, or some bits are
        !        not used in the representation of numbers, which is possible,
        !        (e.g. Cray machines) or the mantissa has an implicit bit,
        !        (e.g. IEEE machines, Dec Vax machines), which is perhaps the
        !        most likely. We have to assume the last alternative.
        !        If this is true, then we need to reduce EMAX by one because
        !        there must be some way of representing zero in an implicit-bit
        !        system. On machines like Cray, we are reducing EMAX by one
        !        unnecessarily.
        !
        EMAX = EMAX - 1
    END IF
    !
    IF(IEEE) THEN
        !
        !        Assume we are on an IEEE machine which reserves one exponent
        !        for infinity and NaN.
        !
        EMAX = EMAX - 1
    END IF
    !
    !     Now create RMAX, the largest machine number, which should
    !     be equal to (1.0 - BETA**(-P)) * BETA**EMAX .
    !
    !     First compute 1.0 - BETA**(-P), being careful that the
    !     result is less than 1.0 .
    !
    RECBAS = ONE / BETA
    Z = BETA - ONE
    Y = ZERO
    DO 20 I = 1, P
        Z = Z * RECBAS
        IF(Y.LT.ONE)&
                OLDY = Y
        Y = SLAMC3(Y, Z)
    20 CONTINUE
    IF(Y.GE.ONE)&
            Y = OLDY
    !
    !     Now multiply by BETA**EMAX to get RMAX.
    !
    DO 30 I = 1, EMAX
        Y = SLAMC3(Y * BETA, ZERO)
    30 CONTINUE
    !
    RMAX = Y
    RETURN
    !
    !     End of SLAMC5
    !
END
LOGICAL          FUNCTION LSAME(CA, CB)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     June 30, 1994
    !
    !     .. Scalar Arguments ..
    CHARACTER          CA, CB
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  LSAME returns .TRUE. if CA is the same letter as CB regardless of
    !  case.
    !
    !  Arguments
    !  =========
    !
    !  CA      (input) CHARACTER*1
    !  CB      (input) CHARACTER*1
    !          CA and CB specify the single characters to be compared.
    !
    ! =====================================================================
    !
    !     .. Intrinsic Functions ..
    INTRINSIC ICHAR
    !     ..
    !     .. Local Scalars ..
    INTEGER            INTA, INTB, ZCODE
    !     ..
    !     .. Executable Statements ..
    !
    !     Test if the characters are equal
    !
    LSAME = CA.EQ.CB
    IF(LSAME)&
            RETURN
    !
    !     Now test for equivalence if both characters are alphabetic.
    !
    ZCODE = ICHAR('Z')
    !
    !     Use 'Z' rather than 'A' so that ASCII can be detected on Prime
    !     machines, on which ICHAR returns a value with bit 8 set.
    !     ICHAR('A') on Prime machines returns 193 which is the same as
    !     ICHAR('A') on an EBCDIC machine.
    !
    INTA = ICHAR(CA)
    INTB = ICHAR(CB)
    !
    IF(ZCODE.EQ.90 .OR. ZCODE.EQ.122) THEN
        !
        !        ASCII is assumed - ZCODE is the ASCII code of either lower or
        !        upper case 'Z'.
        !
        IF(INTA.GE.97 .AND. INTA.LE.122) INTA = INTA - 32
        IF(INTB.GE.97 .AND. INTB.LE.122) INTB = INTB - 32
        !
    ELSE IF(ZCODE.EQ.233 .OR. ZCODE.EQ.169) THEN
        !
        !        EBCDIC is assumed - ZCODE is the EBCDIC code of either lower or
        !        upper case 'Z'.
        !
        IF(INTA.GE.129 .AND. INTA.LE.137 .OR.&
                INTA.GE.145 .AND. INTA.LE.153 .OR.&
                INTA.GE.162 .AND. INTA.LE.169) INTA = INTA + 64
        IF(INTB.GE.129 .AND. INTB.LE.137 .OR.&
                INTB.GE.145 .AND. INTB.LE.153 .OR.&
                INTB.GE.162 .AND. INTB.LE.169) INTB = INTB + 64
        !
    ELSE IF(ZCODE.EQ.218 .OR. ZCODE.EQ.250) THEN
        !
        !        ASCII is assumed, on Prime machines - ZCODE is the ASCII code
        !        plus 128 of either lower or upper case 'Z'.
        !
        IF(INTA.GE.225 .AND. INTA.LE.250) INTA = INTA - 32
        IF(INTB.GE.225 .AND. INTB.LE.250) INTB = INTB - 32
    END IF
    LSAME = INTA.EQ.INTB
    !
    !     RETURN
    !
    !     End of LSAME
    !
END
DOUBLE PRECISION FUNCTION DLARND(IDIST, ISEED)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     June 30, 1994
    !
    !     .. Scalar Arguments ..
    INTEGER            IDIST
    !     ..
    !     .. Array Arguments ..
    INTEGER            ISEED(4)
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  DLARND returns a random real number from a uniform or normal
    !  distribution.
    !
    !  Arguments
    !  =========
    !
    !  IDIST   (input) INTEGER
    !          Specifies the distribution of the random numbers:
    !          = 1:  uniform (0,1)
    !          = 2:  uniform (-1,1)
    !          = 3:  normal (0,1)
    !
    !  ISEED   (input/output) INTEGER array, dimension (4)
    !          On entry, the seed of the random number generator; the array
    !          elements must be between 0 and 4095, and ISEED(4) must be
    !          odd.
    !          On exit, the seed is updated.
    !
    !  Further Details
    !  ===============
    !
    !  This routine calls the auxiliary routine DLARAN to generate a random
    !  real number from a uniform (0,1) distribution. The Box-Muller method
    !  is used to transform numbers from a uniform to a normal distribution.
    !
    !  =====================================================================
    !
    !     .. Parameters ..
    DOUBLE PRECISION   ONE, TWO
    PARAMETER          (ONE = 1.0D+0, TWO = 2.0D+0)
    DOUBLE PRECISION   TWOPI
    PARAMETER          (TWOPI = 6.2831853071795864769252867663D+0)
    !     ..
    !     .. Local Scalars ..
    DOUBLE PRECISION   T1, T2
    !     ..
    !     .. External Functions ..
    DOUBLE PRECISION   DLARAN
    EXTERNAL           DLARAN
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC COS, LOG, SQRT
    !     ..
    !     .. Executable Statements ..
    !
    !     Generate a real random number from a uniform (0,1) distribution
    !
    T1 = DLARAN(ISEED)
    !
    IF(IDIST.EQ.1) THEN
        !
        !        uniform (0,1)
        !
        DLARND = T1
    ELSE IF(IDIST.EQ.2) THEN
        !
        !        uniform (-1,1)
        !
        DLARND = TWO * T1 - ONE
    ELSE IF(IDIST.EQ.3) THEN
        !
        !        normal (0,1)
        !
        T2 = DLARAN(ISEED)
        DLARND = SQRT(-TWO * LOG(T1)) * COS(TWOPI * T2)
    END IF
    RETURN
    !
    !     End of DLARND
    !
END
DOUBLE COMPLEX   FUNCTION ZLARND(IDIST, ISEED)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     June 30, 1994
    !
    !     .. Scalar Arguments ..
    INTEGER            IDIST
    !     ..
    !     .. Array Arguments ..
    INTEGER            ISEED(4)
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  ZLARND returns a random complex number from a uniform or normal
    !  distribution.
    !
    !  Arguments
    !  =========
    !
    !  IDIST   (input) INTEGER
    !          Specifies the distribution of the random numbers:
    !          = 1:  real and imaginary parts each uniform (0,1)
    !          = 2:  real and imaginary parts each uniform (-1,1)
    !          = 3:  real and imaginary parts each normal (0,1)
    !          = 4:  uniformly distributed on the disc abs(z) <= 1
    !          = 5:  uniformly distributed on the circle abs(z) = 1
    !
    !  ISEED   (input/output) INTEGER array, dimension (4)
    !          On entry, the seed of the random number generator; the array
    !          elements must be between 0 and 4095, and ISEED(4) must be
    !          odd.
    !          On exit, the seed is updated.
    !
    !  Further Details
    !  ===============
    !
    !  This routine calls the auxiliary routine DLARAN to generate a random
    !  real number from a uniform (0,1) distribution. The Box-Muller method
    !  is used to transform numbers from a uniform to a normal distribution.
    !
    !  =====================================================================
    !
    !     .. Parameters ..
    DOUBLE PRECISION   ZERO, ONE, TWO
    PARAMETER          (ZERO = 0.0D+0, ONE = 1.0D+0, TWO = 2.0D+0)
    DOUBLE PRECISION   TWOPI
    PARAMETER          (TWOPI = 6.2831853071795864769252867663D+0)
    !     ..
    !     .. Local Scalars ..
    DOUBLE PRECISION   T1, T2
    !     ..
    !     .. External Functions ..
    DOUBLE PRECISION   DLARAN
    EXTERNAL           DLARAN
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC DCMPLX, EXP, LOG, SQRT
    !     ..
    !     .. Executable Statements ..
    !
    !     Generate a pair of real random numbers from a uniform (0,1)
    !     distribution
    !
    T1 = DLARAN(ISEED)
    T2 = DLARAN(ISEED)
    !
    IF(IDIST.EQ.1) THEN
        !
        !        real and imaginary parts each uniform (0,1)
        !
        ZLARND = DCMPLX(T1, T2)
    ELSE IF(IDIST.EQ.2) THEN
        !
        !        real and imaginary parts each uniform (-1,1)
        !
        ZLARND = DCMPLX(TWO * T1 - ONE, TWO * T2 - ONE)
    ELSE IF(IDIST.EQ.3) THEN
        !
        !        real and imaginary parts each normal (0,1)
        !
        ZLARND = SQRT(-TWO * LOG(T1)) * EXP(DCMPLX(ZERO, TWOPI * T2))
    ELSE IF(IDIST.EQ.4) THEN
        !
        !        uniform distribution on the unit disc abs(z) <= 1
        !
        ZLARND = SQRT(T1) * EXP(DCMPLX(ZERO, TWOPI * T2))
    ELSE IF(IDIST.EQ.5) THEN
        !
        !        uniform distribution on the unit circle abs(z) = 1
        !
        ZLARND = EXP(DCMPLX(ZERO, TWOPI * T2))
    END IF
    RETURN
    !
    !     End of ZLARND
    !
END
DOUBLE PRECISION FUNCTION DLARAN(ISEED)
    !
    !  -- LAPACK auxiliary routine (version 2.0) --
    !     Univ. of Tennessee, Univ. of California Berkeley, NAG Ltd.,
    !     Courant Institute, Argonne National Lab, and Rice University
    !     February 29, 1992
    !
    !     .. Array Arguments ..
    INTEGER            ISEED(4)
    !     ..
    !
    !  Purpose
    !  =======
    !
    !  DLARAN returns a random real number from a uniform (0,1)
    !  distribution.
    !
    !  Arguments
    !  =========
    !
    !  ISEED   (input/output) INTEGER array, dimension (4)
    !          On entry, the seed of the random number generator; the array
    !          elements must be between 0 and 4095, and ISEED(4) must be
    !          odd.
    !          On exit, the seed is updated.
    !
    !  Further Details
    !  ===============
    !
    !  This routine uses a multiplicative congruential method with modulus
    !  2**48 and multiplier 33952834046453 (see G.S.Fishman,
    !  'Multiplicative congruential random number generators with modulus
    !  2**b: an exhaustive analysis for b = 32 and a partial analysis for
    !  b = 48', Math. Comp. 189, pp 331-344, 1990).
    !
    !  48-bit integers are stored in 4 integer array elements with 12 bits
    !  per element. Hence the routine is portable across machines with
    !  integers of 32 bits or more.
    !
    !  =====================================================================
    !
    !     .. Parameters ..
    INTEGER            M1, M2, M3, M4
    PARAMETER          (M1 = 494, M2 = 322, M3 = 2508, M4 = 2549)
    DOUBLE PRECISION   ONE
    PARAMETER          (ONE = 1.0D+0)
    INTEGER            IPW2
    DOUBLE PRECISION   R
    PARAMETER          (IPW2 = 4096, R = ONE / IPW2)
    !     ..
    !     .. Local Scalars ..
    INTEGER            IT1, IT2, IT3, IT4
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC DBLE, MOD
    !     ..
    !     .. Executable Statements ..
    !
    !     multiply the seed by the multiplier modulo 2**48
    !
    IT4 = ISEED(4) * M4
    IT3 = IT4 / IPW2
    IT4 = IT4 - IPW2 * IT3
    IT3 = IT3 + ISEED(3) * M4 + ISEED(4) * M3
    IT2 = IT3 / IPW2
    IT3 = IT3 - IPW2 * IT2
    IT2 = IT2 + ISEED(2) * M4 + ISEED(3) * M3 + ISEED(4) * M2
    IT1 = IT2 / IPW2
    IT2 = IT2 - IPW2 * IT1
    IT1 = IT1 + ISEED(1) * M4 + ISEED(2) * M3 + ISEED(3) * M2 + &
            ISEED(4) * M1
    IT1 = MOD(IT1, IPW2)
    !
    !     return updated seed
    !
    ISEED(1) = IT1
    ISEED(2) = IT2
    ISEED(3) = IT3
    ISEED(4) = IT4
    !
    !     convert 48-bit integer to a real number in the interval (0,1)
    !
    DLARAN = R * (DBLE(IT1) + R * (DBLE(IT2) + R * (DBLE(IT3) + R * &
            (DBLE(IT4)))))
    RETURN
    !
    !     End of DLARAN
    !
END

!  =====================================================================
!     SUBROUTINE LADD
!  =====================================================================
!
SUBROUTINE LADD(J, K, I)
    !
    !  -- ScaLAPACK routine (version 1.7) --
    !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
    !     and University of California, Berkeley.
    !     May 1, 1997
    !
    !     .. Array Arguments ..
    INTEGER            I(2), J(2), K(2)
    !     ..
    !
    !  =====================================================================
    !
    !     .. Parameters ..
    INTEGER            IPOW16, IPOW15
    PARAMETER        (IPOW16 = 2**16, IPOW15 = 2**15)
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC MOD
    !     ..
    !     .. Executable Statements ..
    !
    I(1) = MOD(K(1) + J(1), IPOW16)
    I(2) = MOD((K(1) + J(1)) / IPOW16 + K(2) + J(2), IPOW15)
    !
    RETURN
    !
    !     End of LADD
    !
END
!
!  =====================================================================
!     SUBROUTINE LMUL
!  =====================================================================
!
SUBROUTINE LMUL(K, J, I)
    !
    !  -- ScaLAPACK routine (version 1.7) --
    !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
    !     and University of California, Berkeley.
    !     May 1, 1997
    !
    !     .. Array Arguments ..
    INTEGER            I(2), J(2), K(2)
    !     ..
    !
    !  =====================================================================
    !
    !     .. Parameters ..
    INTEGER            IPOW15, IPOW16, IPOW30
    PARAMETER        (IPOW15 = 2**15, IPOW16 = 2**16, IPOW30 = 2**30)
    !     ..
    !     .. Local Scalars ..
    INTEGER            KT, LT
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC MOD
    !     ..
    !     .. Executable Statements ..
    !
    KT = K(1) * J(1)
    IF(KT.LT.0) KT = (KT + IPOW30) + IPOW30
    I(1) = MOD(KT, IPOW16)
    LT = K(1) * J(2) + K(2) * J(1)
    IF(LT.LT.0) LT = (LT + IPOW30) + IPOW30
    KT = KT / IPOW16 + LT
    IF(KT.LT.0) KT = (KT + IPOW30) + IPOW30
    I(2) = MOD(KT, IPOW15)
    !
    RETURN
    !
    !     End of LMUL
    !
END
!
!  =====================================================================
!     SUBROUTINE XJUMPM
!  =====================================================================
!
SUBROUTINE XJUMPM(JUMPM, MULT, IADD, IRANN, IRANM, IAM, ICM)
    !
    !  -- ScaLAPACK routine (version 1.7) --
    !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
    !     and University of California, Berkeley.
    !     May 1, 1997
    !
    !     .. Scalar Arguments ..
    INTEGER            JUMPM
    !     ..
    !     .. Array Arguments ..
    INTEGER            IADD(2), IAM(2), ICM(2), IRANM(2), IRANN(2)
    INTEGER            MULT(2)
    !     ..
    !
    !  =====================================================================
    !
    !     .. Local Scalars ..
    INTEGER            I
    !     ..
    !     .. Local Arrays ..
    INTEGER            J(2)
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           LADD, LMUL
    !     ..
    !     .. Executable Statements ..
    !
    IF(JUMPM.GT.0) THEN
        DO 10 I = 1, 2
            IAM(I) = MULT(I)
            ICM(I) = IADD(I)
        10    CONTINUE
        DO 20 I = 1, JUMPM - 1
            CALL LMUL(IAM, MULT, J)
            IAM(1) = J(1)
            IAM(2) = J(2)
            CALL LMUL(ICM, MULT, J)
            CALL LADD(IADD, J, ICM)
        20    CONTINUE
        CALL LMUL(IRANN, IAM, J)
        CALL LADD(J, ICM, IRANM)
    ELSE
        IRANM(1) = IRANN(1)
        IRANM(2) = IRANN(2)
    END IF
    !
    RETURN
    !
    !     End of XJUMPM
    !
END
!
!  =====================================================================
!     SUBROUTINE SETRAN
!  =====================================================================
!
SUBROUTINE SETRAN(IRAN, IA, IC)
    !
    !  -- ScaLAPACK routine (version 1.7) --
    !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
    !     and University of California, Berkeley.
    !     May 1, 1997
    !
    !     .. Array Arguments ..
    INTEGER            IA(2), IC(2), IRAN(2)
    !     ..
    !
    !  =====================================================================
    !
    !     .. Local Scalars ..
    INTEGER            I
    !     ..
    !     .. Local Arrays ..
    INTEGER            IAS(2), ICS(2), IRAND(2)
    !     ..
    !     .. Common Blocks ..
    COMMON /RANCOM/    IRAND, IAS, ICS
    SAVE   /RANCOM/
    !     ..
    !     .. Executable Statements ..
    !
    DO 10 I = 1, 2
        IRAND(I) = IRAN(I)
        IAS(I) = IA(I)
        ICS(I) = IC(I)
    10 CONTINUE
    !
    RETURN
    !
    !     End of SETRAN
    !
END
!
!  =====================================================================
!     SUBROUTINE JUMPIT
!  =====================================================================
!
SUBROUTINE JUMPIT(MULT, IADD, IRANN, IRANM)
    !
    !  -- ScaLAPACK routine (version 1.7) --
    !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
    !     and University of California, Berkeley.
    !     May 1, 1997
    !
    !     .. Array Arguments ..
    INTEGER            IADD(2), IRANM(2), IRANN(2), MULT(2)
    !     ..
    !
    !  =====================================================================
    !
    !     .. Local Arrays ..
    INTEGER            IAS(2), ICS(2), IRAND(2), J(2)
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           LADD, LMUL
    !     ..
    !     .. Common Blocks ..
    COMMON /RANCOM/    IRAND, IAS, ICS
    SAVE   /RANCOM/
    !     ..
    !     .. Executable Statements ..
    !
    CALL LMUL(IRANN, MULT, J)
    CALL LADD(J, IADD, IRANM)
    !
    IRAND(1) = IRANM(1)
    IRAND(2) = IRANM(2)
    !
    RETURN
    !
    !     End of JUMPIT
    !
END
!
!  =====================================================================
!     REAL FUNCTION PSRAND
!  =====================================================================
!
REAL FUNCTION PSRAND(IDUMM)
    !
    !  -- ScaLAPACK routine (version 1.7) --
    !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
    !     and University of California, Berkeley.
    !     May 1, 1997
    !
    !     .. Scalar Arguments ..
    INTEGER            IDUMM
    !     ..
    !
    !  =====================================================================
    !
    !     .. Parameters ..
    REAL               DIVFAC, POW16
    PARAMETER          (DIVFAC = 2.147483648E+9, POW16 = 6.5536E+4)
    !     ..
    !     .. Local Arrays ..
    INTEGER            J(2)
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           LADD, LMUL
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC REAL
    !     ..
    !     .. Common Blocks ..
    INTEGER            IAS(2), ICS(2), IRAND(2)
    COMMON /RANCOM/    IRAND, IAS, ICS
    SAVE   /RANCOM/
    !     ..
    !     .. Executable Statements ..
    !
    PSRAND = (REAL(IRAND(1)) + POW16 * REAL(IRAND(2))) / DIVFAC
    !
    CALL LMUL(IRAND, IAS, J)
    CALL LADD(J, ICS, IRAND)
    !
    RETURN
    !
    !     End of PSRAND
    !
END
!
!  =====================================================================
!     DOUBLE PRECISION FUNCTION PDRAND
!  =====================================================================
!
DOUBLE PRECISION FUNCTION PDRAND(IDUMM)
    !
    !  -- ScaLAPACK routine (version 1.7) --
    !     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
    !     and University of California, Berkeley.
    !     May 1, 1997
    !
    !     .. Scalar Arguments ..
    INTEGER            IDUMM
    !     ..
    !
    !  =====================================================================
    !
    !     .. Parameters ..
    DOUBLE PRECISION   DIVFAC, POW16
    PARAMETER          (DIVFAC = 2.147483648D+9, POW16 = 6.5536D+4)
    !     ..
    !     .. Local Arrays ..
    INTEGER            J(2)
    !     ..
    !     .. External Subroutines ..
    EXTERNAL           LADD, LMUL
    !     ..
    !     .. Intrinsic Functions ..
    INTRINSIC DBLE
    !     ..
    !     .. Common Blocks ..
    INTEGER            IAS(2), ICS(2), IRAND(2)
    COMMON /RANCOM/    IRAND, IAS, ICS
    SAVE   /RANCOM/
    !     ..
    !     .. Executable Statements ..
    !
    PDRAND = (DBLE(IRAND(1)) + POW16 * DBLE(IRAND(2))) / DIVFAC
    !
    CALL LMUL(IRAND, IAS, J)
    CALL LADD(J, ICS, IRAND)
    !
    RETURN
    !
    !     End of PDRAND
    !
END

