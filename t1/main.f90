program schomework_t1
    use mpi
    implicit none

    ! Local variables
    integer ictxt, nprocs, nrank, nprow, npcol, myrow, mycol, ierr
    integer argc

    ! Matrix related local variables
    integer ng, nb
    ! Local matrix vector (column-based)
    integer, dimension(9) :: desca, descb
    integer, dimension(:), allocatable :: ipiv
    real(kind = 4), dimension(:), allocatable :: work
    real(kind = 4), dimension(:, :), allocatable :: A, Ac
    real(kind = 4), dimension(:, :), allocatable :: b, bc

    ! Resid related variables
    real(kind = 4) xnorm, resid

    ! Local time variables
    real(kind = 8) ts, te

    ! Local utility variables
    integer i, stat, tmp, info
    character(len = 32) :: arg

    integer ia, ja, ib, jb
    parameter (ia = 1, ja = 1, ib = 1, jb = 1)

    ! External subroutines
    external sl_init, blacs_gridinfo, blacs_gridexit, blacs_exit
    external matinit

    real(kind = 4) :: pslange

    ! MPI Initialization
    call mpi_init(ierr)

    ! MPI processors number and current rank
    call mpi_comm_size(MPI_COMM_WORLD, nprocs, ierr)
    call mpi_comm_rank(MPI_COMM_WORLD, nrank, ierr)

    argc = command_argument_count()
    if (argc .ne. 3) then
        if (nrank .eq. 0) then
            write(*, *) '[ERROR] Invalid number of input parameters'
            write(*, *) 'Usage: mpirun -np X ./program <N>'
            write(*, *) '    X - number of processes'
            write(*, *) '    N - matrix size (should be dividable by X)'
        end if
        goto 404
    end if

    ! Read N from command line arguments
    call getarg(1, arg)
    read(arg, *, iostat = stat) ng
    if (stat .ne. 0) then
        if (nrank .eq. 0) then
            write(*, *) '[ERROR] Invalid matrix size provided'
        end if
        goto 404
    end if

    tmp = ng / nprocs
    tmp = tmp * nprocs
    if (tmp .ne. ng) then
        if (nrank .eq. 0) then
            write(*, *) '[ERROR] Invalid matrix size provided (should be dividable by number of processes)'
        end if
        goto 404
    end if

    ! Calculate grid size npcol x nprow
    call getarg(2, arg)
    read(arg, *, iostat = stat) nprow
    if (stat .ne. 0) then
        if (nrank .eq. 0) then
            write(*, *) '[ERROR] Invalid nprow size provided'
        end if
        goto 404
    end if

    call getarg(3, arg)
    read(arg, *, iostat = stat) npcol
    if (stat .ne. 0) then
        if (nrank .eq. 0) then
            write(*, *) '[ERROR] Invalid npcol size provided'
        end if
        goto 404
    end if

    tmp = nprow * npcol
    if (tmp .ne. nprocs) then
        if (nrank .eq. 0) then
            write(*, *) '[ERROR] Invalid nprow and npcol sizes provided (multiplication of them should be equal to nprocs)'
        end if
        goto 404
    end if


    ! call calculate_grid_size(nprocs, nprow, npcol)

    ! if (nprow .ne. npcol) then
    !     write(*, *) '[ERROR] Invalid nprow != npcol'
    !     goto 404
    ! end if

    nb = ng / nprow

    ! From docs:
    ! A call to the ScaLAPACK TOOLS routine SL_INIT initializes the process grid.
    ! This routine initializes a npcol x nprow process grid by using a row-major ordering of the processes,
    ! and obtains a default system context
    call sl_init(ictxt, nprow, npcol)

    if (nrank .eq. 0) then
        9  format('MPI proccesses count: ', I3)
        10 format('Matrix size ', I3, ' with ', I3, ' x ', I3, ' blocks')
        write(*, fmt = 9) nprocs
        write(*, fmt = 10) ng, ng / nprow, ng / npcol
    end if

    ! From docs:
    ! The user can then query the process grid to identify each process's coordinates (myrow, mycol)
    ! via a call to BLACS_GRIDINFO
    call blacs_gridinfo(ictxt, nprow, npcol, myrow, mycol)

    if (myrow == -1 .or. mycol == -1) then
        41 format('[WARNING] Process with rank ', I2, ' is unused')
        write(*, fmt = 41) nrank
        goto 404
    end if

    if (nrank .eq. 0) then
        11 format('Initialized [', I2, ' x', I2, ' ] process grid')
        write (*, fmt = 11) nprow, npcol
    end if

    allocate(A(nb, nb))
    allocate(b(nb, 1))

    ts = mpi_wtime()
    call generate_distributed_random_matrix(desca, A, ng, nb, ictxt)
    te = mpi_wtime()

    if (nrank .eq. 0) then
        write(*, *) '[Time] Matrix:', (te - ts), ' seconds.'
    end if

    ts = mpi_wtime()
    call generate_distributed_random_vector(descb, b, ng, nb, ictxt)
    te = mpi_wtime()

    if (nrank .eq. 0) then
        write(*, *) '[Time] Vector:', (te - ts), ' seconds.'
    end if

    ! Make a copy of A and b for checking purposes
    ! call psalcpy( 'All', ng, ng, A, 1, 1, desca, Ac, 1, 1, desca)
    ! call psalcpy( 'All', ng, 1, b, 1, 1, descb, bc, 1, 1, descb)
    Ac = A
    bc = b

    allocate(ipiv(nb * nb), work(nb * nb))

    ts = mpi_wtime()
    call psgesv(ng, 1, A, ia, ja, desca, ipiv, b, ib, jb, descb, info)
    te = mpi_wtime()

    if (nrank .eq. 0) then
        write(*, *) '[Time] Vector:', (te - ts), ' seconds.'
    end if

    if (nrank .eq. 0) then
        21 format('INFO code returned by PSGESV = ', I3)
        write (*, fmt = 21) info
    end if

    ! Resudial computational
    ! call psgemm( 'N', 'N', ng, 1, ng, 1.0, Ac, 1, 1, desca, b, 1, 1, descb, -1.0, bc, 1, 1, descb)
    ! xnorm = pslange( 'I', ng, 1, bc, 1, 1, descb, work)
    ! resid = xnorm

    ! if (nrank .eq. 0) then
    !     write (*, *) 'Resid = ', resid
    ! endif

    deallocate(ipiv, work)
    deallocate(A)
    deallocate(b)

    ! From docs:
    ! After the desired computation on a process grid has been completed,
    ! it is advisable to release the process grid via a call to BLACS_GRIDEXIT .
    call blacs_gridexit(ictxt)

    404 continue

    ! From docs
    ! When all computations have been completed, the program is exited with a call to BLACS_EXIT.
    call blacs_exit(0)
end program schomework_t1

subroutine calculate_grid_size(size, nprow, npcol)
    implicit none
    ! Subroutine parameters
    integer, intent(in) :: size
    integer, intent(out) :: nprow, npcol

    ! Local variables
    real(8) tmp

    tmp = real(size)
    tmp = sqrt(tmp)
    npcol = int(ceiling(tmp))

    tmp = size / tmp
    nprow = int(floor(tmp))

    ! npcol = int(tmp)
    ! nprow = npcol
end subroutine calculate_grid_size

! This subroutine generates distributed on the process grid identity matrix
! Input parameters:
! -- A     - array of matrix coefficients with nb x nb size
! -- ng    - global size
! -- nb    - block size
! -- ictxt - process grid context
subroutine generate_distributed_random_matrix(desc, A, ng, nb, ictxt)
    implicit none
    ! Subroutine parameters
    real(kind = 4), intent(inout) :: A(*)
    integer, intent(inout) :: desc(9)
    integer, intent(in) :: ng, nb, ictxt

    ! Grid related local variables
    integer nprow, npcol, myrow, mycol

    ! Local tmp variables
    integer i, j, seed(64)

    ! External subroutines
    external psmatgen

    call blacs_gridinfo(ictxt, nprow, npcol, myrow, mycol)

    desc(1) = 1     ! Matrix is dense
    desc(2) = ictxt ! Current grid context
    desc(3) = ng    ! Number of global rows
    desc(4) = ng    ! Number of global columns
    desc(5) = 1     ! Row block size
    desc(6) = 1     ! Column block size
    desc(7) = 0     ! ??
    desc(8) = 0     ! ??
    desc(9) = nb    ! Leading dimension

    ! For debug purposes generate here the identity matrix first
    ! if (myrow .eq. mycol) then
    !    do i = 1, nb, 1
    !        do j = 1, nb, 1
    !            if (i .eq. j) then
    !                A(j + (i - 1) * nb) = 1.0
    !            else
    !                A(j + (i - 1) * nb) = 0.0
    !            end if
    !        end do
    !    end do
    ! else
    !    do i = 1, nb * nb, 1
    !        A(i) = 0.0
    !    end do
    ! end if

    ! Real call to psmatgen subroutine
    ! PSMATGEN : Parallel Real Single precision MATrix GENerator.
    ! Generate (or regenerate) a distributed matrix A (or sub-matrix of A).
    call random_seed(get = seed)
!    seed = 10
    call psmatgen( ictxt, 'N', 'N', desc(3), desc(4), desc(5), desc(6), A,    &
            desc(9), desc(7), desc(8), seed(1), 0, ng / nprow, 0, ng / npcol, &
            myrow, mycol, nprow, npcol)

end subroutine generate_distributed_random_matrix

! This subroutine generates distributed on the process grid identity matrix
! Input parameters:
! -- A     - array of vector coefficients with nb x 1 size
! -- ng    - global size
! -- nb    - block size
! -- ictxt - process grid context
subroutine generate_distributed_random_vector(desc, b, ng, nb, ictxt)
    implicit none
    ! Subroutine parameters
    real(kind = 4), intent(inout) :: b(*)
    integer, intent(inout) :: desc(9)
    integer, intent(in) :: ng, nb, ictxt

    ! Grid related local variables
    integer nprow, npcol, myrow, mycol

    ! Local tmp variables
    integer i, j, tmp

    call blacs_gridinfo(ictxt, nprow, npcol, myrow, mycol)

    desc(1) = 1     ! Matrix is dense
    desc(2) = ictxt ! Current grid context
    desc(3) = ng    ! Number of global rows
    desc(4) = 1     ! Number of global columns
    desc(5) = 1     ! Row block size
    desc(6) = 1     ! Column block size
    desc(7) = 0     ! ??
    desc(8) = 0     ! ??
    desc(9) = nb    ! Leading dimension

    ! generate here the identity vector
    do i = 1, nb, 1
        b(i) = 1.0
    end do
end subroutine generate_distributed_random_vector